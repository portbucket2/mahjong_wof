/**
 * The driver for the Localization Tool.
 * author: Steve Hart steven.hart@lionstudios.cc
 * First Edit: 7/10/2020
 * Last Edit: 12/3/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace LionStudios.Localization
{

    [System.Serializable]
    public class Strings : MonoBehaviour
    {
        //Singleton instance (in case the dev forgot to add the LocalizationController.prefab)
        private static Strings strings;
        public static Strings Instance
        {
            get
            {
                if (!strings)
                {
                    //attempt to find the singelton in the scene, otherwise create it
                    strings = FindObjectOfType(typeof(Strings)) as Strings;
                    if (strings == null)
                    {
                        GameObject objToSpawn = new GameObject("LocalizationController (generated)");
                        strings = objToSpawn.AddComponent<Strings>();
                    }
                }
                return strings;
            }
        }
        //Inspector values
        [SerializeField]
        [Tooltip("(Editor Only) Use translations for the chosen language rather than the device system language")]
        private bool overrideDeviceLanguage = false;

        [SerializeField]
        private SystemLanguage languageOverride = SystemLanguage.Arabic;

        [SerializeField] private List<SystemLanguage> availableLanguages = new List<SystemLanguage>();

        [SerializeField]
        [Tooltip("For devices with \"Chinese-Simplified\" and \"Chinese-Traditional\" designation, use translations for \"Chinese\"")]
        private bool unifyChineseLanguages = true;

        //settings
        private static Dictionary<string, string> translations = new Dictionary<string, string>();
        private int idIdx = -1;
        private int translationIdx = -1;

        //maintain this gameobject during play
        void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        public static string Get(string keyword)
        {
            return Instance.GetImpl(keyword);
        }

        private string GetImpl(string keyword)
        {
            if (translations.ContainsKey(keyword))
            {
                return translations[keyword];
            }
            else
            {
                string message = "Localization error: Could not find id key for requested keyword: \"" + keyword + "\"";
                Debug.LogError(message);
                return "<err-localization>";
            }
        }

        public static void ParseStringsFile(List<SystemLanguage> expectedLanguages, string resourceFileName = "Strings")
        {
            if (Instance == null)
            {
                Debug.LogWarning("Localization warning: Could not load your translation file. " +
                                 " Localization instance not started yet.");
                return;
            }
            
            // Cache languages
            Instance.availableLanguages = expectedLanguages;
            
            // Loading the dataset from Unity's Resources folder
            TextAsset dataset = Resources.Load<TextAsset>(resourceFileName);
            if (dataset == null)
            {
                Debug.LogError("Localization error: Could not load your translation file.  " +
                    "Please make sure there is a valid file located in Resources/" + resourceFileName + ".txt.  " +
                    "Please consult the Readme in Packages/Localization for setup details.");
                return;
            }
            Instance.ParseStringsFileImpl(dataset);
        }


        private void ParseStringsFileImpl(TextAsset dataset)
        { 
            SystemLanguage language = overrideDeviceLanguage && Debug.isDebugBuild ? languageOverride : Application.systemLanguage;
            if (unifyChineseLanguages)
            {
                language = UnifyChineseLanguageDesignations(language);
            }

            // Splitting the dataset in the end of line
            var splitDataset = dataset.text.Split(new char[] { '\r' });
            if (splitDataset == null)
            {
                Debug.LogError("Localization error: Could not split your translation file by lines.  " +
                    "Please make sure your file (Assets/Resources/Strings.tsv) is correctly formatted.  " +
                    "Please consult the Readme in Packages/Localization for setup details.");
                return;
            }

            // Get the id column index and translation index from the header columns
            if (splitDataset.Length < 1)
            {
                Debug.LogError("Localization error: Your translation file (Assets/Resources/Strings.tsv) must have at least 2 rows, including the header row and at least one row of translations. " +
                    "Please consult the Readme in Packages/Localization for setup details.");
                return;
            }
            string[] headerRow = splitDataset[0].Split(new char[] { '\t' });
            if (availableLanguages.Contains(language))
            {
                SetIndicies(headerRow, language);
            }
            else
            {
                Debug.LogErrorFormat("Localization error: Attempting to use language '{0}' but you have not added that language to 'availableLanguages' in your LocalizationController gameobject.  Defaulting to English.", language.ToString());
                SetIndicies(headerRow, SystemLanguage.English);
            }



            // Iterating through remaining dataset and compose lookup table from id and translation
            for (var i = 1; i < splitDataset.Length; i++)
            {
                string[] row = splitDataset[i].Split(new char[] { '\t' });
                string key = FormatKey(row[idIdx]);

                string value = row[translationIdx];
                if (!translations.ContainsKey(key))
                {
                    translations.Add(key, value);
                }
                else
                {
                    string message = "Localization error: Your Resources/Strings.txt file has a duplicate element: " + key + ".  Each id must be unique.";
                    Debug.LogError(message);
                    return;
                }

            }
        }

        private void SetIndicies(string[] headerRow, SystemLanguage deviceLanguage)
        {
            for (int i = 0; i < headerRow.Length; i++)
            {
                string header = headerRow[i];
                if (header == "id")
                {
                    idIdx = i;
                }
                else if (header == deviceLanguage.ToString())
                {
                    translationIdx = i;
                }
            }
            if (idIdx == -1)
            {
                string message = "Localization error: Your Resources/Strings.txt file does not contain the \"id\" header in the first row";
                Debug.LogError(message);

            }

            if (translationIdx == -1)
            {
                string message = "Localization error: Your Resources/Strings.txt file may have a typo in the language header. " +
                    "Language name: \"" + deviceLanguage.ToString() + "\". Valid language names include:\n";
                foreach (SystemLanguage language in Enum.GetValues(typeof(SystemLanguage)))
                {
                    message += language.ToString() + "\n";
                }

                Debug.LogError(message);
            }
        }

        private SystemLanguage UnifyChineseLanguageDesignations(SystemLanguage language)
        {
            if (language == SystemLanguage.ChineseSimplified || language == SystemLanguage.ChineseTraditional)
            {
                language = SystemLanguage.Chinese;
            }
            return language;
        }

        private string FormatKey(string rawKey)
        {
            string key = rawKey.TrimStart('\n'); //trim newline character from beginning of keys
            return key;
        }

        public static bool CheckIfEnglish()
        {
            return Application.systemLanguage == SystemLanguage.English;
        }
    }

}