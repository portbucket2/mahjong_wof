using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LionStudios.Localization
{ 
    public class LocalizeText : MonoBehaviour
    {
        [SerializeField]
        [Tooltip("Enter the keyword to localize this Text")]
        string keyword = "";

        void Start()
        {
            Localize();
        }

        /// <summary>
        /// Localizes static text into the device's language
        /// </summary>
        public void Localize()
        {
            if (_Localized)
                return;
            
            if (!string.IsNullOrEmpty(keyword))
            {
                //using regular UI Text
                if (gameObject.TryGetComponent(out Text text))
                {
                    text.text = Strings.Get(keyword);
                }
                //using TMPro
                else if (gameObject.TryGetComponent(out TMP_Text tmpro))
                {
                    tmpro.SetText(Strings.Get(keyword));
                }
                //using TMProUGUI
                else if (gameObject.TryGetComponent(out TextMeshProUGUI tmproUGUI))
                {
                    tmproUGUI.SetText(Strings.Get(keyword));
                }
                else
                {
                    Debug.LogErrorFormat("Localization Error: You are trying to Localize a gameobject that" +
                        "does not have a Text, TextMesh, TextMeshPro, or TextMeshProUGUI component: " + gameObject.name);
                }

                _Localized = true;
            }
            else
            {
                Debug.LogWarningFormat("Keyword for localization is not initialized");
            }
        }

        private bool _Localized = false;
    }
}