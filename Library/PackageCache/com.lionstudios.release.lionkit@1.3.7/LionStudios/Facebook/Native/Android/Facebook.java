package com.lionstudios.lionkit;

import android.app.Activity;
import android.util.Log;
import android.os.Bundle;
import java.util.HashMap;
import java.util.Map;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

public class Facebook
{
    private static final String TAG = "LionKit";

    private final AppEventsLogger _FacebookEventLogger;

    public Facebook(Activity currentActivity)
    {
        AppEventsLogger.activateApp( currentActivity.getApplication() );
        _FacebookEventLogger = AppEventsLogger.newLogger( currentActivity );
    }

    public void LogEvent(final String event, final Bundle bundle)
    {
        Log.d(TAG, "Tracking facebook event: " + event);
        for (String key : bundle.keySet())
        {
            Log.d( TAG, "Param: " + key + " : " + bundle.get(key).toString());
        }

        _FacebookEventLogger.logEvent(event, bundle);
    }

}