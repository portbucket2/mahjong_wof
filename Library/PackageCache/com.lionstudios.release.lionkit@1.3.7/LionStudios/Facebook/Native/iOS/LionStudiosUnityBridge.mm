//
//  UnityBridge.h
//  Unity-iPhone
//
//  Created by Michael Silk on 8/10/20.
//

#import <Foundation/Foundation.h>
#include <string>
#import "LionStudiosFBUnityUtility.h"
#import "FBSDKAppEvents.h"


extern "C"
{
//    void HelloWorld()
//    {
//        NSLog(@"hi there");
//        [FBSDKAppEvents logEvent:@"superDuperiOSEvent"];
//    }

    void IOSFBAppEventsLogEvent(const char *eventName, double valueToSum, int numParams,
                                const char **paramKeys, const char **paramVals)
    {
        NSDictionary *params =  [LionStudiosFBUnityUtility dictionaryFromKeys:paramKeys values:paramVals length:numParams];
        [FBSDKAppEvents logEvent:[LionStudiosFBUnityUtility stringFromCString:eventName] valueToSum:valueToSum parameters:params];
    }
}
