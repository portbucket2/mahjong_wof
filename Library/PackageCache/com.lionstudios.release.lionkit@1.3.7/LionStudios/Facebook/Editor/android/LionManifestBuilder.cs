#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using UnityEngine;
using System.IO;

namespace LionStudios.Facebook.Editor.Android
{
	public class LionManifestBuilder : AndroidManifestBuilder
	{
		public const string AndroidManifestPath = "Plugins/Android/AndroidManifest.xml";
		private const string attr_fb_app_id = "facebook_app_id";
		private const string meta_app_id = "com.facebook.sdk.ApplicationId";
		private const string meta_auto_log_id = "com.facebook.sdk.AutoLogAppEventsEnabled";
		private const string permission_internet = "android.permission.INTERNET";
		private const string content_provider = "com.facebook.FacebookContentProvider";
		private const string content_provider_app = "com.facebook.app.FacebookContentProvider";

		public static bool CheckManifest()
		{ 
			return ManifestMod.CheckManifest();

			//bool result = true;
			//var outputFile = Path.Combine(Application.dataPath, AndroidManifestPath);
			//if (!File.Exists(outputFile))
			//{
			//	Debug.LogError("An android manifest must be generated.");
			//	return false;
			//}

			//XDocument manifest = XDocument.Load(outputFile);
			//if (manifest == null)
			//{
			//	Debug.LogError("Couldn't load " + outputFile);
			//	return false;
			//}

			//XElement manNode = manifest.Element(key.manifest);
			//if (manNode == null)
			//{
			//	Debug.LogError("Parsing error");
			//	return false;
			//}
			//XElement application = manNode.Element(key.application);
			//if (application == null)
			//{
			//	Debug.LogError("Parsing error");
			//	return false;
			//}

			//XElement activity = application.Element(key.activity);
			//if (activity == null)
			//{
			//	Debug.LogError("Parsing error");
			//	return false;
			//}

			//XName androidName = XName.Get("name", GetNamespace(manifest).NamespaceName);

			//var deprecatedMainActivityName = "com.facebook.unity.FBUnityPlayerActivity";
			//XAttribute activityAttr = activity.Attribute(androidName);

			//if (activityAttr.Value == deprecatedMainActivityName)
			//{
			//	Debug.LogWarning(string.Format("{0} is deprecated and no longer needed for the Facebook SDK.  Feel free to use your own main activity or use the default \"com.unity3d.player.UnityPlayerNativeActivity\"", deprecatedMainActivityName));
			//}

			//return result;
		}

		public static void GenerateManifest(string path = null)
		{
			Debug.Log("GenerateManifest");
			ManifestMod.GenerateManifest(path);
			UnityEditor.AssetDatabase.SaveAssets();
			UnityEditor.AssetDatabase.Refresh();

			//LionSettingsAndroid.SerializedPropsAndroid androidSettings = new LionSettingsAndroid.SerializedPropsAndroid();
			//XDocument manifest = GetAndroidManifest("Assets/" + pathToManifestUnity);
			//string ns = GetNamespace(manifest).NamespaceName;

			//UpdateFacebookManifest(manifest, ns);
			//SaveManifest(manifest, "Assets/" + pathToManifestUnity);
		}

		private static void UpdateFacebookManifest(XDocument manifest, string ns)
		{

			XElement application = manifest.Root.Element(key.application);
			XElement activity = application.Element(key.activity);

			XName androidName = XName.Get("name", ns);
			XName androidAuthorities = XName.Get("authorities", ns);
			XName androidExported = XName.Get("exported", ns);
			XName androidValue = XName.Get("value", ns);
			XName androidDebuggable = XName.Get("debuggable", ns);

			XElement metaAppIdElement;
			XElement metaAutoLogElement;
			XElement providerElement;
			XElement usePermissionElement;

			metaAppIdElement = application.Elements().FirstOrDefault(x => { return x.Name == "meta-data" && x.HasAttributes && x.FirstAttribute.Value == meta_app_id; });
			metaAutoLogElement = application.Elements().FirstOrDefault(x => { return x.Name == "meta-data" && x.HasAttributes && x.FirstAttribute.Value == meta_auto_log_id; });
			providerElement = application.Elements().FirstOrDefault(x => { return x.Name == "provider" && x.HasAttributes && x.FirstAttribute.Value == content_provider; });
			usePermissionElement = manifest.Root.Elements().FirstOrDefault(x => { return x.Name == "uses-permission" && x.HasAttributes && x.FirstAttribute.Value == permission_internet; });

			if (metaAppIdElement == null)
			{
				metaAppIdElement = new XElement("meta-data");
				metaAppIdElement.SetAttributeValue(androidName, meta_app_id);
				application.Add(metaAppIdElement);
			}

			if (metaAutoLogElement == null)
			{
				metaAutoLogElement = new XElement("meta-data");
				metaAutoLogElement.SetAttributeValue(androidName, meta_auto_log_id);
				application.Add(metaAutoLogElement);
			}

			if (providerElement == null)
			{
				providerElement = new XElement("provider");
				providerElement.SetAttributeValue(androidName, content_provider);
				providerElement.SetAttributeValue(androidExported, true);
				application.Add(providerElement);
			}

			if (usePermissionElement == null)
			{
				usePermissionElement = new XElement("uses-permission");
				usePermissionElement.SetAttributeValue(androidName, permission_internet);
				manifest.Root.Add(usePermissionElement);
			}

			metaAppIdElement.SetAttributeValue(androidValue, "fb" + LionSettings.Facebook.AppId);
			metaAutoLogElement.SetAttributeValue(androidValue, LionSettings.Facebook.AutoLogAppEventsEnabled);
			providerElement.SetAttributeValue(androidAuthorities, content_provider_app + LionSettings.Facebook.AppId);
			application.SetAttributeValue(androidDebuggable, Debug.isDebugBuild);
		}
	}
}
#endif