﻿using System.Collections;
using System.Collections.Generic;
using LionStudios;
using LionStudios.Editor.Platform;
using LionStudios.Runtime.IAP;
using UnityEditor;
using UnityEngine;
using System.IO;
using LionStudios.Editor.PackageManager;


namespace LionStudios.Editor
{
    public static class PropertyEnforcer
    {
        [UnityEditor.Callbacks.DidReloadScripts]
        internal static void EnforceSettings()
        {
            EnforceFirebaseProps();
            EnforceIAP();
            EnforceInAppReview();
            CleanUp();
        }
        
        static void CleanUp()
        {
#if !LION_KIT_DEV 
            // These are folders/files that we no longer are moving from the packages to Assets
            // so if they exist in the Assets dir we will have GUID conflicts that we need to cleanup. 
             string[] pathsForRemoval = new[]
             {
                "Assets/MaxSdk/AppLovin",
                "Assets/MaxSdk/Prefabs",
             };

            foreach (var path in pathsForRemoval)
            {
                if (Directory.Exists(path))
                {
                    Debug.Log($"Cleaning up path: {path}");
                    Directory.Delete(path, true);
                }
            }
            
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
#endif
        }

        static void EnforceFirebaseProps()
        {
            /// check for firebase installation
            if (LionSettings.Firebase.Enabled
                && SDKDownload.HasFirebase())
            {
                LionPlatformDefine.TryAddDefineSymbol(LionDefineSymbol.LK_USE_FIREBASE.ToString());
            }
            else
            {
                LionPlatformDefine.TryRemoveDefineSymbol(LionDefineSymbol.LK_USE_FIREBASE.ToString());
            }
        }
        
        static void EnforceIAP()
        {
            /// Check for IAP installation
            if (LionSettings.InAppPurchase.Enabled 
                && LionIAP.IAP_Service_Enabled()
                && LionIAP.IAP_Purchasing_Installed())
            {
                LionPlatformDefine.TryAddDefineSymbol(LionDefineSymbol.LK_USE_UNITY_IAP.ToString());
            }
            else
            {
                LionPlatformDefine.TryRemoveDefineSymbol(LionDefineSymbol.LK_USE_UNITY_IAP.ToString());
            }
        }

        static void EnforceInAppReview()
        {
            /// check for In-App Review installation
            if (LionSettings.InAppReview.Enabled
                && SDKDownload.HasInAppReview())
            {
                LionPlatformDefine.TryAddDefineSymbol(LionDefineSymbol.LK_USE_APP_REVIEW.ToString());
            }
            else
            {
                LionPlatformDefine.TryRemoveDefineSymbol(LionDefineSymbol.LK_USE_APP_REVIEW.ToString());
            }
        }
    }
    

}