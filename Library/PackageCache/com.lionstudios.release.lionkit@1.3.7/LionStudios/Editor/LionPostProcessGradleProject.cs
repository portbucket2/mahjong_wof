﻿
#if UNITY_2018_2_OR_NEWER && UNITY_ANDROID

using System.IO;
using LionStudios.Debugging;
using UnityEditor.Android;
using UnityEngine;
using LionStudios.Facebook.Editor.Android;

namespace LionStudios.Editor
{
    public class LionPostProcessGradleProject : IPostGenerateGradleAndroidProject
    {
        public void OnPostGenerateGradleAndroidProject(string path)
        {
            // On Unity 2019.3+, the path returned is the path to the unityLibrary's module.
            // The AppLovin Quality Service buildscript closure related lines need to be added to the root build.gradle file.
            var rootManifestPath = Path.Combine(path, "src/main/AndroidManifest.xml");

            LionDebug.Log("OnPostGenerateGradleAndroidProject - " + rootManifestPath + " :: File Exists = " + File.Exists(rootManifestPath));
            // if (File.Exists(rootManifestPath))
            //     Debug.Log(File.ReadAllText(rootManifestPath));
            
            LionManifestBuilder.GenerateManifest(rootManifestPath);
        }

        public int callbackOrder
        {
            get { return int.MaxValue; }
        }
    }
}

#endif
