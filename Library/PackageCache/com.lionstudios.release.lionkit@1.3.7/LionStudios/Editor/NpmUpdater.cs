﻿#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LionStudios.Utility.Json;

namespace LionStudios.Editor
{
    public class ManifestUpdate
    {
        private static string manifestPath
        {
            get
            {
                return Directory.GetCurrentDirectory() + "/Packages/manifest.json";
            }
        }

        private static Dictionary<string, object> _manifest;
        private static bool _hasUpdated;

        public static void TryUpdate()
        {
            try
            {
                if (_hasUpdated == true)
                {
                    return;
                }

                if (!File.Exists(manifestPath))
                {
                    return;
                }

                string jsonText = File.ReadAllText(manifestPath);
                if (string.IsNullOrEmpty(jsonText))
                {
                    return;
                }

                _manifest = MiniJson.Deserialize(jsonText) as Dictionary<string, object>;
                if (_manifest == null)
                {
                    throw new Exception("Lion Studios NPM Setup: Failed to parse manifest!");
                }

                UpdateDependencies();
                if (!string.IsNullOrEmpty(LionKit.PackageId) 
                    && !LionKit.PackageId.Contains("beta"))
                {
                    UpdateScopedRegistries();
                }

                string content = MiniJson.Serialize(_manifest, prettyPrint: true);
                File.WriteAllText(manifestPath, content);
                UnityEditor.AssetDatabase.Refresh();
                _hasUpdated = true;
            }
            catch(Exception e)
            {
                UnityEngine.Debug.LogException(e);
            }
        }

        private static void UpdateDependencies()
        {
            string dependencyKey = "dependencies";
            string oldLionKitName = "com.lionstudios.lionkit";
            string newLionKitName = LionKit.PackageId;

            if (!string.IsNullOrEmpty(newLionKitName))
            {
                Dictionary<string, object> dependencies = _manifest[dependencyKey] as Dictionary<string, object>;
                if (dependencies.ContainsKey(oldLionKitName))
                {
                    dependencies.Remove(oldLionKitName);
                    dependencies.Add(newLionKitName, LionKit.Version);
                }
            }
        }

        private static void UpdateScopedRegistries()
        {
            string scopedRegKey = "scopedRegistries";

            List<object> scopedRegistries = (List<object>)_manifest[scopedRegKey];
            if (scopedRegistries == null)
            {
                scopedRegistries = new List<object>();
            }

            foreach(Dictionary<string, object> registry in scopedRegistries)
            {
                string regName = (string)registry["name"];
                if (!string.IsNullOrEmpty(regName) && regName.Contains("Lion Studios"))
                {
                    registry["scopes"] = new string[]
                    {
                        "com.lionstudios.release",
                        "com.lionstudios.templates",
                        "com.lionstudios.dev",
#if LION_KIT_DEV
                        "com.lionstudios.development"
#endif
                    };
                }
            }
            _manifest[scopedRegKey] = scopedRegistries;
        }
    }

}
#endif