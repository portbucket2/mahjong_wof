#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using LionStudios.Debugging;
using MiniJson = LionStudios.Utility.Json.MiniJson;

namespace LionStudios.Editor.PackageManager
{
    internal class SDKDownload
    {
#if UNITY_EDITOR_WIN
        private const string manifestPath = "..\\Packages\\manifest.json";
#else
        private const string manifestPath = "../Packages/manifest.json";
#endif

        private const string dependenciesKey = "dependencies";

        private static Dictionary<string, object> manifest = null;
        private static Dictionary<string, object> Manifest
        {
            get
            {
                if (manifest == null)
                {
                    string path = Path.Combine(Application.dataPath, manifestPath);
                    if (!File.Exists(path))
                    {
                        return null;
                    }
            
                    string manifestContent = File.ReadAllText(path);
                    manifest = MiniJson.Deserialize(manifestContent) as Dictionary<string, object>;
                }

                return manifest;
            }
        }
        
        private static void SaveManifest()
        {
            string path = Path.Combine(Application.dataPath, manifestPath);
            File.WriteAllText(path, MiniJson.Serialize(Manifest, true));
        }

        private static bool AddDependency(string name, string version)
        {
            if (Manifest == null || Manifest.Count == 0 || !Manifest.ContainsKey(dependenciesKey))
            {
                LionDebug.LogError(
                    $"Failed to add package dependency '{name}@{version}'. Manifest is missing or corrupt.");
                return false;
            }
            
            Dictionary<string, object> dependencies = Manifest[dependenciesKey] as Dictionary<string, object>;
            if (!dependencies.ContainsKey(name))
            {
                dependencies.Add(name, version);
                return true;
            }else if ((string) dependencies[name] != version)
            {
                dependencies[name] = version.ToString();
                return true;
            }
            else
            {
                LionDebug.LogWarning(
                    $"Package dependency '{name}@{version}' already exists. Aborting package install.");
                return false;
            }
        }

        private static bool RemoveDependency(string name)
        {
            if (Manifest == null || Manifest.Count == 0 || !Manifest.ContainsKey(dependenciesKey))
            {
                LionDebug.LogError(
                    $"Failed to remove package dependency '{name}'. Manifest is missing or corrupt.");
                return false;
            }

            try
            {
                Dictionary<string, object> dependencies = Manifest[dependenciesKey] as Dictionary<string, object>;
                return dependencies.Remove(name);
            }
            catch (KeyNotFoundException keyNotFound)
            {
                LionDebug.LogError("Manifest is malformed!");
                LionDebug.LogException(keyNotFound);
                return false;
            }
        }

        private static bool HasDependency(string name)
        {
            if (Manifest == null || Manifest.Count == 0 || !Manifest.ContainsKey(dependenciesKey))
            {
                LionDebug.LogError(
                    $"Failed to query dependency '{name}'. Manifest is missing or corrupt.");
                return false;
            }

            try
            {
                Dictionary<string, object> dependencies = Manifest[dependenciesKey] as Dictionary<string, object>;
                return dependencies.ContainsKey(name);
            }
            catch (KeyNotFoundException keyNotFound)
            {
                LionDebug.LogError("Manifest is malformed!");
                LionDebug.LogException(keyNotFound);
                return false;
            }
        }
        
        public static void InstallFirebase()
        {
            /// add firebase to manifest
            if (AddDependency(FirebaseSdk.PkgNameCoreApp, FirebaseSdk.coreAppMaxVersion)
                && AddDependency(FirebaseSdk.PkgNameAnalytics, FirebaseSdk.analyticsMaxVersion))
            {
                SaveManifest();
                LionDebug.Log("Firebase installed, refreshing packages.");
                UnityEditor.Compilation.CompilationPipeline.RequestScriptCompilation();
            }
        }

        public static void UninstallFirebase()
        {
            if (RemoveDependency(FirebaseSdk.PkgNameCoreApp)
                && RemoveDependency(FirebaseSdk.PkgNameAnalytics))
            {
                SaveManifest();
                LionDebug.Log("Firebase uninstalled, refreshing packages.");
                UnityEditor.Compilation.CompilationPipeline.RequestScriptCompilation();
            }
        }

        public static bool HasFirebase()
        {
            return HasDependency(FirebaseSdk.PkgNameCoreApp) && HasDependency(FirebaseSdk.PkgNameAnalytics);
        }

        public static void InstallAppReview()
        {
            /// add in-app review to manifest
            if (AddDependency(LionInAppReview.GoogleCommonPkgId, LionInAppReview.GoogleCommonPkgVer)
                && AddDependency(LionInAppReview.GoogleReviewPkgId, LionInAppReview.GoogleReviewPkgVer))
            {
                SaveManifest();
                LionDebug.Log("In-App Review installed, refreshing packages.");
                UnityEditor.Compilation.CompilationPipeline.RequestScriptCompilation();
            }
        }

        public static void UninstallAppReview()
        {
            /// add in-app review to manifest
            if (RemoveDependency(LionInAppReview.GoogleCommonPkgId)
                && RemoveDependency(LionInAppReview.GoogleReviewPkgId))
            {
                SaveManifest();
                LionDebug.Log("In-App Review installed, refreshing packages.");
                UnityEditor.Compilation.CompilationPipeline.RequestScriptCompilation();
            }
        }

        public static bool HasInAppReview()
        {
            return HasDependency(LionInAppReview.GoogleCommonPkgId)
                   && HasDependency(LionInAppReview.GoogleReviewPkgId);
        }
    }
}
#endif