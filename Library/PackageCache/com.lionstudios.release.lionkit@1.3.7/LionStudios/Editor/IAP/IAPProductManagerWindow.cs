﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if LK_USE_UNITY_IAP && UNITY_PURCHASING
using LionStudios.Runtime.IAP;
using UnityEngine.Purchasing;

namespace LionStudios.Editor
{
    public class IAPProductManagerWindow : EditorWindow
    {
        private SerializedObject _SerializedObj;
        private SerializedProperty _products;
        
        public static void OpenWindow()
        {
            IAPProductManagerWindow window =
                (IAPProductManagerWindow) GetWindow(typeof(IAPProductManagerWindow), true, "IAP Product Manager");
            window.Show();
        }

        private void OnEnable()
        {
            LionSettings settings = LionSettings.Get();
            _SerializedObj = new SerializedObject(settings);
            _products = _SerializedObj.FindProperty("_InAppPurchase._Products");
        }

        private Vector2 _productScrollPos;
        private void OnGUI()
        {
            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(250));
            {
                if (GUILayout.Button("Add Product"))
                {
                    _products.InsertArrayElementAtIndex(_products.arraySize);
                    _SerializedObj.ApplyModifiedProperties();
                }
            }
            
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(250));
            EditorGUILayout.LabelField("Product ID");
            EditorGUILayout.LabelField("Type");
            EditorGUILayout.LabelField("Store Availability");
            EditorGUILayout.LabelField("Enabled");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            _productScrollPos = EditorGUILayout.BeginScrollView(_productScrollPos);
            
            // draw current products
            for (int i = 0; i < _products.arraySize; i++)
            {
                SerializedProperty product = _products.GetArrayElementAtIndex(i);

                EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(250));
                GUILayout.FlexibleSpace();
                using (SerializedProperty p = _products.GetArrayElementAtIndex(i))
                {
                    SerializedProperty prodId = p.FindPropertyRelative("productId");
                    SerializedProperty prodType = p.FindPropertyRelative("productType");
                    SerializedProperty prodAvailability = p.FindPropertyRelative("productAvailability");
                    SerializedProperty prodEnabled = p.FindPropertyRelative("productEnabled");

                    EditorGUILayout.PropertyField(prodId,new GUIContent(""),  GUILayout.MinWidth(200));
                    EditorGUILayout.PropertyField(prodType, new GUIContent(""), GUILayout.MinWidth(130));
                    GUILayout.Space(77);
                    EditorGUILayout.PropertyField(prodAvailability, new GUIContent(""), GUILayout.MinWidth(130));
                    GUILayout.Space(75);
                    EditorGUILayout.PropertyField(prodEnabled, new GUIContent(""));
                }

                var oldColor = GUI.color;
                GUI.color = Color.red;
                if (GUILayout.Button("Delete Product", EditorStyles.miniButtonRight))
                {
                    _products.DeleteArrayElementAtIndex(i);
                }

                GUI.color = oldColor;
                EditorGUILayout.EndHorizontal();
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();

            _SerializedObj.ApplyModifiedProperties();
        }
    }
}
#else
namespace LionStudios.Editor
{
    public class IAPProductManagerWindow : EditorWindow
    {
        public static void OpenWindow(){}
    }
}
#endif