﻿#if UNITY_EDITOR
using System.IO;
using System.Linq;
using LionStudios.Debugging;
using UnityEngine;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;

namespace LionStudios.Editor
{
    public static class LionKitPostProcessAssets
    {
        private static readonly string[] illegalPackages = new string[]
        {
            "com.unity.ads"
        };
        
        private static string[] PATHS_BLACKLIST = new string[] {
            "ExternalDependencyManager/",
            "PlayServicesResolver/"
        };

        [UnityEditor.Callbacks.DidReloadScripts]
        private static void OnReloadedScripts()
        {
            RemoveBlacklistDirectories();
            
            // check for illegal packages
            illegalListRequest = Client.List();
            EditorApplication.update -= CheckForIllegalPackages;
            EditorApplication.update += CheckForIllegalPackages;
        }
        
        private static void RemoveBlacklistDirectories()
        {
            foreach(string path in PATHS_BLACKLIST)
            {
                string assetPath = Path.Combine(Application.dataPath, path);
                if (Directory.Exists(assetPath))
                {
                    string notif = string.Format("Lion Kit: Incompatible package detected '{0}'\n"
                        + "Try installing with the Package Manager.", path);
                    Debug.LogWarning(notif);

                    Directory.Delete(assetPath, true);
                    AssetDatabase.Refresh();
                }
            }
        }
        
        private static ListRequest illegalListRequest;
        private static void CheckForIllegalPackages()
        {
            LionDebug.Log("Checking for illegal packages", LionDebug.DebugLogLevel.Verbose);
            
            if (illegalListRequest != null && illegalListRequest.IsCompleted)
            {
                var illegalPkgs  = illegalListRequest.Result
                    .Where(x => illegalPackages.Contains(x.name)).ToArray();

                if (illegalPkgs.Length > 0)
                {
                    UnityEditor.EditorUtility.DisplayProgressBar("Removing Illegal Packages", "Please Wait...", 0);
                    for (int i = 0; i < illegalPkgs.Length; i++)
                    {
                        UnityEditor.EditorUtility.DisplayProgressBar("Removing Illegal Packages", "Please Wait...",
                            ((float) i / (float) illegalPkgs.Length));

                        AskForRemovePrompt(illegalPkgs[i].name, illegalPkgs[i].displayName);
                    }
                    UnityEditor.EditorUtility.ClearProgressBar();
                }
            }
            
            EditorApplication.update -= CheckForIllegalPackages;
        }
        
        private static void AskForRemovePrompt(string packageName, string displayName)
        {
            string msg = $"{displayName} conflicts with Lion Kit or its dependencies. Lion Kit may not work correctly if you choose to keep this package." +
                         $"What would you like to do?";
            if (UnityEditor.EditorUtility.DisplayDialog("Conflicting Package", msg, "Remove", "Keep"))
            {
                var rmPkg = Client.Remove(packageName);
                while(!rmPkg.IsCompleted);
            }
        }
    }
}
#endif