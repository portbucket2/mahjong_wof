#if UNITY_EDITOR
using System.IO;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using LionStudios.Debugging;
using LionStudios.Utility;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

#if UNITY_ANDROID
namespace LionStudios.Editor.Platform
{
    public class PlatformPreProcessor : IPreprocessBuildWithReport
    {
        public int callbackOrder { get; }
        
        public void OnPreprocessBuild(BuildReport report)
        {
            LionDebug.Log($"======Lion Kit Build Preprocessor (Callback: {callbackOrder})======");
            WritePluginVersion();
            WriteMultiDex();
            LionDebug.Log("===========================================");
        }
        
        private void WritePluginVersion()
        {
            const string baseTemplatePath = "Assets/Plugins/Android/baseProjectTemplate.gradle";
            const string classPathContent = "com.android.tools.build:gradle:";

            if (!File.Exists(Path.GetFullPath(baseTemplatePath)))
            {
                throw new BuildFailedException($"Missing {baseTemplatePath}");
            }

            string gradlePluginVersion = LionSettings.GeneralSettings.OverrideGradlePluginVersion
                ? LionSettings.GeneralSettings.GradlePluginOverride
                :
#if UNITY_2020_1_OR_NEWER
            "3.6.1";
#else
            "3.4.3";
#endif
            
            LionDebug.Log($"Writing Gradle Plugin Version: {gradlePluginVersion}");

            LionStudios.Editor.EditorUtility.ReplaceInFile(baseTemplatePath,
                $"{classPathContent}" + @"[\d\.]+",
                $"{classPathContent}{gradlePluginVersion}");
        }
        
        private void WriteMultiDex()
        {
            const string mainTemplatePath = "Assets/Plugins/Android/mainTemplate.gradle";
            const string multiDexVersion = "1.0.3";
            const string multiDexDep = "com.android.support:multidex:";
            const string multiDexConfigTag = "multiDexEnabled";

            if (!File.Exists(Path.GetFullPath(mainTemplatePath)))
            {
                throw new BuildFailedException($"Missing {mainTemplatePath}");
            }
            
            string useMultiDex = LionSettings.GeneralSettings.UseMultiDexSupport.ToString().ToLower();
            LionDebug.Log($"Writing Multi-Dex Support. (Enabled: '{useMultiDex}')");
            
            var content = string.Empty;
            using (StreamReader reader = new StreamReader(Path.GetFullPath(mainTemplatePath)))
            {
                content = reader.ReadToEnd();
                reader.Close();
            }

            // add dependency implementation
            var multiDexDependency = Regex.Match(content, multiDexDep); 
            if (multiDexDependency.Success == false)
            {
                Debug.Log("Failed to find multiDex dependency..adding it");
                
                var dependencies = Regex.Match(content, @"dependencies[\s]*{");
                if (dependencies.Success == false)
                {
                    throw new BuildFailedException("mainTemplate.gradle is not formatted correctly!");
                }

                content = content.Insert(dependencies.Index + dependencies.Length,
                    $"\n    implementation '{multiDexDep}{multiDexVersion}'\n");
            }
            
            // add config tag
            var multiDexConfig = Regex.Match(content, multiDexConfigTag);
            if (multiDexConfig.Success == false)
            {
                Debug.Log("Failed to find multi-dex config tag..adding it");

                var defaultConfig = Regex.Match(content, @"defaultConfig[\s]*{");
                if (!defaultConfig.Success)
                {
                    throw new BuildFailedException("mainTemplate.gradle is not formatted correctly!");
                }

                content = content.Insert(defaultConfig.Index + defaultConfig.Length,
                    $"\n{new string(' ', 8)}{multiDexConfigTag} {useMultiDex}");
            }

            content = Regex.Replace(content, multiDexConfigTag + @"[\s]+(true|false)",
                $"{multiDexConfigTag} {useMultiDex}");

            using (StreamWriter writer = new StreamWriter(Path.GetFullPath(mainTemplatePath)))
            {
                writer.Write(content);
                writer.Close();
            }
        }
    }
}
#endif
#endif