#if UNITY_EDITOR
using System.IO;
using System.Linq;
using UnityEditor;
using System.Text.RegularExpressions;

namespace LionStudios.Editor
{
    public static class EditorUtility
    {
        [MenuItem("LionStudios/Advanced/Delete All Data")]
        public static void DeleteAllPersistentData()
        {
            Database.DeleteAllData();
        }
        
        public static T[] GetAllAssets<T>() where T : UnityEngine.Object
        {
            string[] assetGUIDs = AssetDatabase.FindAssets("t:" + typeof(T));
            T[] assets = new T[assetGUIDs.Length];
            for (int i = 0; i < assetGUIDs.Length; i++)
            {
                assets[i] = AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(assetGUIDs[i]));
            }

            return assets;
        }
        
        public static void ReplaceInFile(string filePath, string searchText, string replaceText)
        {
            var content = string.Empty;
            using (StreamReader reader = new StreamReader( filePath ))
            {
                content = reader.ReadToEnd();
                reader.Close();
            }

            content = Regex.Replace( content, searchText, replaceText );

            using (StreamWriter writer = new StreamWriter( filePath ))
            {
                writer.Write( content );
                writer.Close();
            }
        }
    } 
}

#endif