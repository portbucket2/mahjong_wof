﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.WSA;

#if !LION_KIT_DEV
using System.Linq;
using System.Xml;
using GooglePlayServices;
using LionStudios.Debugging;
using UnityEngine;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
#endif

namespace LionStudios.Editor
{
	[SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
	public static class LionKitPostImportMover
	{
		private static LionKitPostImportMover.PersistentData data;
		private class PersistentData : PersistentData<LionKitPostImportMover.PersistentData>
		{
			public string version = "0.0.0";
		}

		private const string PkgDir = "Packages";
		private const string DestDir = "Assets";
		public const string ImportMoveDir = "ImportMove~";
		
		public static readonly List<string> CopyPaths = new List<string>
		{
#if UNITY_EDITOR_WIN
			"LionStudios\\GDPR",
			"LionStudios\\Debugger",
			
			"Adjust\\Android\\AdjustAndroidManifest.xml",
			
			// "MaxSdk/Prefabs",
			//"MaxSdk\\Resources",
			
			// only overwrite when the LionSdk version is ahead of the local version
			// "MaxSdk/AppLovin/Editor/Dependencies.xml",
			
			// don't overwrite - this is because the EDM will handle resolution and devs can customize their template files
			"Plugins\\Android\\gradleTemplate.properties",
			//"Plugins/Android/launcherTemplate.gradle",
			"Plugins\\Android\\mainTemplate.gradle",
			"Plugins\\Android\\baseProjectTemplate.gradle",
			
			// don't overwrite - dev may have customized this file
			"Plugins\\Android\\res\\xml\\network_security_config.xml"
#else
			"LionStudios/GDPR",
			"LionStudios/Debugger",
			
			"Adjust/Android/AdjustAndroidManifest.xml",
			
			// "MaxSdk/Prefabs",
			//"MaxSdk/Resources",
			
			// only overwrite when the LionSdk version is ahead of the local version
			// "MaxSdk/AppLovin/Editor/Dependencies.xml",
			
			// don't overwrite - this is because the EDM will handle resolution and devs can customize their template files
			"Plugins/Android/gradleTemplate.properties",
			//"Plugins/Android/launcherTemplate.gradle",
			"Plugins/Android/mainTemplate.gradle",
			"Plugins/Android/baseProjectTemplate.gradle",
			
			// don't overwrite - dev may have customized this file
			"Plugins/Android/res/xml/network_security_config.xml"
#endif
		};

#if LION_KIT_DEV
		[MenuItem("LionStudios/Advanced/LK Developer/Clear LK Version Editor Pref")]
		public static void ClearPostImportEditorPref()
		{
			data.Delete();
			data = null;
		}
		
#else
		
		private static readonly List<string> NoOverwrite = new List<string>
		{
			"Plugins/Android/gradleTemplate.properties",
			//"Plugins/Android/launcherTemplate.gradle",
			"Plugins/Android/mainTemplate.gradle",
			"Plugins/Android/baseProjectTemplate.gradle",
			
			"Plugins/Android/res/xml/network_security_config.xml",
		};
		
#endif

		private static readonly List<string> IllegalDirectories = new List<string>
		{
			#if UNITY_EDITOR_WIN
			"MaxSdk\\Resources\\Images",
			"MaxSdk\\Resources\\Images.meta"
			#else
			"MaxSdk/Resources/Images",
			"MaxSdk/Resources/Images.meta"
			#endif
		};

		[MenuItem("LionStudios/Advanced/Run Post-Import Move")]
		private static void CopyLionFiles()
		{
			
#if LION_KIT_DEV
			return;
#else
			string pkgId = LionKit.PackageId;
			foreach (string copyPath in CopyPaths)
			{
				string pkgPath = PkgDir + "/" + pkgId + "/" + ImportMoveDir + "/" + copyPath;
				
				if (CheckExistence(pkgPath))
				{
					string destPath = DestDir + "/" + copyPath;

					if (NoOverwrite.Contains(copyPath) && CheckExistence(destPath))
					{
						continue;
					}
					
					// ReSharper disable once AssignNullToNotNullAttribute
					Directory.CreateDirectory(Path.GetDirectoryName(destPath));

					// replace files/folders
					FileUtil.DeleteFileOrDirectory(destPath);
					FileUtil.DeleteFileOrDirectory(destPath + ".meta");
					FileUtil.MoveFileOrDirectory(pkgPath, destPath);
					FileUtil.MoveFileOrDirectory(pkgPath + ".meta", destPath + ".meta");

					if (copyPath == "Plugins/Android/mainTemplate.gradle")
					{
						LionDebug.Log("mainTemplate.gradle updated - RESOLVING DEPENDENCIES");
						PlayServicesResolver.Resolve(null, true);
					}
				}
			}

			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
#endif
		}
		
		[UnityEditor.Callbacks.DidReloadScripts]
		private static void OnScriptsReloaded()
		{
			data = LionKitPostImportMover.PersistentData.Load();
#if !LION_KIT_DEV

			/// remove illegal directories
			if (IllegalDirectories.Count > 0)
			{
				string assetPath = UnityEngine.Application.dataPath;
				foreach (string asset in IllegalDirectories)
				{
					string path = Path.Combine(assetPath, asset);
					if (Directory.Exists(path))
					{
						Directory.Delete(path, true);
						
					}
				}
			}

			bool assetUpdateReq = false;
			foreach (string path in CopyPaths)
			{
				string destPath = DestDir + "/" + path;
				if (!CheckExistence(destPath))
				{
					assetUpdateReq = true;
				}
			}

			if (!string.IsNullOrEmpty(LionKit.Version))
			{
				// check prefs for version
				string lastInstalledVer = data.version;
				Version lastVer = Version.Parse(lastInstalledVer);
				Version installedVer = Version.Parse(LionKit.Version);
			
				// only copy files if version is newer or if files are missing
				if (installedVer > lastVer || assetUpdateReq)
				{
					if(assetUpdateReq)
					{
						LionDebug.Log("Missing asset(s). Reloading from packages...");
					}

					data.version = installedVer.ToString();
					data.SaveLocal();
					CopyLionFiles();
				}
			}
#endif

		}

		private static void FindElementsByName(string name, XContainer startingElement, ICollection<XElement> children)
		{
			foreach (var element in startingElement.Elements())
			{
				if (element.Name == name)
					children.Add(element);
				FindElementsByName(name, element, children);
			}
		}
		
		class DependencyVersionInfo
		{
			private Dictionary<string, Version> _VersionLookup = new Dictionary<string, Version>();
			public DependencyVersionInfo(string pathToDependency)
			{
				var docElement = XElement.Load(Path.GetFullPath(pathToDependency));
				AddVersionInfoAndroid(docElement);
				AddVersionInfoIOS(docElement);
			}

			private void AddVersionInfoAndroid(XElement docElement)
			{
				var packageElements = new List<XElement>();
				
				FindElementsByName("androidPackage", docElement, packageElements);
				foreach (var element in packageElements)
				{
					foreach (var attribute in element.Attributes())
					{
						if (attribute.Name == "spec")
						{
							var attrSplit = attribute.Value.Split(':');
							if (attrSplit.Length >= 2)
							{
								string sdkName = "";
								for (int i = 0; i < attrSplit.Length - 1; i++)
								{
									if (!string.IsNullOrEmpty(attrSplit[i]))
									{
										sdkName += attrSplit[i];
										if (i < attrSplit.Length - 2)
											sdkName += ":";
									}
								}

								if (Version.TryParse(attrSplit[attrSplit.Length - 1], out Version version))
									_VersionLookup[sdkName] = version;
							}
						}
					}
				}
			}
			
			private void AddVersionInfoIOS(XElement docElement)
			{
				var packageElements = new List<XElement>();
				FindElementsByName("iosPod", docElement, packageElements);
				foreach (var element in packageElements)
				{
					string sdkName = null;
					Version version = null;
					foreach (var attribute in element.Attributes())
					{
						if (attribute.Name == "name")
						{
							sdkName = attribute.Value;
						}
						if (attribute.Name == "version")
						{
							Version.TryParse(attribute.Value, out version);
						}
					}
					
					if (sdkName != null && version != null)
						_VersionLookup[sdkName] = version;
				}
			}
			
			/// <summary>
			/// Checks if the other dependency file is newer.
			/// Returns true if the other file is newer otherwise false.
			/// </summary>
			/// <param name="otherInfo"></param>
			/// <returns>Returns true if the other file is newer otherwise false.</returns>
			public bool CheckNewer(DependencyVersionInfo otherInfo)
			{
				foreach (var kvp in _VersionLookup)
				{
					if (otherInfo._VersionLookup.ContainsKey(kvp.Key))
					{
						if (otherInfo._VersionLookup[kvp.Key] > kvp.Value)
						{
							return true;
						}
					}
				}

				return false;
			}
		}

		private static bool CheckExistence(string location)
		{
			return File.Exists(location) ||
				   Directory.Exists(location) ||
				   location.EndsWith("/*") && Directory.Exists(Path.GetDirectoryName(location));
		}
	}
}