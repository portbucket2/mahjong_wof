using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEngine.Networking;
using LionStudios.Editor.PackageManager;
using LionStudios.Editor.Platform;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using PackageInfo = UnityEditor.PackageManager.PackageInfo;
using LionStudios.Facebook.Editor.Android;
using LionStudios.Runtime.IAP;
using AppLovinMax.Scripts.IntegrationManager.Editor;
using Network = AppLovinMax.Scripts.IntegrationManager.Editor.Network;

namespace LionStudios.Editor
{
    public class LionIntegrationManagerWindow : EditorWindow
    {
        const string documentationURL =
            "https://docs.google.com/document/d/1tBC-D8Uu_U1Whr-egvQSOoxrxuo9O8i-gLNb-7gZn7I/edit#heading=h.vnfup2fp5v8d";
        
        // Create window
        [MenuItem("LionStudios/Integration Settings")]
        static void Init()
        {
            LionIntegrationManagerWindow window =
                (LionIntegrationManagerWindow) GetWindow(typeof(LionIntegrationManagerWindow), true,
                    "Lion Integration Settings");
            window.Show();
        }

        private void OnEnable()
        {
            _Settings = new SerializedProps();
            ConnectGDPR();
            ConnectDebugger();

            if (File.Exists("Assets/Plugins/Android/AndroidManifest.xml") == false)
                LionManifestBuilder.GenerateManifest();

            if (string.IsNullOrEmpty(LionSettings.GDPR.AppName) &&
                string.IsNullOrEmpty(Application.productName) == false)
                LionSettings.GDPR.AppName = Application.productName;

            if (!EditorApplication.isCompiling)
            {
                SetApplovinSharedAttributes();
            }
            
            string packageId = LionKit.PackageId;
            if (string.IsNullOrEmpty(packageId))
            {
                packageId = "com.lionstudios.release.lionkit";
            }

            latestSearchRequest = Client.Search(packageId);
            Load();

            AssetDatabase.importPackageCompleted -= RefreshLocalPluginData;
            AssetDatabase.importPackageCompleted += RefreshLocalPluginData;
        }

        private void OnDisable()
        {
            AssetDatabase.importPackageCompleted -= RefreshLocalPluginData;
        }

        //static bool _PluginDataLoadFailed;
        static PluginData _PluginData;

        private static SerializedProps.UIEnabled uiEnabled;

        static SerializedProps.UIEnabled _UIEnabled
        {
            get
            {
                if (uiEnabled == null)
                {
                    uiEnabled = SerializedProps.UIEnabled.Load();
                }

                return uiEnabled;
            }
        }

        readonly static List<Network> _Interstitials = new List<Network>();
        readonly static List<Network> _InterstitialNetworksToUpdate = new List<Network>();
        readonly static List<Network> _Rewarded = new List<Network>();
        readonly static List<Network> _RewardedNetworksToUpdate = new List<Network>();
        readonly static List<Network> _Banners = new List<Network>();
        readonly static List<Network> _BannerNetworksToUpdate = new List<Network>();

        readonly static List<Network> _ExtraNetworks = new List<Network>();

        /// <summary>
        /// Loads the plugin data to be displayed by this window.
        /// </summary>
        static void Load()
        {
            AppLovinEditorCoroutine loadDataCoroutine = AppLovinEditorCoroutine.StartCoroutine(
                AppLovinIntegrationManager.Instance.LoadPluginData(data =>
                {
                    if (data == null)
                    {
                        //_PluginDataLoadFailed = true;
                        Debug.LogWarning("AppLovin Mediation Data failed to load");
                    }
                    else
                    {
                        _PluginData = data;
                        //_PluginDataLoadFailed = false;
                        //Debug.Log("Retrieved Plugin Data :: " + _PluginData.MediatedNetworks.Length);
                        RefreshLocalNetworkAdapterData();
                    }

                    //Repaint();
                }));
        }

        static void RefreshLocalPluginData(string packageName)
        {
            RefreshLocalNetworkAdapterData();
        }

        static bool _AdMobIntegrated;

        static void RefreshLocalNetworkAdapterData()
        {
            //Debug.Log("RefreshLocalPluginData - yodaaaa");
            _Interstitials.Clear();
            _InterstitialNetworksToUpdate.Clear();
            _Rewarded.Clear();
            _RewardedNetworksToUpdate.Clear();
            _Banners.Clear();
            _BannerNetworksToUpdate.Clear();
            _ExtraNetworks.Clear();
            _AdMobIntegrated = false;

            if (_PluginData == null || _PluginData.MediatedNetworks == null) return;

            for (int i = 0; i < _PluginData.MediatedNetworks.Length; i++)
            {
                var network = _PluginData.MediatedNetworks[i];
                
                #if !LION_KIT_DEV
                AppLovinIntegrationManager.UpdateCurrentVersions(network, Path.GetFullPath(
#if UNITY_EDITOR_WIN
                    $"Packages\\{LionKit.PackageId}\\MaxSdk\\"
#else
                    $"Packages/{LionKit.PackageId}/MaxSdk/"
#endif
                ));
#else
                AppLovinIntegrationManager.UpdateCurrentVersions(network, AppLovinIntegrationManager.DefaultPluginExportPath);
#endif

                //Debug.Log(network.Name + " :: " + network.DisplayName + " :: " + network.DownloadUrl);

                bool networkNeedsUpdate = string.IsNullOrEmpty(network.CurrentVersions.Unity) ||
                                          network.CurrentToLatestVersionComparisonResult ==
                                          MaxSdkUtils.VersionComparisonResult.Lesser;
                if (network.Name == "ADMOB_NETWORK") _AdMobIntegrated = true;

                //Debug.Log(network.CurrentVersions.Unity + " :: " + network.LatestVersions.Unity);
                bool unusedNetwork = true;
                if (AppLovin.Mediation.InterstitialNetworks.Contains(network.Name))
                {
                    unusedNetwork = false;
                    _Interstitials.Add(network);
                    if (networkNeedsUpdate)
                    {
                        _InterstitialNetworksToUpdate.Add(network);
                    }
                }

                if (AppLovin.Mediation.RewardedNetworks.Contains(network.Name))
                {
                    unusedNetwork = false;
                    _Rewarded.Add(network);
                    if (networkNeedsUpdate)
                    {
                        _RewardedNetworksToUpdate.Add(network);
                    }
                }

                if (AppLovin.Mediation.BannerNetworks.Contains(network.Name))
                {
                    unusedNetwork = false;
                    _Banners.Add(network);
                    if (networkNeedsUpdate)
                    {
                        _BannerNetworksToUpdate.Add(network);
                    }
                }

                if (unusedNetwork && string.IsNullOrEmpty(network.CurrentVersions.Unity) == false)
                {
                    _ExtraNetworks.Add(network);
                }
            }

            _TimeOfLastRefresh = System.DateTime.UtcNow;
        }

        private void ConnectGDPR()
        {
            if (LionSettings.GDPR.Prefab != null) return;

            var assetPaths = AssetDatabase.GetAllAssetPaths();
            foreach (var path in assetPaths)
            {
                var go = AssetDatabase.LoadAssetAtPath<GameObject>(path);

                if (!go) continue;

                var gdpr = go.GetComponent<GDPR>();
                if (gdpr == null) continue;

                _Settings.UpdateGDPR(gdpr);
                return;
            }
        }

        private void ConnectDebugger()
        {
            if (LionSettings.Debugging.Prefab != null)
            {
                return;
            }

            var assetPaths = AssetDatabase.GetAllAssetPaths();
            foreach (var path in assetPaths)
            {
                var go = AssetDatabase.LoadAssetAtPath<GameObject>(path);

                if (!go) continue;

                var debugger = go.GetComponent<LionStudios.Debugging.LionDebugger>();
                if (debugger == null) continue;

                _Settings.UpdateDebugger(debugger);
                return;
            }
        }

        private void SetApplovinSharedAttributes()
        {
            if (AppLovinSettings.Instance.SdkKey != LionSettings.AppLovin.SDKKey)
            {
                AppLovinSettings.Instance.SdkKey = LionSettings.AppLovin.SDKKey;
                AppLovinSettings.Instance.SaveAsync();
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }

            if (string.IsNullOrEmpty(AppLovinSettings.Instance.SdkKey))
            {
                AppLovinSettings.Instance.QualityServiceEnabled = false;
                AppLovinSettings.Instance.SaveAsync();
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }

            if (AppLovinSettings.Instance.QualityServiceEnabled != LionSettings.AppLovin.MaxAdReviewEnabled)
            {
                AppLovinSettings.Instance.QualityServiceEnabled = LionSettings.AppLovin.MaxAdReviewEnabled &&
                                                                  LionSettings.AppLovin.Enabled &&
                                                                  !string.IsNullOrEmpty(LionSettings.AppLovin.SDKKey);
                AppLovinSettings.Instance.SaveAsync();
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }

            if (_Settings._AndroidAdMobAppId.stringValue != AppLovinSettings.Instance.AdMobAndroidAppId &&
                string.IsNullOrEmpty(_Settings._AndroidAdMobAppId.stringValue))
            {
                _Settings._AndroidAdMobAppId.stringValue = AppLovinSettings.Instance.AdMobAndroidAppId;
                _Settings.ApplyModifiedProperties();
            }

            if (_Settings._iOSAdMobAppId.stringValue != AppLovinSettings.Instance.AdMobIosAppId &&
                string.IsNullOrEmpty(_Settings._iOSAdMobAppId.stringValue))
            {
                _Settings._iOSAdMobAppId.stringValue = AppLovinSettings.Instance.AdMobIosAppId;
                _Settings.ApplyModifiedProperties();
            }
        }

        SearchRequest latestSearchRequest = null;

        GUIStyle
            versionStyle; // = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleLeft, stretchWidth = false };

        GUIStyle updateBtnStyle; // = new GUIStyle(GUI.skin.button) { stretchWidth = false };

        void UpdateVersion()
        {
            if (versionStyle == null)
                versionStyle = new GUIStyle(GUI.skin.label) {alignment = TextAnchor.MiddleLeft, stretchWidth = false};

            if (updateBtnStyle == null) updateBtnStyle = new GUIStyle(GUI.skin.button) {stretchWidth = false};

            string version = LionKit.Version;
            if (string.IsNullOrEmpty(version)) version = "0.0.0";

            System.Version installedVer = new System.Version(version);
            System.Version latestVer = null;
            if (latestSearchRequest != null && latestSearchRequest.IsCompleted == true)
            {
                PackageInfo[] packages = latestSearchRequest.Result;

                if (packages != null && packages.Length > 0)
                {
                    latestVer = new System.Version(packages[0].version);
                }
            }

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Current Version", EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Latest Version", EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Min Req. Version", EditorStyles.boldLabel);
            EditorGUILayout.EndHorizontal();

            // version stuff
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(installedVer.ToString());
            EditorGUILayout.LabelField(latestVer != null ? latestVer.ToString() : "- - -");
            EditorGUILayout.LabelField("- - -");
            EditorGUILayout.EndHorizontal();

            // upgrade btn
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUI.enabled = latestVer != null && latestVer > installedVer;
            if (GUILayout.Button("Upgrade", GUILayout.MaxWidth(60)))
            {
                string pkg = string.Format("{0}@{1}", LionKit.PackageId, latestVer);
                Debug.Log("Lion Kit: Updating package " + pkg);
#if !LION_KIT_DEV
                Client.Add(pkg);
#endif
            }

            GUILayout.Space(postIndent);
            EditorGUILayout.EndHorizontal();
            GUI.enabled = true;
        }

        void UpdateLionKitProps()
        {
            using (new EditorGUILayout.VerticalScope("box"))
            {
                GUILayout.Space(1f);
                EditorGUILayout.BeginHorizontal();
                
                EditorGUILayout.LabelField("Lion Kit",
                    EditorStyles.boldLabel, GUILayout.MaxWidth(75));
                
                GUILayout.FlexibleSpace();

                bool autoUpdateEnabled = EditorPrefs.GetBool(LionSettings.AUTO_UPDATE_ENABLED_KEY);
                string autoUpdateEnabledTooltip =
                    "Enable this to automatically keep LionKit up-to-date.  Triggered when Unity loads or scripts are recompiled.";
                bool autoUpdateEnabledEdit = EditorGUILayout.ToggleLeft(
                    new GUIContent("Enable Auto Update", autoUpdateEnabledTooltip), autoUpdateEnabled,
                    GUILayout.MaxWidth(135));
                if (autoUpdateEnabled != autoUpdateEnabledEdit)
                {
                    EditorPrefs.SetBool(AppLovinAutoUpdater.KeyAutoUpdateEnabled, autoUpdateEnabledEdit);
                    EditorPrefs.SetBool(LionSettings.AUTO_UPDATE_ENABLED_KEY, autoUpdateEnabledEdit);
                }

                EditorGUILayout.EndHorizontal();

                GUILayout.Space(10f);
                GUILayout.BeginHorizontal();
                GUILayout.Space(7f);
                using (new EditorGUILayout.VerticalScope())
                {
                    UpdateVersion();
                }

                GUILayout.EndHorizontal();
            }
        }

        /// <summary>
        /// Downloads the plugin file for a given network.
        /// </summary>
        /// <param name="network">Network for which to download the current version.</param>
        /// <returns></returns>
        static public IEnumerator DownloadPlugin(Network network)
        {
            var path = Path.Combine(Application.temporaryCachePath, network.Name ); // TODO: Maybe delete plugin file after finishing import.
#if UNITY_2017_2_OR_NEWER
            var downloadHandler = new DownloadHandlerFile(path);
#else
            var downloadHandler = new AppLovinDownloadHandler(path);
#endif
            var webRequest = new UnityWebRequest(network.DownloadUrl)
            {
                method = UnityWebRequest.kHttpVerbGET, downloadHandler = downloadHandler
            };

#if UNITY_2017_2_OR_NEWER
            var operation = webRequest.SendWebRequest();
#else
            var operation = webRequest.Send();
#endif

            while (!operation.isDone)
            {
                yield return
                    new WaitForSeconds(
                        0.1f); // Just wait till webRequest is completed. Our coroutine is pretty rudimentary.
                //CallDownloadPluginProgressCallback(network.DisplayName, operation.progress, operation.isDone);
            }

#if UNITY_2017_2_OR_NEWER
            if (webRequest.isNetworkError || webRequest.isHttpError)
#else
            if (webRequest.isError)
#endif
            {
                Debug.LogError(webRequest.error);
            }
            else
            {
                //importingNetwork = network;
                AssetDatabase.ImportPackage(path, false);
            }

            webRequest = null;
        }

        static System.DateTime _TimeOfLastRefresh;

        private void OnGUI()
        {
            GUI.enabled = !EditorApplication.isCompiling;

            // Check if there have been any local updates
            if ((System.DateTime.UtcNow - _TimeOfLastRefresh).TotalSeconds > 3f) RefreshLocalNetworkAdapterData();

            _ScrollPos = EditorGUILayout.BeginScrollView(_ScrollPos);
            UpdateLionKitProps();
            _Settings.DrawSettings();
            EditorGUILayout.EndScrollView();
        }

        SerializedProps _Settings;

        Vector2 _ScrollPos;

        const float sectionSpace = 3f;
        const float topSpace = 1f;
        const float afterLabelSpace = 10f;
        const float indent = 7f;
        const float postIndent = 5f;
        const float bottomSpace = 10f;
        const float spacing = 7f;

        public class SerializedProps
        {
            Texture2D infoIcon
            {
                get
                {
                    return EditorGUIUtility.FindTexture("d__Help");
                }
            }
            
            public SerializedProps()
            {
                LionSettings settings = LionSettings.Get();
                _SerializedObj = new SerializedObject(settings);
                GetProps();
            }

            public void DrawSettings()
            {
                float startingWidth = EditorGUIUtility.labelWidth;
                EditorGUIUtility.labelWidth = 185f;
                GUILayout.Space(sectionSpace);
                UpdateGeneralProps();
                GUILayout.Space(sectionSpace);
                UpdateRemoteConfigProps();
                GUILayout.Space(sectionSpace);
                UpdateGDPRProps();
                GUILayout.Space(sectionSpace);
                UpdateAppLovinProps();
                GUILayout.Space(sectionSpace);
                UpdateFacebookProps();
                GUILayout.Space(sectionSpace);
                UpdateAdjustProps();
                GUILayout.Space(sectionSpace);
                UpdateFirebaseProps();
                GUILayout.Space(sectionSpace);
                UpdateInAppReviewProps();
                GUILayout.Space(sectionSpace);
                UpdateInAppPurchaseProps();
                GUILayout.Space(sectionSpace);
                UpdateDebuggerProps();

                if (ValidateProps())
                {
                    ApplyModifiedProperties();
                }

                EditorGUIUtility.labelWidth = startingWidth;
            }

            public void UpdateGDPR(GDPR gdpr)
            {
                _GdprPrefab.objectReferenceValue = gdpr;
                _GdprPrefab.serializedObject.ApplyModifiedProperties();
            }

            public void UpdateDebugger(Debugging.LionDebugger debugger)
            {
                _DebuggerPrefab.objectReferenceValue = debugger;
                _DebuggerPrefab.serializedObject.ApplyModifiedProperties();
            }

            void UpdateRemoteConfigProps()
            {
                using (new EditorGUILayout.VerticalScope("box"))
                {
                    GUILayout.Space(topSpace);
                    GUIContent section = new GUIContent("Remote Config",
                        "These values will control LionKit's behavior.  " +
                        "All values shown here can be configured on the AppLovin console and will be passed to the client at runtime.  " +
                        "Your Lion rep will help configure these values.");

                    if (SectionToggle(section, ref _UIEnabled.RemoteConfig))
                    {
                        GUILayout.Space(afterLabelSpace);
                        GUILayout.BeginHorizontal();
                        GUILayout.Space(indent);
                        using (new EditorGUILayout.VerticalScope()) // "box"))
                        {
                            GUILayout.Space(spacing);
                            const string minimumInterstitialIntervalTooltip =
                                "The minimum interval between interstitial ads.  " +
                                "If your game attempts to show an ad before the time interval has completed, no ad will be shown.";
                            DrawProperty(_MinimumInterstitialInterval, new GUIContent("Min Interstitial Interval", minimumInterstitialIntervalTooltip));

                            const string rvInterTimerTooltip =
                                "The minimum interval between showing a reward video and interstitial ad.  " +
                                "If your game attempts to show an ad before the time interval has completed, no ad will be shown.";
                            DrawProperty(_RVInterstitialTimer,
                                new GUIContent("RV Interstitial Timer", rvInterTimerTooltip));

                            const string interstitialStartTimerTooltip =
                                "The minimum time from first launch before showing an interstitial.  " +
                                "If your game attempts to show an interstitial ad before the time interval has completed, no ad will be shown.";
                            DrawProperty(_InterstitialStartTimer,
                                new GUIContent("Interstitial Start Timer", interstitialStartTimerTooltip));

                            const string interstitialStartLevelTooltip =
                                "The minimum level a user has to complete before seeing an interstitial.  " +
                                "If your game attempts to show an interstitial ad before the minimum level has been completed, no ad will be shown.";
                            DrawProperty(_InterstitialStartLevel,
                                new GUIContent("Interstitial Start Level", interstitialStartLevelTooltip));

                            GUILayout.Space(spacing);
                            string bannersDisabledTooltip =
                                "If enabled banners will not be shown.  NOTE: You still need to call `AdManager.Banner.Show()` even if this value is disabled.";
                            DrawProperty(_BannersDisabled,
                                new GUIContent("Banners Disabled", bannersDisabledTooltip));
                        }

                        GUILayout.Space(postIndent);
                        GUILayout.EndHorizontal();
                        GUILayout.Space(bottomSpace);
                    }

                    GUILayout.Space(bottomSpace);
                }
            }

            void UpdateGDPRProps()
            {
                using (new EditorGUILayout.VerticalScope("box"))
                {
                    GUILayout.Space(topSpace);
                    //EditorGUILayout.LabelField("GDPR", EditorStyles.boldLabel);
                    if (SectionToggle("GDPR", ref _UIEnabled.GDPR))
                    {
                        GUILayout.Space(afterLabelSpace);
                        GUILayout.BeginHorizontal();
                        GUILayout.Space(indent);
                        using (new EditorGUILayout.VerticalScope())
                        {
                            EditorGUI.BeginChangeCheck();
                            string gdprTooltip =
                                "GDPR only applies to some regions (Europe).  Leave this with its default prefab, unless you want to customize the look of your game's GDPR experience.";
                            DrawProperty(_GdprPrefab, new GUIContent("Prefab", gdprTooltip));

                            string gdprAppNameTooltip = "The user facing name of the app, defaults to product name.";
                            DrawProperty(_GdprAppName, new GUIContent("App Name", gdprAppNameTooltip));

                            string gdprHeightTooltip = "This sets the height of the fix it banner for GDPR.";
                            DrawProperty(_GdprFixItBannerHeight,
                                new GUIContent("Fix-It Banner Height", gdprHeightTooltip));
                            _GdprFixItBannerHeight.floatValue = Mathf.Clamp01(_GdprFixItBannerHeight.floatValue);

                            string gdprFontTooltip = "The font used for the GDPR prompts.";
                            DrawProperty(_GdprFont, new GUIContent("Font", gdprFontTooltip));

                            string gdprFontScaleTooltip = "The font scale used for the GDPR prompts.";
                            DrawProperty(_GdprFontScale,
                                new GUIContent("GDPR Font Scale", gdprFontScaleTooltip));

                            DrawProperty(_GdprTitleFontColor,
                                new GUIContent("Title Font Color", "The font color used for the GDPR title prompts."));

                            DrawProperty(_GdprBaseFontColor,
                                new GUIContent("Font Color", "The font color used for GDPR standard text."));

                            string gdprSecondaryColorTooltip = "The secondary color used for the GDPR prompts.";
                            DrawProperty(_GdprSecondaryColor,
                                new GUIContent("Secondary Color", gdprSecondaryColorTooltip));

                            string gdprShowBordersTooltip =
                                "Should the borders be shown (uses secondary color for border)";
                            DrawProperty(_GdprShowBorders,
                                new GUIContent("Show Borders", gdprShowBordersTooltip));
                            string gdprBackgroundColorTooltip = "The background color used for the GDPR prompts.";
                            DrawProperty(_GdprBackgroundColor,
                                new GUIContent("Background Color", gdprBackgroundColorTooltip));

                            if (EditorGUI.EndChangeCheck())
                            {
                                //Debug.Log("Changed GDPR");
                                LionSettings.GDPR.Prefab.UpdateFormatting();
                            }

                            string gdprPrivacyLinksTooltip = "Add any privacy policy html links for the app.";
                            DrawProperty(_GdprPrivacyLinks,
                                new GUIContent("Privacy Links", gdprPrivacyLinksTooltip));
                        }

                        GUILayout.Space(postIndent);
                        GUILayout.EndHorizontal();
                        GUILayout.Space(bottomSpace);
                    }

                    GUILayout.Space(bottomSpace);
                }
            }

            const string applovinRepProvidedTooltip =
                "Your Lion Developer Relations representative will provide you with this key!";
            
                        private GUIContent _PackageNameLabel = new GUIContent("Package Name", "aka: the bundle identifier");
            private GUIContent _ClassNameLabel = new GUIContent("Class Name", "aka: the activity name");

            private GUIContent _DebugAndroidKeyLabel = new GUIContent("Debug Android Key Hash",
                "Copy this key to the Facebook Settings in order to test a Facebook Android app");

            GUIStyle _Style_FBNativeAndroidAppSettings;

            private void AndroidUtilGUI()
            {
                //_ShowFBAndroidUtils = EditorGUILayout.Foldout(_ShowFBAndroidUtils, "Android Build Facebook Settings");
                //if (_ShowFBAndroidUtils)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(-1f);
                    using (new EditorGUILayout.VerticalScope("box"))
                    {
                        EditorGUILayout.LabelField("Android Build Facebook Settings", EditorStyles.boldLabel);

                        if (_Style_FBNativeAndroidAppSettings == null)
                        {
                            _Style_FBNativeAndroidAppSettings = new GUIStyle(EditorStyles.label);
                            _Style_FBNativeAndroidAppSettings.fontSize = 10;
                            //_Style_FBNativeAndroidAppSettings.normal.textColor = GUI.contentColor;// Color.white;
                        }
                        //Debug.Log(GUI.color);

                        EditorGUILayout.LabelField(
                            "Copy and Paste these into your \"Native Android App\" Settings on developers.facebook.com/apps",
                            _Style_FBNativeAndroidAppSettings);

                        GUILayout.BeginHorizontal();
                        GUILayout.Space(5f);
                        using (new EditorGUILayout.VerticalScope("box"))
                        {
                            SelectableLabelField(_PackageNameLabel,
                                Application.identifier); // GetApplicationIdentifier();
                            SelectableLabelField(_ClassNameLabel, ManifestMod.DeepLinkingActivityName);
                            SelectableLabelField(_DebugAndroidKeyLabel, FacebookAndroidUtil.DebugKeyHash);
                        }

                        GUILayout.EndHorizontal();
                        //if (GUILayout.Button("Regenerate Android Manifest"))
                        //{
                        //    ManifestMod.GenerateManifest();
                        //}
                    }

                    //GUILayout.Space(postIndent);
                    GUILayout.EndHorizontal();
                }

                EditorGUILayout.Space();
            }

            private void SelectableLabelField(GUIContent label, string value)
            {
                if (_Style_FBNativeAndroidAppSettings == null)
                {
                    _Style_FBNativeAndroidAppSettings = new GUIStyle(EditorStyles.label);
                    _Style_FBNativeAndroidAppSettings.fontSize = 10;
                    //_Style_FBNativeAndroidAppSettings.normal.textColor = GUI.contentColor;// Color.white;
                }

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(label, _Style_FBNativeAndroidAppSettings, GUILayout.Width(180),
                    GUILayout.Height(16));
                EditorGUILayout.SelectableLabel(value, _Style_FBNativeAndroidAppSettings, GUILayout.Height(16));
                EditorGUILayout.EndHorizontal();
            }

            private void DisplayMediationIntegration(string type, SerializedProperty propA, SerializedProperty propB,
                List<Network> networksToUpdate, List<Network> networks)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(-1f);
                using (new EditorGUILayout.VerticalScope("box"))
                {
                    EditorGUILayout.LabelField(type, EditorStyles.boldLabel);
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5f);
                    using (new EditorGUILayout.VerticalScope())
                    {
                        DrawProperty(propB, new GUIContent("Ad Unit Id - Android", applovinRepProvidedTooltip));
                        DrawProperty(propA, new GUIContent("Ad Unit Id - iOS", applovinRepProvidedTooltip));
                        
                        if (string.IsNullOrEmpty(propA.stringValue) == false ||
                            string.IsNullOrEmpty(propB.stringValue) == false)
                        {
                            GUILayout.Space(5f);
                            bool wasEnabled = GUI.enabled;
                            GUI.enabled &= networksToUpdate.Count > 0;

                            using (new EditorGUILayout.HorizontalScope())
                            {
                                EditorGUILayout.LabelField("");
                                string state = networksToUpdate.Count > 0
                                    ? "Update " + type + " Mediated Networks"
                                    : type + " Mediated Networks Installed";
                                string networksNames = "";
                                if (networksToUpdate.Count > 0)
                                {
                                    for (int i = 0; i < networksToUpdate.Count; i++)
                                    {
                                        networksNames += networksToUpdate[i].DisplayName;
                                        if (i < networksToUpdate.Count - 1) networksNames += ", ";
                                    }
                                }
                                else
                                {
                                    networksNames += "AppLovin, ";
                                    for (int i = 0; i < networks.Count; i++)
                                    {
                                        networksNames += networks[i].DisplayName;
                                        if (i < networks.Count - 1) networksNames += ", ";
                                    }
                                }

                                var content = new GUIContent(state, networksNames);
                                if (GUILayout.Button(content, EditorStyles.miniButton, GUILayout.Width(240)))
                                {
                                    foreach (Network network in networksToUpdate)
                                        AppLovinEditorCoroutine.StartCoroutine(DownloadPlugin(network));
                                }
                            }

                            GUI.enabled = wasEnabled;
                            GUILayout.Space(5f);
                        }
                    }

                    GUILayout.EndHorizontal();
                }

                GUILayout.EndHorizontal();
            }
            
            private void DrawProperty(SerializedProperty property, GUIContent content)
            {
                if (_invalidProps.Contains(property))
                {
                    GUI.color = Color.red;
                }

                EditorGUILayout.PropertyField(property, content);
                GUI.color = Color.white;
            } 

            void DisplayExtraNetworks()
            {
                if (_ExtraNetworks.Count > 0)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(-1f);
                    using (new EditorGUILayout.VerticalScope("box"))
                    {
                        EditorGUILayout.LabelField("Extra Networks", EditorStyles.boldLabel);
                        GUILayout.BeginHorizontal();
                        GUILayout.Space(5f);
                        using (new EditorGUILayout.VerticalScope())
                        {
                            bool refresh = false;
                            foreach (Network network in _ExtraNetworks)
                            {
                                GUILayout.BeginHorizontal();
                                EditorGUILayout.LabelField(network.DisplayName);
                                if (GUILayout.Button(" - ", EditorStyles.miniButton, GUILayout.Width(30)))
                                {
                                    UnityEditor.EditorUtility.DisplayProgressBar("Integration Manager",
                                        "Deleting " + network.Name + "...", 0.5f);
                                    foreach (var pluginFilePath in network.PluginFilePaths)
                                    {
                                        FileUtil.DeleteFileOrDirectory(Path.Combine("Assets", pluginFilePath));
                                        FileUtil.DeleteFileOrDirectory(Path.Combine("Assets", pluginFilePath) +
                                                                       ".meta");
                                    }

                                    refresh = true;

                                    // Refresh UI
                                    AssetDatabase.Refresh();
                                    UnityEditor.EditorUtility.ClearProgressBar();
                                }

                                GUILayout.EndHorizontal();
                            }

                            GUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("");
                            if (GUILayout.Button("Delete All", EditorStyles.miniButton, GUILayout.Width(100f)))
                            {
                                foreach (Network network in _ExtraNetworks)
                                {
                                    UnityEditor.EditorUtility.DisplayProgressBar("Integration Manager",
                                        "Deleting " + network.Name + "...", 0.5f);
                                    foreach (var pluginFilePath in network.PluginFilePaths)
                                    {
                                        FileUtil.DeleteFileOrDirectory(Path.Combine("Assets", pluginFilePath));
                                        FileUtil.DeleteFileOrDirectory(Path.Combine("Assets", pluginFilePath) +
                                                                       ".meta");
                                    }

                                    refresh = true;

                                    // Refresh UI
                                    AssetDatabase.Refresh();
                                    UnityEditor.EditorUtility.ClearProgressBar();
                                }
                            }

                            if (refresh) RefreshLocalNetworkAdapterData();

                            GUILayout.EndHorizontal();
                        }

                        GUILayout.EndHorizontal();
                    }

                    GUILayout.EndHorizontal();
                }
            }

            void UpdateAppLovinProps()
            {
                bool applovinEnabled = _AppLovinEnabled.boolValue;
                string appLovinSdkKey = _AppLovinSDKKey.stringValue;
                bool maxAdReviewEnabled = _MaxAdReviewEnabled.boolValue;
                using (new EditorGUILayout.VerticalScope("box"))
                {
                    GUILayout.Space(topSpace);
                    if (SectionToggle("AppLovin", ref _UIEnabled.AppLovin, _AppLovinEnabled))
                    {
                        if (_AppLovinEnabled.boolValue)
                        {
                            GUILayout.Space(afterLabelSpace);
                        GUILayout.BeginHorizontal();
                        GUILayout.Space(indent);
                        using (new EditorGUILayout.VerticalScope()) // "box"))
                        {
                            DrawProperty(_AppLovinSDKKey, new GUIContent("SDK Key", applovinRepProvidedTooltip));
                            DrawProperty(_MaxAdReviewEnabled,new GUIContent("Enable MAX Ad Review", "Enable to allow MAX Ad Review"));

                            _BannerAdBackgroundColor.colorValue = EditorGUILayout.ColorField(
                                new GUIContent("Banner Ad Background Color",
                                    "This is the color that will appear on the background behind the banner. " +
                                    "This color must contrast your game screen color.  " +
                                    "For example if your game is generally dark, " +
                                    "then you would choose a white banner background color"),
                                _BannerAdBackgroundColor.colorValue);

                            GUILayout.Space(spacing);
                            DisplayMediationIntegration("Rewarded", _iOSRewardedAdUnitId, _AndroidRewardedAdUnitId,
                                _RewardedNetworksToUpdate, _Rewarded);
                            DisplayMediationIntegration("Interstitial", _iOSInterstitialAdUnitId,
                                _AndroidInterstitialAdUnitId, _InterstitialNetworksToUpdate, _Interstitials);
                            DisplayMediationIntegration("Banner", _iOSBannerAdUnitId, _AndroidBannerAdUnitId,
                                _BannerNetworksToUpdate, _Banners);
                            DisplayExtraNetworks();

                            if (_AdMobIntegrated)
                            {
                                GUILayout.BeginHorizontal();
                                GUILayout.Space(-1f);
                                using (new EditorGUILayout.VerticalScope("box"))
                                {
                                    EditorGUILayout.LabelField("Google AdMob", EditorStyles.boldLabel);
                                    GUILayout.BeginHorizontal();
                                    GUILayout.Space(5f);
                                    using (new EditorGUILayout.VerticalScope())
                                    {
                                        DrawProperty(_AndroidAdMobAppId, new GUIContent("App Id - Android", applovinRepProvidedTooltip));
                                        DrawProperty(_iOSAdMobAppId, new GUIContent("App Id - iOS", applovinRepProvidedTooltip));
                                    }

                                    GUILayout.EndHorizontal();
                                    GUILayout.Space(5f);
                                }

                                GUILayout.EndHorizontal();
                            }

                            GUILayout.Space(spacing);

                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("");
                            string applovinIntegrationManagerTooltip =
                                "Opens the window to manage your Applovin Max Plugins and Mediation Networks.  " +
                                "You can also find this window by selecting 'Applovin > Integration Manager' in the toolbar";

                            if (GUILayout.Button(
                                new GUIContent("Applovin Integration Manager", applovinIntegrationManagerTooltip),
                                EditorStyles.miniButton, GUILayout.Width(200)))
                                AppLovinIntegrationManagerWindow.ShowManager();

                            //EditorGUILayout.LabelField("", GUILayout.Width(10));
                            EditorGUILayout.EndHorizontal();
                        }

                        GUILayout.Space(postIndent);
                        GUILayout.EndHorizontal();
                        }
                        
                    }

                    GUILayout.Space(bottomSpace);
                }

                if (applovinEnabled != _AppLovinEnabled.boolValue ||
                    maxAdReviewEnabled != _MaxAdReviewEnabled.boolValue ||
                    appLovinSdkKey != _AppLovinSDKKey.stringValue)
                {
                    _SerializedObj.ApplyModifiedProperties();
                    AppLovinSettings.Instance.SdkKey = _AppLovinSDKKey.stringValue;
                    AppLovinSettings.Instance.QualityServiceEnabled = _AppLovinEnabled.boolValue &&
                                                                      string.IsNullOrEmpty(_AppLovinSDKKey
                                                                          .stringValue) == false &&
                                                                      _MaxAdReviewEnabled.boolValue;

                    AppLovinSettings.Instance.SaveAsync();
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();

                    if (_AppLovinEnabled.boolValue && _MaxAdReviewEnabled.boolValue &&
                        AppLovinSettings.Instance.QualityServiceEnabled != _MaxAdReviewEnabled.boolValue)
                    {
                        Debug.LogErrorFormat("You have enabled MAX Ad Review but your Applovin Sdk Key is empty, " +
                                             "so MAX Ad Review will not be enabled until you provide an Applovin Sdk Key in the LionKit Settings");
                    }
                }

                if (AppLovinSettings.Instance.AdMobAndroidAppId != _AndroidAdMobAppId.stringValue)
                {
                    AppLovinSettings.Instance.AdMobAndroidAppId = _AndroidAdMobAppId.stringValue;
                    AppLovinSettings.Instance.SaveAsync();
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                }

                if (AppLovinSettings.Instance.AdMobIosAppId != _iOSAdMobAppId.stringValue)
                {
                    AppLovinSettings.Instance.AdMobIosAppId = _iOSAdMobAppId.stringValue;
                    AppLovinSettings.Instance.SaveAsync();
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                }
            }
            void UpdateFacebookProps()
            {
                using (new EditorGUILayout.VerticalScope("box"))
                {
                    GUILayout.Space(topSpace);
                    if (SectionToggle("Facebook", ref _UIEnabled.Facebook, _FacebookEnabled))
                    {
                        if (_FacebookEnabled.boolValue)
                        {
                            GUILayout.Space(afterLabelSpace);
                            GUILayout.BeginHorizontal();
                            GUILayout.Space(indent);
                            using (new EditorGUILayout.VerticalScope()) // "box"))
                            {
                                // TODO: Add check for valid facebook app id
                                // grab this functionality from unity fb sdk source code

                                string fbAppId = _FacebookAppId.stringValue;
                                string facebookAppIdTooltip =
                                    "The Facebook App Id for your App, which can be found at 'developers.facebook.com/apps/'";
                                DrawProperty(_FacebookAppId, new GUIContent("Facebook App Id", facebookAppIdTooltip));
                                if (fbAppId != _FacebookAppId.stringValue)
                                {
                                    _FacebookAppId.serializedObject.ApplyModifiedProperties();
                                    LionManifestBuilder.GenerateManifest();
                                }

                                if (string.IsNullOrEmpty(_FacebookAppName.stringValue))
                                    _FacebookAppName.stringValue = Application.productName;

                                string fbAppName = _FacebookAppName.stringValue;
                                string facebookAppNameTooltip =
                                    "The Facebook App Name for your App, which can be found as the 'Display Name' in the basic settings for your app at 'developers.facebook.com/apps/'";
                                DrawProperty(_FacebookAppName, new GUIContent("Facebook App Name", facebookAppNameTooltip));
                                if (fbAppName != _FacebookAppName.stringValue)
                                {
                                    _FacebookAppName.serializedObject.ApplyModifiedProperties();
                                    LionManifestBuilder.GenerateManifest();
                                }

                                const string facebookAutoLogEventsTooltip =
                                    "Enable to automatically log Facebook App Events with Facebook";
                                bool fbAutoLogAppEvents = _FacebookAutoLogAppEvents.boolValue;
                                DrawProperty(_FacebookAutoLogAppEvents, new GUIContent("Auto Log App Events", facebookAutoLogEventsTooltip));
                                if (fbAutoLogAppEvents != _FacebookAutoLogAppEvents.boolValue)
                                {
                                    _FacebookAutoLogAppEvents.serializedObject.ApplyModifiedProperties();
                                    LionManifestBuilder.GenerateManifest();
                                }

                                GUILayout.Space(spacing);
                                AndroidUtilGUI();
                            }

                            GUILayout.Space(postIndent);
                            GUILayout.EndHorizontal();
                        }
                    }

                    GUILayout.Space(bottomSpace);
                }
            }
            void UpdateAdjustProps()
            {
                using (new EditorGUILayout.VerticalScope("box"))
                {
                    GUILayout.Space(topSpace);
                    if (SectionToggle("Adjust", ref _UIEnabled.Adjust, _AdjustEnabled))
                    {
                        if (_AdjustEnabled.boolValue)
                        {
                            GUILayout.Space(afterLabelSpace);
                            GUILayout.BeginHorizontal();
                            GUILayout.Space(indent);
                            using (new EditorGUILayout.VerticalScope()) // "box"))
                            {
                                string adjustTokenTooltip = "The alphanumeric Adjust App Token for this app. " +
                                                            "This is provided when creating your game in the Lion Portal, and can also be " +
                                                            "found at dash.adjust.com by clicking on the up arrow below your app's listing.  " +
                                                            "Be sure you are using the appropriate platform for your App, which can be " +
                                                            "verified by clicking 'All Settings' in your App listing.\nEx: dh3uh0firabk";

                                DrawProperty(_AndroidAdjustToken,
                                    new GUIContent("Android Adjust Token", infoIcon, adjustTokenTooltip));

                                DrawProperty(_iOSAdjustToken,
                                    new GUIContent("iOS Adjust Token", infoIcon, adjustTokenTooltip));

                                DrawProperty(_AdjustSandboxMode, new GUIContent("Is Sandbox",
                                    "Sandbox mode will send all events " +
                                    "to the sandbox environment and should be used for all test and devopment builds"));
                            }

                            GUILayout.Space(postIndent);
                            GUILayout.EndHorizontal();
                        }
                    }

                    GUILayout.Space(bottomSpace);
                }
            }
            void DrawFirebaseProperties()
            {
                if (_Style_FBNativeAndroidAppSettings == null)
                {
                    _Style_FBNativeAndroidAppSettings = new GUIStyle(EditorStyles.label);
                    _Style_FBNativeAndroidAppSettings.fontSize = 10;
                    //_Style_FBNativeAndroidAppSettings.normal.textColor = GUI.contentColor;// Color.white;
                }

                GUILayout.Space(afterLabelSpace);
                GUILayout.BeginHorizontal();
                GUILayout.Space(indent);
                using (new EditorGUILayout.VerticalScope()) // "box"))
                {
                    EditorGUILayout.LabelField(
                        "Please ensure that google service files have been added to the project before building.\n" +
                        "Ask your Lion Rep for these files if Firebase is required.", _Style_FBNativeAndroidAppSettings,
                        GUILayout.Height(28));

                    DrawProperty(_Firebase_RegisterStandardEvents, null);
                    DrawProperty(_Firebase_RegisterUAEvents, null);
                }

                GUILayout.Space(postIndent);
                GUILayout.EndHorizontal();
            }
            void UpdateFirebaseProps()
            {
                using (new EditorGUILayout.VerticalScope("box"))
                {
                    GUILayout.Space(topSpace);
                    if (SectionToggle("Firebase", ref _UIEnabled.Firebase, _FirebaseEnabled))
                    {
                        if (_FirebaseEnabled.boolValue)
                        {
#if !LK_USE_FIREBASE
                            string firebaseNotInstalledMsg =
                                $"Warning: Firebase package(s) not installed. Click the install button below.";
                            GUILayout.Label(firebaseNotInstalledMsg, EditorStyles.miniLabel);
                            if (GUILayout.Button("Install Firebase"))
                            {
                                SDKDownload.InstallFirebase();
                            }
#else
                            DrawFirebaseProperties();
#endif
                        }
                        else
                        {
#if LK_USE_FIREBASE
                            string firebaseInstalledWarning =
                                $"Warning: Firebase is disabled but still installed. To remove the Firebase package(s) click the uninstall button below.";
                            if (GUILayout.Button("Uninstall Firebase"))
                            {
                                SDKDownload.UninstallFirebase();
                            }   
#else
                            string firebaseInfo =
                                "Lion Kit allows the use of Firebase analytics for your game via Lion Kit API. (See documentation for more)\nEnable Firebase to begin. ";
                            GUILayout.Label(firebaseInfo);
#endif
                        }
                    }

                    GUILayout.Space(bottomSpace);
                }
            }
            void UpdateDebuggerProps()
            {
                using (new EditorGUILayout.VerticalScope("box"))
                {
                    GUILayout.Space(topSpace);
                    if (SectionToggle("Debugging", ref _UIEnabled.Debugging))
                    {
                        GUILayout.Space(afterLabelSpace);
                        GUILayout.BeginHorizontal();
                        GUILayout.Space(indent);

                        using (new EditorGUILayout.VerticalScope()) // "box"))
                        {
                            string lionDebuggerTooltip = "Toggling this will enable the Lion Runtime Debugger." +
                                                         "Swiping up with three fingers anywhere on the screen will open the debugger.";

                            string showAtStartupTooltip =
                                "Toggling this will show the Lion Runtime Debugger when your app starts.";

                            string debuggingLoggingTooltip = "What level of output to display from Lion Kit" +
                                                             "None ~ Hide all output from Lion Kit" +
                                                             "Default ~ Log standard output" +
                                                             "Event ~ Log only Analytics/Ads events";

                            if (_DebuggerEnabled.boolValue == true)
                            {
                                Color oldColor = GUI.color;
                                GUI.color = Color.white;
                                EditorGUILayout.LabelField(
                                    "NOTE: The debugger will NOT be present in builds installed from the store.",
                                    EditorStyles.helpBox);
                                GUI.color = oldColor;
                            }

                            DrawProperty(_DebuggerEnabled, new GUIContent("Enable Lion Runtime Debugger", infoIcon, lionDebuggerTooltip));
                            if (_DebuggerEnabled.boolValue == true)
                            {
                                DrawProperty(_DebuggerShowAtStartup, new GUIContent("Show At Startup", infoIcon, showAtStartupTooltip));
                            }
                            
                            DrawProperty(_DebugLogLevel, new GUIContent("Logging Level", infoIcon, debuggingLoggingTooltip));
                        }

                        GUILayout.Space(postIndent);
                        GUILayout.EndHorizontal();
                    }

                    GUILayout.Space(bottomSpace);
                }
            }
            void UpdateGeneralProps()
            {
                using (new EditorGUILayout.VerticalScope("box"))
                {
                    GUILayout.Space(topSpace);
                    if (SectionToggle("General", ref _UIEnabled.General))
                    {
                        float width = EditorGUIUtility.labelWidth;
                        EditorGUIUtility.labelWidth = 210f;
                        EditorGUILayout.Space(
#if UNITY_2019_4_OR_NEWER
                            topSpace
#endif
                        );

                        using (new EditorGUILayout.VerticalScope("HelpBox"))
                        {
                            EditorGUILayout.LabelField("Android", EditorStyles.miniBoldLabel);

                            string gradlePluginVersionTooltip =
                                "We recommend using the gradle plugin that ships with Unity. However, in some cases, overriding may be necessary.";
                            DrawProperty(_OverrideGradlePluginVersion,new GUIContent("Override Gradle Plugin Version", infoIcon, gradlePluginVersionTooltip));
                            if (_OverrideGradlePluginVersion.boolValue)
                            {
                                string pluginVersion = EditorGUILayout.DelayedTextField("Gradle Plugin Version (Advanced)",
                                    _GradlePluginVersion.stringValue);
                                if (!string.Equals(pluginVersion, _GradlePluginVersion.stringValue))
                                {
                                    LionSettings.GeneralSettings.GradlePluginOverride = pluginVersion;
                                    _GradlePluginVersion.stringValue =
                                        LionSettings.GeneralSettings.GradlePluginOverride;
                                    ApplyModifiedProperties();
                                }
                            }

                            /// multi-dex support
                            string multidextooltip =
                                "Multi-dex consolidates package dependencies to reduce package size";
                            DrawProperty(_UseMultiDexSupport,
                                new GUIContent("Enable Multi-Dex Support", infoIcon, multidextooltip));

                            /// proguard
                            string proguardTooltip =
                                "Proguard minification reduces the size of your apk by reducing duplicate references.";
                            GUIContent proguardLabel = new GUIContent("Enable Proguard Minification", infoIcon, proguardTooltip);
                            bool useProguard =
                                EditorGUILayout.Toggle(proguardLabel, _UseProGuard.boolValue);
                            if (useProguard != _UseProGuard.boolValue)
                            {
                                LionSettings.GeneralSettings.UseProGuard = useProguard;
                                _UseProGuard.boolValue = useProguard;
                                ApplyModifiedProperties();
                            }

                            // EditorGUILayout.BeginHorizontal();
                            // EditorGUILayout.LabelField("");
                            EditorGUILayout.Space();
                            if (GUILayout.Button("Regenerate Android Manifest", EditorStyles.miniButton,
                                GUILayout.Width(250)))
                            {
                                ManifestMod.UpdateAndroidManifest();
                                AssetDatabase.SaveAssets();
                                AssetDatabase.Refresh();
                            }

                            // EditorGUILayout.EndHorizontal();
                        }

                        
                        EditorGUIUtility.labelWidth = width;
                        EditorGUILayout.Space(
#if UNITY_2019_4_OR_NEWER
                            bottomSpace
#endif
                        );
                        
                        if (GUILayout.Button("Go To Integration Guide", EditorStyles.linkLabel))
                        {
                            Application.OpenURL(documentationURL);
                        }
                        
                        GUILayout.Space(postIndent);
                    }
                    GUILayout.Space(bottomSpace);
                }
            }
            void UpdateInAppPurchaseProps()
            {
                using (new EditorGUILayout.VerticalScope("box"))
                {
                    bool oldIAPEnabled = _InAppPurchaseEnabled.boolValue;
                    if (SectionToggle("In-App Purchases (Beta)", ref _UIEnabled.InAppPurchase, _InAppPurchaseEnabled))
                    {
                        GUI.color = Color.yellow;
                        EditorGUILayout.LabelField("WARNING: In-App Purchases is currently in beta. Some features may be incomplete or prone to errors.");
                        GUI.color = Color.white;
                        
                        if (_InAppPurchaseEnabled.boolValue)
                        {
                            if (LionIAP.IAP_Service_Enabled())
                            {
                                if (LionIAP.IAP_Purchasing_Installed())
                                {
                                    string validatePurchasesTooltip =
                                        "Validate purchases to prevent cheating. This requires Adjust Purchase to be setup";
                                    DrawProperty(_ValidatePurchases, new GUIContent("Validate Purchases", infoIcon, validatePurchasesTooltip));
                                    DrawProperty(_AllowRestorePurchases, null);

                                    if (GUILayout.Button("Manage IAP Products"))
                                    {
                                        IAPProductManagerWindow.OpenWindow();
                                    }
                                }
                                else
                                {
                                    string installPurchasingWarning =
                                        "In-App Purchase UnityPackage not installed. Click here to navigate to the Services tab to import the package.";
                                    if (GUILayout.Button(installPurchasingWarning, EditorStyles.linkLabel))
                                    {
                                        EditorApplication.ExecuteMenuItem("Window/General/Services");
                                    }
                                }
                            }
                            else
                            {
                                string enableServicesWarning =
                                    "In-App Purchases Service is disabled. It must be enabled before using Lion Kit IAP. Click here to navigate to services."
                                    + "\n(You may need to save your project after enabling the service.)";
                                if (GUILayout.Button(enableServicesWarning, EditorStyles.linkLabel))
                                {
                                    EditorApplication.ExecuteMenuItem("Window/General/Services");
                                }
                            }

                            GUILayout.Space(bottomSpace);
                        }
                    }
                    
                    if (_InAppPurchaseEnabled.boolValue != oldIAPEnabled)
                    {
                        UnityEditor.Compilation.CompilationPipeline.RequestScriptCompilation();
                    }
                }
            }
            void UpdateInAppReviewProps()
            {
                using (new EditorGUILayout.VerticalScope("box"))
                {
                    bool oldAppReviewEnabled = _InAppReviewEnabled.boolValue;
                    if (SectionToggle("In-App Review", ref _UIEnabled.InAppReview, _InAppReviewEnabled))
                    {
                        if (_InAppReviewEnabled.boolValue)
                        {
                            if (!SDKDownload.HasInAppReview())
                            {
                                const string inAppReviewNotInstalledMsg =
                                    "In-App Review package not installed. Press the button below to install.";

                                GUILayout.Label(inAppReviewNotInstalledMsg);
                                if (GUILayout.Button("Install In-App Review Package"))
                                {
                                    SDKDownload.InstallAppReview();
                                }
                            }
                            else
                            {
                                const string inAppReviewInstalledMsg =
                                    "In-App Review is installed! Use LionStudios.LionInAppReview.TryGetReview() to prompt app review.";
                                if (GUILayout.Button(inAppReviewInstalledMsg, EditorStyles.linkLabel))
                                {
                                    Application.OpenURL(documentationURL);
                                }
                            }
                        }
                    }
                    if (_InAppReviewEnabled.boolValue != oldAppReviewEnabled)
                    {
                        UnityEditor.Compilation.CompilationPipeline.RequestScriptCompilation();
                    }
                }
            }

            bool SectionToggle(string sectionName, ref bool uiEnabled, SerializedProperty sdkEnabled = null)
            {
                return SectionToggle(new GUIContent(sectionName), ref uiEnabled, sdkEnabled);
            }

            bool SectionToggle(GUIContent section, ref bool uiEnabled, SerializedProperty sdkEnabled = null)
            {
                EditorGUILayout.BeginHorizontal();

                section.text = ((uiEnabled == true) ? "[-] " : "[+] ") + section.text;
                if (GUILayout.Button(section, EditorStyles.boldLabel))
                {
                    uiEnabled = !uiEnabled;
                    _UIEnabled.SaveLocal();
                }

                bool sdkEnabledBool = true;
                if (sdkEnabled != null)
                {
                    sdkEnabledBool = sdkEnabled.boolValue;
                    string label = sdkEnabled.boolValue ? "Disable" : "Enable";
                    if (GUILayout.Button(label, EditorStyles.miniButton, GUILayout.Width(60f)))
                    {
                        sdkEnabled.boolValue = !sdkEnabled.boolValue;
                        sdkEnabled.serializedObject.ApplyModifiedProperties();
                    }
                }

                GUILayout.Space(postIndent);
                EditorGUILayout.EndHorizontal();
                return uiEnabled;
            }

            public class UIEnabled : PersistentData<UIEnabled>
            {
                public bool General;
                public bool RemoteConfig;
                public bool GDPR;
                public bool AppLovin;
                public bool Facebook;
                public bool Adjust;
                public bool Firebase;
                public bool InAppPurchase;
                public bool InAppReview;
                public bool Debugging;
            }

            public void ApplyModifiedProperties()
            {
                _SerializedObj.ApplyModifiedProperties();
            }
            
            #region Retrieve Settings Properties
            void GetGeneralProps()
            {
                _OverrideGradlePluginVersion = _SerializedObj.FindProperty("_General._OverrideGradlePluginVersion");
                _GradlePluginVersion = _SerializedObj.FindProperty("_General._GradlePluginOverride");
                _UseMultiDexSupport = _SerializedObj.FindProperty("_General._UseMultiDexSupport");
                _UseProGuard = _SerializedObj.FindProperty("_General._UseProGuard");
            }

            void GetInAppPurchaseProps()
            {
                _InAppPurchaseEnabled = _SerializedObj.FindProperty("_InAppPurchase._Enabled");
                _ValidatePurchases = _SerializedObj.FindProperty("_InAppPurchase._ValidatePurchases");
                _AllowRestorePurchases = _SerializedObj.FindProperty("_InAppPurchase._AllowRestorePurchases");
            }

            void GetInAppReviewProps()
            {
                _InAppReviewEnabled = _SerializedObj.FindProperty("_InAppReview._Enabled");
            }

            void GetFirebaseProps()
            {
                _FirebaseEnabled = _SerializedObj.FindProperty("_Firebase._Enabled");
                _Firebase_RegisterStandardEvents = _SerializedObj.FindProperty("_Firebase._RegisterForStandardEvents");
                _Firebase_RegisterUAEvents = _SerializedObj.FindProperty("_Firebase._RegisterForUAEvents");
            }

            void GetAdjustProps()
            {
                _AdjustEnabled = _SerializedObj.FindProperty("_Adjust._Enabled");
                _AdjustIsSandbox = _SerializedObj.FindProperty("_Adjust._IsSandbox");
                _AdjustSandboxMode = _SerializedObj.FindProperty("_Adjust._SandboxMode");
                _iOSAdjustToken = _SerializedObj.FindProperty("_Adjust._iOSToken");
                _AndroidAdjustToken = _SerializedObj.FindProperty("_Adjust._AndroidToken");
            }

            void GetFacebookProps()
            {
                _FacebookEnabled = _SerializedObj.FindProperty("_Facebook._Enabled");
                _FacebookAppId = _SerializedObj.FindProperty("_Facebook._AppId");
                _FacebookAppName = _SerializedObj.FindProperty("_Facebook._AppName");
                _FacebookAutoLogAppEvents = _SerializedObj.FindProperty("_Facebook._AutoLogAppEventsEnabled");
            }

            void GetDebugProps()
            {
                // setup debugger properties
                _DebuggerPrefab = _SerializedObj.FindProperty("_Debugging._Prefab");
                _DebuggerEnabled = _SerializedObj.FindProperty("_Debugging._EnableDebugger");
                _DebuggerShowAtStartup = _SerializedObj.FindProperty("_Debugging._ShowAtStartup");
                _DebugLogLevel = _SerializedObj.FindProperty("_Debugging._DebugLevel");
            }

            void GetGDPRProps()
            {
                // setup GDPR properties
                _GdprPrefab = _SerializedObj.FindProperty("_GDPR._Prefab");
                _GdprFixItBannerHeight = _SerializedObj.FindProperty("_GDPR._FixItBannerHeight");
                _GdprAppName = _SerializedObj.FindProperty("_GDPR._AppName");
                _GdprFont = _SerializedObj.FindProperty("_GDPR._Font");
                _GdprFontScale = _SerializedObj.FindProperty("_GDPR._FontScale");
                _GdprTitleFontColor = _SerializedObj.FindProperty("_GDPR._TitleFontColor");
                _GdprBaseFontColor = _SerializedObj.FindProperty("_GDPR._BaseFontColor");
                _GdprSecondaryColor = _SerializedObj.FindProperty("_GDPR._SecondaryColor");
                _GdprShowBorders = _SerializedObj.FindProperty("_GDPR._ShowBorders");
                _GdprBackgroundColor = _SerializedObj.FindProperty("_GDPR._BackgroundColor");
                _GdprPrivacyLinks = _SerializedObj.FindProperty("_GDPR._PrivacyLinks");
            }
            
            void GetAppLovinProps()
            {
                // setup ads properties
                _MinimumInterstitialInterval = _SerializedObj.FindProperty("_RemoteConfig._MinimumInterstitialInterval");
                _RVInterstitialTimer = _SerializedObj.FindProperty("_RemoteConfig._RVInterstitialTimer");
                _InterstitialStartTimer = _SerializedObj.FindProperty("_RemoteConfig._InterstitialStartTimer");
                _InterstitialStartLevel = _SerializedObj.FindProperty("_RemoteConfig._InterstitialStartLevel");
                _BannersDisabled = _SerializedObj.FindProperty("_RemoteConfig._BannersDisabled");

                _AppLovinEnabled = _SerializedObj.FindProperty("_AppLovin._Enabled");
                _MaxAdReviewEnabled = _SerializedObj.FindProperty("_AppLovin._MaxAdReviewEnabled");
                _AppLovinSDKKey = _SerializedObj.FindProperty("_AppLovin._SDKKey");
                _BannerAdBackgroundColor = _SerializedObj.FindProperty("_AppLovin._BannerAdBackgroundColor");
                _iOSRewardedAdUnitId = _SerializedObj.FindProperty("_AppLovin._iOSRewardedAdUnitId");
                _iOSInterstitialAdUnitId = _SerializedObj.FindProperty("_AppLovin._iOSInterstitialAdUnitId");
                _iOSBannerAdUnitId = _SerializedObj.FindProperty("_AppLovin._iOSBannerAdUnitId");
                _AndroidRewardedAdUnitId = _SerializedObj.FindProperty("_AppLovin._AndroidRewardedAdUnitId");
                _AndroidInterstitialAdUnitId = _SerializedObj.FindProperty("_AppLovin._AndroidInterstitialAdUnitId");
                _AndroidBannerAdUnitId = _SerializedObj.FindProperty("_AppLovin._AndroidBannerAdUnitId");
                _iOSAdMobAppId = _SerializedObj.FindProperty("_AppLovin._iOSAdMobAppId");
                _AndroidAdMobAppId = _SerializedObj.FindProperty("_AppLovin._AndroidAdMobAppId");
            }

            void GetProps()
            {
                GetGeneralProps();
                GetGDPRProps();
                GetAppLovinProps();
                GetAdjustProps();
                GetFirebaseProps();
                GetFacebookProps();
                GetInAppReviewProps();
                GetInAppPurchaseProps();
                GetDebugProps();
            }

            private List<SerializedProperty> _invalidProps = new List<SerializedProperty>();
            bool ValidateProps()
            {
                _invalidProps = new List<SerializedProperty>();
                
                // gradle override
                if (_OverrideGradlePluginVersion.boolValue == true)
                {
                    if (string.IsNullOrEmpty(_GradlePluginVersion.stringValue))
                    {
                        _invalidProps.Add(_GradlePluginVersion);
                    }
                }

                if (_GdprPrefab.objectReferenceValue == null)
                {
                    _invalidProps.Add(_GdprPrefab);
                }

                if (string.IsNullOrEmpty(_GdprAppName.stringValue))
                {
                    _invalidProps.Add(_GdprAppName);
                }

                if (_AppLovinEnabled.boolValue == true)
                {
                    if (string.IsNullOrEmpty(_AppLovinSDKKey.stringValue))
                    {
                        _invalidProps.Add(_AppLovinSDKKey);
                    }

                    if (string.IsNullOrEmpty(_AndroidRewardedAdUnitId.stringValue)
                        && string.IsNullOrEmpty(_iOSRewardedAdUnitId.stringValue))
                    {
                        _invalidProps.Add(_AndroidRewardedAdUnitId);
                        _invalidProps.Add(_iOSRewardedAdUnitId);
                    }

                    if (string.IsNullOrEmpty(_AndroidInterstitialAdUnitId.stringValue)
                        && string.IsNullOrEmpty(_iOSInterstitialAdUnitId.stringValue))
                    {
                        _invalidProps.Add(_AndroidInterstitialAdUnitId);
                        _invalidProps.Add(_iOSInterstitialAdUnitId);
                    }

                    if (string.IsNullOrEmpty(_AndroidBannerAdUnitId.stringValue)
                        && string.IsNullOrEmpty(_iOSBannerAdUnitId.stringValue))
                    {
                        _invalidProps.Add(_AndroidBannerAdUnitId);
                        _invalidProps.Add(_iOSBannerAdUnitId);
                    }

                    if (_AdMobIntegrated)
                    {
                        if (string.IsNullOrEmpty(_AndroidAdMobAppId.stringValue)
                            && string.IsNullOrEmpty(_iOSAdMobAppId.stringValue))
                        {
                            _invalidProps.Add(_AndroidAdMobAppId);
                        }
                    }
                }

                if (_FacebookEnabled.boolValue == true)
                {
                    if (string.IsNullOrEmpty(_FacebookAppId.stringValue))
                    {
                        _invalidProps.Add(_FacebookAppId);
                    }
                    
                    if (string.IsNullOrEmpty(_FacebookAppName.stringValue))
                    {
                        _invalidProps.Add(_FacebookAppName);
                    }
                }

                if (_AdjustEnabled.boolValue == true)
                {
                    if (string.IsNullOrEmpty(_AndroidAdjustToken.stringValue)
                        && string.IsNullOrEmpty(_iOSAdjustToken.stringValue))
                    {
                        _invalidProps.Add(_AndroidAdjustToken);
                        _invalidProps.Add(_iOSAdjustToken);
                    }
                }

                return _invalidProps.Count == 0;
            }
            
            #endregion

            #region Settings Properties
            SerializedObject _SerializedObj;

            // android
            SerializedProperty _OverrideGradlePluginVersion;
            SerializedProperty _GradlePluginVersion;
            SerializedProperty _UseMultiDexSupport;
            SerializedProperty _UseProGuard;

            // ios
            // todo: ios settings

            // debugging
            SerializedProperty _DebuggerPrefab;
            SerializedProperty _DebuggerEnabled;
            SerializedProperty _DebuggerShowAtStartup;
            SerializedProperty _DebugLogLevel;

            // gdpr
            SerializedProperty _GdprPrefab;
            SerializedProperty _GdprFixItBannerHeight;
            SerializedProperty _GdprAppName;
            SerializedProperty _GdprFont;
            SerializedProperty _GdprFontScale;
            SerializedProperty _GdprTitleFontColor;
            SerializedProperty _GdprBaseFontColor;
            SerializedProperty _GdprSecondaryColor;
            SerializedProperty _GdprShowBorders;
            SerializedProperty _GdprBackgroundColor;
            SerializedProperty _GdprPrivacyLinks;

            // ads
            SerializedProperty _MinimumInterstitialInterval;
            SerializedProperty _RVInterstitialTimer;
            SerializedProperty _InterstitialStartTimer;
            SerializedProperty _InterstitialStartLevel;
            SerializedProperty _BannersDisabled;

            // plugins
            SerializedProperty _AppLovinEnabled;
            SerializedProperty _MaxAdReviewEnabled;
            SerializedProperty _AppLovinSDKKey;
            SerializedProperty _iOSRewardedAdUnitId;
            SerializedProperty _iOSInterstitialAdUnitId;
            SerializedProperty _iOSBannerAdUnitId;
            SerializedProperty _AndroidRewardedAdUnitId;
            SerializedProperty _AndroidInterstitialAdUnitId;
            SerializedProperty _AndroidBannerAdUnitId;
            SerializedProperty _BannerAdBackgroundColor;
            public SerializedProperty _iOSAdMobAppId;
            public SerializedProperty _AndroidAdMobAppId;

            SerializedProperty _FacebookEnabled;
            SerializedProperty _FacebookAppId;
            SerializedProperty _FacebookAppName;
            SerializedProperty _FacebookAutoLogAppEvents;

            SerializedProperty _AdjustEnabled;
            SerializedProperty _AdjustIsSandbox;
            SerializedProperty _AdjustSandboxMode;
            SerializedProperty _iOSAdjustToken;
            SerializedProperty _AndroidAdjustToken;

            SerializedProperty _FirebaseEnabled;
            SerializedProperty _Firebase_RegisterStandardEvents;
            SerializedProperty _Firebase_RegisterUAEvents;

            SerializedProperty _InAppPurchaseEnabled;
            SerializedProperty _ValidatePurchases;
            SerializedProperty _AllowRestorePurchases;

            SerializedProperty _InAppReviewEnabled;
            #endregion
        }
    }
}