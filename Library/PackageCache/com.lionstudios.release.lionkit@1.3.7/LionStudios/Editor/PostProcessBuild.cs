﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Xml.Linq;
using System.Linq;

#if UNITY_IOS || UNITY_IPHONE
using UnityEditor.iOS.Xcode;
#endif

namespace LionStudios.Editor
{
	public static class PostProcessBuild
	{
		[PostProcessBuild(100)]
		public static void OnPostProcessBuild(BuildTarget target, string pathToBuiltProject)
		{
			// clear database cache
			Database.DeleteEnvironmentData();
			
			if (target == BuildTarget.iOS)
			{
#if UNITY_IOS || UNITY_IPHONE
				ProcessiOS(pathToBuiltProject);
#endif
			}
			else if (target == BuildTarget.Android)
			{
#if UNITY_ANDROID
				ProcessAndroid(pathToBuiltProject);
#endif
			}
		}

		#region iOS
		#if UNITY_IOS || UNITY_IPHONE
		public static void ProcessiOS(string path)
		{
			string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
			PBXProject proj = new PBXProject();
			proj.ReadFromString(File.ReadAllText(projPath));
#if UNITY_2019_3_OR_NEWER
			string targetGUID = proj.GetUnityMainTargetGuid();
#else
            string targetGUID = proj.TargetGuidByName("Unity-iPhone");
#endif

			AddTargetMemberShip(path, proj, targetGUID);
            AddFrameworks(proj, targetGUID);
            AddLinkerFlags(proj, targetGUID);
            AddPlistEntries(path);
            AddBuildProperties(proj, targetGUID);

            File.WriteAllText(projPath, proj.WriteToString());
        }

		public static void AddTargetMemberShip(string path, PBXProject proj, string targetGUID)
		{
			string[] files = Directory.GetFiles(path,"*GoogleService-Info.plist", SearchOption.AllDirectories);
			if (files.Length > 0)
			{
				//Debug.Log(files[0]);
				Debug.Log("Adding Target Membership for GoogleService-Info.plist to Unity-iPhone");
				string guid = proj.FindFileGuidByProjectPath("GoogleService-Info.plist");
				//Debug.Log("GUID = " + guid);
				proj.AddFileToBuild(targetGUID, guid);
			}
		}

        public static void AddLinkerFlags(PBXProject project, string targetGUID)
        {
            project.AddBuildProperty(targetGUID, "OTHER_LDFLAGS", "-ObjC");
        }

        public static void AddFrameworks(PBXProject project, string targetGUID)
        {
            project.AddFrameworkToProject(targetGUID, "iAd.framework", false);
            project.AddFrameworkToProject(targetGUID, "GameKit.framework", false);
            project.AddFrameworkToProject(targetGUID, "AdSupport.framework", false);
        }

        public static void AddPlistEntries (string pathToBuiltProject)
        {
            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict root = plist.root;
            
            root.SetString("NSCalendarsUsageDescription", "Used to deliver better advertising experience");
   //          root.SetString("FacebookAppID", LionSettings.Facebook.AppId);
   //          root.SetString("FacebookDisplayName", Application.productName);
			// root.SetBoolean("FacebookAutoLogAppEventsEnabled", LionSettings.Facebook.AutoLogAppEventsEnabled);
			// root.SetBoolean("FacebookAdvertiserIDCollectionEnabled", true);

            //IDictionary<string, PlistElement> rootDict = root.values;
            //if (rootDict.ContainsKey("CFBundleURLTypes"))
            //{
            //    PlistElementArray CFBundleURLTypes = rootDict["CFBundleURLTypes"].AsArray();

            //}
            //else
            // {
            //     PlistElementArray CFBundleURLTypes = root.CreateArray("CFBundleURLTypes");
            //     PlistElementDict dict = CFBundleURLTypes.AddDict();
            //     PlistElementArray CFBundleURLSchemes = dict.CreateArray("CFBundleURLSchemes");
            //     CFBundleURLSchemes.AddString("fb" + LionSettings.Facebook.AppId);
            // }

            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());
        }
        
        static void AddBuildProperties(PBXProject project, string guid)
        {
	        // Add the -lresolv so that TikTok (bytedance) can be linked
	        project.AddBuildProperty(guid, "OTHER_LDFLAGS", "-lresolv");
	        //// Disable bitcode for ...
	        //project.SetBuildProperty(guid, "ENABLE_BITCODE", "NO");
        }
        #endif
        #endregion

        #region Android
        #if UNITY_ANDROID
        static void ProcessAndroid(string path)
        {

            // write to manifest
            // write to gradle
            // write to security config
   //         Debug.Log(path);
			//if (LionManifestBuilder.CheckManifest() == false)
   //             LionManifestBuilder.GenerateManifest();
            //FacebookManifestBuilder.UpdateManifestForFacebook(path);

			//string manifestPath = path + "/" + Application.productName + "/src/main/AndroidManifest.xml";
			//if (File.Exists(manifestPath))
			//	Debug.Log("Manifest Exists");

		}
		#endif
		#endregion
    }

	
}
