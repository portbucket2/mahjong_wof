using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LionStudios
{
    public static class TextureUtil
    {
        public static Texture2D CreateTexture(int width, int height, Color fillColor, bool updateMips = false)
        {
            Texture2D tex = new Texture2D(width, height);
            tex.SetPixels(0, 0, width, height, new Color[] { fillColor });
            tex.Apply(updateMipmaps: updateMips);
            return tex;
        }
    }
    
    public static class StringUtil
    {
        public static Dictionary<string, string> ToStringDict(IDictionary<string, object> dict)
        {
            if (dict == null)
            {
                return null;
            }

            var newDict = new Dictionary<string, string>();
            foreach (KeyValuePair<string, object> kvp in dict)
            {
                newDict[kvp.Key] = kvp.Value.ToString();
            }

            return newDict;
        }
    }

    public static class TextUtil
    {
        public struct RText
        {
            public string text;
            public RText(object message, Color? color = null, bool bold = false, bool italic = false, int size = -1)
            {
                text = "<color=#" + ColorUtility.ToHtmlStringRGB(color ?? Color.white) + ">" + message + "</color>";
                if (bold)
                {
                    text = "<b>" + text + "</b>";
                }
                if (italic)
                {
                    text = "<i>" + text + "</i>";
                }
                if (size > 0)
                {
                    text = "<size=" + size + ">" + text + "</size>";
                }
            }
        
            // This seems to break unity with no error message so leaving it commented out for now
            public static implicit operator string(RText richText)
            {
                return richText.text;
            }

            public override string ToString()
            {
                return text;
            }
        }
    }
   
}