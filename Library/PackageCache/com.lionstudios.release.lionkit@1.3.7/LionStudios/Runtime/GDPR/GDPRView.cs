﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace LionStudios
{

    public class GDPRView : MonoBehaviour
    {
        [SerializeField] List<Image> backgroundImageList = null;
        [SerializeField] List<Image> secondaryImageList = null;
        [SerializeField] List<Image> borderImageList = null;
        [SerializeField] List<Text> titleTextList = null;
        [SerializeField] List<Text> textList = null;
        
        private Text[] fullTextList = new Text[0];
        private float lastScale = 1f;
        private EventSystem _tmpEventSystem;

        private void Awake()
        {
            fullTextList = GetComponentsInChildren<Text>();
            
            // check for event system
            EventSystem ev = EventSystem.current;
            if (ev == null || !ev.gameObject.activeInHierarchy)
            {
                _tmpEventSystem = new GameObject("EventSystem")
                    .AddComponent<EventSystem>();
            }
        }

        private void Start()
        {
            UpdateFormatting();
        }

        private void OnDisable()
        {
            if (_tmpEventSystem != null)
            {
                GameObject.Destroy(_tmpEventSystem.gameObject);
                _tmpEventSystem = null;
            }
        }

        public void UpdateFormatting()
        {
            foreach (Image img in backgroundImageList)
            {
                img.color = LionSettings.GDPR.BackgroundColor;
            }
            foreach (Image img in borderImageList)
            {
                img.enabled = LionSettings.GDPR.ShowBorders;
                img.color = LionSettings.GDPR.SecondaryColor;
            }
            foreach (Image img in secondaryImageList)
            {
                img.color = LionSettings.GDPR.SecondaryColor;
            }

            foreach (Text text in titleTextList)
            {
                text.color = LionSettings.GDPR.TitleFontColor;
            }

            foreach (Text text in textList)
            {
                text.color = LionSettings.GDPR.BaseFontColor;
            }

            foreach(Text text in fullTextList)
            {
                if (LionSettings.GDPR.Font != null)
                {
                    text.font = LionSettings.GDPR.Font;
                }
                else
                {
                    text.font = (Font)Resources.Load("acherusgrotesque-black");
                    //text.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
                }
            }

            if (LionSettings.GDPR.FontScale != lastScale)
            {
                ScaleFonts(LionSettings.GDPR.FontScale);
            }
        }

        public void ScaleFonts(float newScale)
        {
            foreach (Text text in fullTextList)
            {
                int scale = Mathf.RoundToInt(text.fontSize / lastScale * newScale);
                text.fontSize = scale;
            }
            lastScale = newScale;
        }
    }

}