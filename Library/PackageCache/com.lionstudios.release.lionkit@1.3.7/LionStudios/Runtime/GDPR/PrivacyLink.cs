﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

namespace LionStudios
{
    public class PrivacyLink : Button
    {
        static Font _Font;
        public static void Create(Transform parent, string url)
        {
            GameObject inst = new GameObject(url);
            inst.transform.parent = parent;
            inst.transform.localScale = Vector3.one;

            var privacyButton = inst.AddComponent<PrivacyLink>();
            Text text = privacyButton.gameObject.AddComponent<Text>();
            text.text = url;
            text.fontSize = 33;
            if (_Font == null)
                _Font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            text.font = _Font;
            text.color = new Color32(37, 37, 166, 255);
            (inst.transform as RectTransform).sizeDelta = new Vector2(2000f, 40f);
            privacyButton.onClick.AddListener(() => Application.OpenURL(text.text));
        }
    }
}
