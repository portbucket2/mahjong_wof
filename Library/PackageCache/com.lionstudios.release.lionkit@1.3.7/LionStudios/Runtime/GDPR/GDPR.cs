using System.Collections.Generic;
using System.Collections;
using LionStudios.Debugging;
using LionStudios.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace LionStudios
{
    public class GDPR : MonoBehaviour
    {
        public enum UserStatus
        {
            NotSet,
            Applies,
            DoesNotApply,
            Unknown
        }

        /// <summary>
        /// Initializes the GDPR and shows the consent dialogue if necessary
        /// </summary>
        /// <param name="userStatus">Information as to whether the GDPR must be shown to the user</param>
        public static void Initialize(UserStatus userStatus)
        {
            Status = userStatus;
            switch (userStatus)
            {
                case UserStatus.Applies:
                    if (_Singleton == null)
                        _Singleton = Instantiate(LionSettings.GDPR.Prefab);
                    _Singleton.Init();
                    _Singleton.OnGameStart();
                    break;
                case UserStatus.DoesNotApply:
                    SetDoesNotApply();
                    break;
                case UserStatus.Unknown:
                    OnGDPRAppliesUnknown?.Invoke();
                    break;
            }
        }

        /// <summary>
        /// Show the GDPR consent dialogue to the user
        /// </summary>
        public static void Show()
        {
            if (_Singleton == null)
                _Singleton = Instantiate(LionSettings.GDPR.Prefab);

            _Singleton.ShowDynamic();
            
        }

#if UNITY_EDITOR
        public static void ShowPromptDbg(int index)
        {
            if (_Singleton == null)
                _Singleton = Instantiate(LionSettings.GDPR.Prefab);

            _Singleton.ShowPrompt(index);
        }
#endif

        /// <summary>
        /// Hides the GDPR consent dialogue
        /// </summary>
        public static void HideBanner()
        {
            if (_Singleton != null)
                _Singleton.gameObject.SetActive(false);
            
            LionDebug.Log("Hiding GDPR Banner", LionDebug.DebugLogLevel.Default);
        }

        /// <summary>
        /// Functionality for users where GDPR consent is not required
        /// </summary>
        static void SetDoesNotApply()
        {
            SetAnalyticsConsent(true);
            SetAdConsent(true);
            SetGDPRCompleted();
        }


        public static UserStatus Status { get; private set; }
        public static bool Active { get { return _Singleton != null && _Singleton._Panels.activeInHierarchy; } }

        public static event System.Action OnGDPRAppliesUnknown;
        public static event System.Action OnCompleted;
        public static event System.Action OnOpen;
        public static event System.Action OnClosed;
        public static event System.Action OnAnalyticsConsentUpdated;
        public static event System.Action OnAdConsentUpdated;

        [SerializeField]
        GameObject[] _Prompts = null;
        [SerializeField]
        Toggle _AnalyticsConsentToggle = null;
        [SerializeField]
        Toggle _AdConsentToggle = null;
        [SerializeField]
        GameObject _Panels = null;
        [SerializeField]
        GameObject _FixItBanner = null;

        static GDPR _Singleton;
        bool _Initialized;

        public static GDPR GetGDPR()
        {
            return _Singleton;
        }

        /// <summary>
        /// Initializes the actual GDPR dialogue
        /// </summary>
        void Init()
        {
            if (_Initialized)
                return;
            
            //Debug.Log("[GDPR] Initialized");
            DontDestroyOnLoad(gameObject);

            // Setup analytics consent toggle
            _AnalyticsConsentToggle.Init(delegate { return AnalyticsConsentGranted; }, SetAnalyticsConsent);

            // Setup advertisements consent toggle
            _AdConsentToggle.Init(delegate { return AdConsentGranted; }, SetAdConsent);

            UpdateFormatting();
            Localize();
            SetTitleName();
            _Initialized = true;
            
            OnOpen += () => LionDebug.Log("GDPR Opened");
            
            // If we are already active then we need to call OnShow to notify delegates of our activation
            if (Active)
                OnOpen?.Invoke();
            
            LionDebug.Log("GDPR Init complete");
        }
        
        /// <summary>
        /// Sets the name of the GDPR dialogue based on the Application name
        /// </summary>
        void SetTitleName()
        {
            //yield return new WaitForEndOfFrame();
            Text[] texts = GetComponentsInChildren<Text>(true);
            string title = string.IsNullOrEmpty(LionSettings.GDPR.AppName) ? Application.productName : LionSettings.GDPR.AppName;

            //Debug.Log("[GDPR] Set Title Name :: " + this + " :: Texts Count = " + texts.Length);
            
            foreach (Text text in texts)
            {
                // bool titleReplaced = false;
                // if (text.text.Contains("<title_name>"))
                // {
                //     Debug.Log("[GDPR] Replace Title.\n" + text.text);
                //     titleReplaced = true;
                // }
                // else
                //     Debug.Log("[GDPR] No title found.\n" + text.text);
                text.text = text.text.Replace("<title_name>", title);

                // if (titleReplaced)
                //     Debug.Log("[GDPR] Title Replaced.\n" + text.text);
            }
        }

        void OnGameStart()
        {
            LionDebug.Log($"[GDPR] OnGameStart :: Completed = {Completed} :: AnalyticsConsentGranted = {AnalyticsConsentGranted} :: AdConsentGranted = {AdConsentGranted}");
            // If GDPR has been completed then just figure out how we are situated...
            if (Completed && AnalyticsConsentGranted && AdConsentGranted)
            {
                gameObject.SetActive(false);
                return;
            }

            ShowPrompt();
        }

        /// <summary>
        /// Calculate and show the correct prompt
        /// </summary>
        void ShowDynamic()
        {
            //Debug.Log("[GDPR] ShowPrompt :: AdConsentGranted = " + AdConsentGranted + " :: AnalyticsConsentGranted = " + AnalyticsConsentGranted);
            int prompt = (AnalyticsConsentGranted && AdConsentGranted) ? 1 : 2;
            ShowPrompt(prompt);
        }
        
        /// <summary>
        /// Shows the GDPR dialogue at the appropriate prompt
        /// </summary>
        /// <param name="prompt">The prompt to show</param>
        public void ShowPrompt(int prompt = 0)
        {
            Init();
            
            bool wasActive = Active;
            
            _Singleton.SetActivePrompt(prompt);
            _Panels.SetActive(true);
            _FixItBanner.SetActive(false);
            gameObject.SetActive(true);
            LionDebug.Log($"[GDPR] Show Prompt - " + prompt);
            if (wasActive == false)
                OnOpen?.Invoke();
        }

        /// <summary>
        /// Triggered by the Awesome response to the GDPR dialogue.
        /// </summary>
        public void Awesome()
        {
            gameObject.SetActive(false);
            SetAdConsent(true);
            SetAnalyticsConsent(true);
            SetGDPRCompleted();
        }


        /// <summary>
        /// Triggered by the Accept response to the GDPR panel.
        /// </summary>
        /// <param name="closePanel">Signify if the panel should be closed</param>
        public void Accept(bool closePanel)
        {
            bool hasUserConsent = AnalyticsConsentGranted && AdConsentGranted;
            if (hasUserConsent == false)
            {
                if (closePanel)
                {
                    ShowPolicyBanner();
                    SetGDPRCompleted();
                }
                else
                    ShowPrompt(3);
            }
            else
            {
                gameObject.SetActive(false);
                SetGDPRCompleted();
            }
        }

        /// <summary>
        /// Shows the consent dialogue (triggered when a user pressed the Back button on the consent dialogue)
        /// </summary>
        public void ShowPolicyBanner()
        {
            _Panels.SetActive(false);
            _FixItBanner.SetActive(true);
            gameObject.SetActive(true);
            SetFixItBannerHeight();
        }

        /// <summary>
        /// Sets the banner height based on the display size.
        /// </summary>
        void SetFixItBannerHeight()
        {
            float height = (_FixItBanner.transform as RectTransform).sizeDelta.y;
            float parentHeight = (_FixItBanner.transform.parent as RectTransform).sizeDelta.y;
            float heightPercent = height / parentHeight;
            float yPos = Mathf.Clamp01(LionSettings.GDPR.FixItBannerHeight);
            float y = parentHeight * (yPos * (1f - heightPercent) + heightPercent - 1f);
            (_FixItBanner.transform as RectTransform).anchoredPosition = new Vector2(0f, y);
        }

#if UNITY_EDITOR
        private void Update()
        {
            SetFixItBannerHeight();
            UpdateFormatting();
            SetTitleName();
        }
#endif

        public void Localize()
        {
            GetComponent<GDPRLocalization>().ParseStrings();
            
            LocalizeText[] texts = GetComponentsInChildren<LocalizeText>(true);
            foreach (var text in texts)
                text.Localize();
        }

        /// <summary>
        /// Refreshes the formatting settings from the Integration Panel
        /// </summary>
        public void UpdateFormatting()
        {
            GetComponent<GDPRView>().UpdateFormatting();
        }

        /// <summary>
        /// Updates the current prompt index
        /// </summary>
        /// <param name="index">The target prompt index</param>
        void SetActivePrompt(int index)
        {
            if (index < 0 || index >= _Prompts.Length)
                return;

            foreach (GameObject prompt in _Prompts)
                prompt.SetActive(false);

            _Prompts[index].SetActive(true);
        }

        static Settings _PrivacySettings;

        /// <summary>
        /// Returns true if the GDPR settings are completed
        /// </summary>
        public static bool Completed
        {
            get
            {
                if (_PrivacySettings == null)
                    _PrivacySettings = Settings.Load();

                return _PrivacySettings.completed;
            }
        }

        /// <summary>
        /// Marks the GDPR settings completed and invokes callbacks
        /// </summary>
        static void SetGDPRCompleted()
        {
            if (_PrivacySettings == null)
                _PrivacySettings = Settings.Load();

            if (_PrivacySettings.completed == false)
            {
                LionDebug.Log($"[GDPR] SetGdprCompleted = {_PrivacySettings.completed}");
                _PrivacySettings.completed = true;
                _PrivacySettings.SaveLocal();

                LionDebug.Log("GDPR Completed");
                // Notify listeners
                OnCompleted?.Invoke();
            }
            LionDebug.Log("GDPR Closed");
            OnClosed?.Invoke();
        }

        /// <summary>
        /// Returns true if the user consented to Ads
        /// </summary>
        public static bool AdConsentGranted
        {
            get
            {
                if (_PrivacySettings == null)
                    _PrivacySettings = Settings.Load();
                return _PrivacySettings.ads;
            }
        }

        /// <summary>
        /// Marks if the user consents to Ads
        /// </summary>
        /// <param name="value">TRUE if the user has consented to Ads</param>
        static void SetAdConsent(bool value)
        {
            if (_PrivacySettings == null)
                _PrivacySettings = Settings.Load();
            if (_PrivacySettings.ads != value)
            {
                _PrivacySettings.ads = value;
                _PrivacySettings.SaveLocal();

                _Singleton._AdConsentToggle.RefreshView();

                LionDebug.Log("OnAdConsentUpdated");
                // Notify listeners
                OnAdConsentUpdated?.Invoke();
            }
        }

        /// <summary>
        /// Returns true if the user has consented to Analytics
        /// </summary>
        public static bool AnalyticsConsentGranted
        {
            get
            {
                if (_PrivacySettings == null)
                    _PrivacySettings = Settings.Load();

                return _PrivacySettings.analytics;
            }
        }

        /// <summary>
        /// Marks if the user consents to Analytics
        /// </summary>
        /// <param name="value">TREU if the user has consented to Analytics</param>
        static void SetAnalyticsConsent(bool value)
        {
            if (_PrivacySettings == null)
                _PrivacySettings = Settings.Load();

            if (_PrivacySettings.analytics != value)
            {
                _PrivacySettings.analytics = value;
                _PrivacySettings.SaveLocal();
                
                if (_Singleton)
                    _Singleton._AnalyticsConsentToggle.RefreshView();

                LionDebug.Log("OnAnalyticsConsentUpdated");
                // Notify listeners
                OnAnalyticsConsentUpdated?.Invoke();
            }
        }

        public class Settings : PersistentData<Settings>
        {
            public bool completed = false;
            public bool analytics = true;
            public bool ads = true;
        }

#if UNITY_EDITOR && LION_KIT_DEV
        [UnityEditor.MenuItem("LionStudios/Clear GDPR Flags")]
        public static void DeleteGDPRSettings()
        {
            Settings.DeleteAllInstances();
        }
#endif
    }
}
