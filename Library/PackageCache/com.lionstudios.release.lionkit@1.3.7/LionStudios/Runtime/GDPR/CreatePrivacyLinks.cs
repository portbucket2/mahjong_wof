﻿using UnityEngine;
using System.Collections.Generic;

namespace LionStudios
{
    public class CreatePrivacyLinks : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            // add privacy links from Applovin
            var privacyLinks = AppLovin.Mediation.GetPrivacyLinks();
            privacyLinks.Add("https://www.adjust.com/terms/privacy-policy/");   // Adjust
            privacyLinks.Add("https://deltadna.com/privacy/");                  // DDNA
            privacyLinks.Add("https://firebase.google.com/support/privacy");    // Firebase
            //privacyLinks.Add("https://www.safedk.com/privacy");                 // SafeDk
            privacyLinks.Add("https://www.applovin.com/privacy/");              // AppLovin

            //add privacy links from Lion Integration Settings
            List<string> linkList = LionSettings.GDPR.PrivacyLinks;
            foreach (string link in linkList)
            {
                privacyLinks.Add(link);
            }
            
            // if we somehow have some children already, let's git rid of them
            while (transform.childCount > 0)
                DestroyImmediate(transform.GetChild(0).gameObject);

            foreach (string link in privacyLinks)
                PrivacyLink.Create(transform, link);
        }
    }
}