﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;


namespace LionStudios
{
    public class Toggle : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        GameObject _On = null;
        [SerializeField]
        GameObject _Off = null;
    
        bool _Initialized;
        event Func<bool> _GetState;
        event Action<bool> _SetState;
    
        public void Init(Func<bool> GetState, Action<bool> SetState)
        {
            _GetState = GetState;
            _SetState = SetState;
            _Initialized = _GetState != null && _SetState != null;
            if (_Initialized)
            {
                UpdateView(GetState());
            }
            else
            {
                Debug.LogWarning("GetState and/or SetState Func is Null.  Toggle is NOT initialized.");
            }
        }
    
        public void RefreshView()
        {
            if (_Initialized)
                UpdateView(_GetState());
        }
    
        void OnEnable()
        {
            RefreshView();
        }
    
        void UpdateView(bool state)
        {
            _On.SetActive(state);
            _Off.SetActive(!state);
        }
    
        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            if (_Initialized == false)
            {
                Debug.LogError("Toggle was clicked before it was initialized.  Please initialize this toggle before allowing users to use it.");
                return;
            }
    
            _SetState(!_GetState());
            UpdateView(_GetState());
        }
    }
}
