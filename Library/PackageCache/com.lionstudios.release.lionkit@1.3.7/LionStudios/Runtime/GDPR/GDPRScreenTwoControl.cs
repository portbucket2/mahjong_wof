﻿using UnityEngine;
using UnityEngine.UI;

namespace LionStudios
{

    public class GDPRScreenTwoControl : MonoBehaviour
    {
        [SerializeField] VerticalLayoutGroup _vertical;
        [SerializeField] HorizontalLayoutGroup _horizontal; 
        
        // Start is called before the first frame update
        void Start()
        {
            EnforceScreen();
        }

        // Update is called once per frame
        void Update()
        {
            EnforceScreen();
        }

        void EnforceScreen()
        {
            if (_vertical == null)
                _vertical = GetComponentInChildren<VerticalLayoutGroup>(true);
            
            if (_horizontal == null)
                _horizontal = GetComponentInChildren<HorizontalLayoutGroup>(true);
            
            if (Screen.width < Screen.height)
            {
                _vertical.gameObject.SetActive(true);
                _horizontal.gameObject.SetActive(false);
            }
            else
            {
                _vertical.gameObject.SetActive(false);
                _horizontal.gameObject.SetActive(true);
            }
        }
    }
}

