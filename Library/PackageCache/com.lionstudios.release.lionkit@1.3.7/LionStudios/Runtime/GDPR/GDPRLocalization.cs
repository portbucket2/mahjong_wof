﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LionStudios.Localization;

namespace LionStudios
{

    public class GDPRLocalization : MonoBehaviour
    {
        public List<SystemLanguage> availableLanguages;

        public void ParseStrings()
        {
            Strings.ParseStringsFile(availableLanguages, "GDPR_Strings");
        }
    }
}