﻿using System;

namespace LionStudios.Debugging
{
    public static class LionDebug
    {
        public enum DebugLogLevel : int
        {
            None = 0, // hide all output from Lion Kit
            Warn,
            Error,
            Event, // log only analytic/Ad events
            Default, // log standard output
            Verbose // show all logs
        }
        
        public static void LogFormat(DebugLogLevel logLevel, string msg, params object[] args)
        {
            Log(string.Format(msg, args), logLevel);
        }
        
        public static void Log(string msg, DebugLogLevel logLevel = DebugLogLevel.Default)
        {
            if (LionSettings.Debugging.DebugLevel >= logLevel)
            {
                UnityEngine.Debug.Log(msg);
            }
        }
        
        public static void LogErrorFormat(string msg, params string[] args)
        {
            LogError(string.Format(msg, args));
        }
        
        public static void LogErrorFormat(UnityEngine.Object context, string msg, params string[] args)
        {
            LogError(context, string.Format(msg, args));
        }
        
        public static void LogError(string msg)
        {
            UnityEngine.Debug.LogError(msg);
        }
        
        public static void LogError(UnityEngine.Object context, string msg)
        {
            UnityEngine.Debug.LogError(msg, context);
        }
        
        public static void LogWarning(string msg)
        {
            UnityEngine.Debug.LogWarning(msg);
        }

        public static void LogException(Exception e)
        {
            UnityEngine.Debug.LogException(e);
        }
    }
}