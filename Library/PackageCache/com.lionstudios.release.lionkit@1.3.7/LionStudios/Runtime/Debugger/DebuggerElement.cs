﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LionStudios.Debugging
{
    public interface ILionDebugElement
    {
        string ID { get; }
        bool Enabled { get; set; }

        //void Recycle();
        GameObject GetGameObject();
    }

    public class DebuggerElement : MonoBehaviour, ILionDebugElement
    {
        private string _id;
        public string ID {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return this.gameObject.activeInHierarchy;
            }

            set
            {
                this.gameObject.SetActive(value);
            }
        }

        public GameObject GetGameObject()
        {
            return this.gameObject;
        }
    }
}