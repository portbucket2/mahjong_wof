﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using LionStudios.Ads;
#if UNITY_EDITOR
using UnityEditorInternal;
#endif

namespace LionStudios.Debugging
{

    public class LionDebugger : MonoBehaviour
    {
        private const string DefaultTabTitle = "General";
        private const string AdsTabTitle = "Ads";
        private const string PersistentDataTabTitle = "Data";

        /********************************************************************************
         * 
         * PUBLIC DEBUGGER METHODS
         * 
         ********************************************************************************
         * Show()
         * Hide()
         * AddButton(string, action, page)
         * AddLabel(label, page)
         * AddElement(element, page)
         * AddTab(string)
         * AddTab(string, tab)
         ********************************************************************************/
        public static bool IsShowing()
        {
            return instance != null && instance.enabled && instance.gameObject.activeInHierarchy;
        }
        
        public static void Show(bool ignoreInstallMode = false)
        {
            LionDebug.Log($"Attempting Show of the Debugger\n" +
                          $"LionSettings.Debugging.EnableDebugger: {LionSettings.Debugging.EnableDebugger}\n" +
                          $"LionKit.IsInitialized: {LionKit.IsInitialized}\n" +
                          $"ignoreInstallMode: {ignoreInstallMode}\n" +
                          $"Application.installMode: {Application.installMode}", LionDebug.DebugLogLevel.Verbose);
            
            if (LionSettings.Debugging.EnableDebugger &&  LionKit.IsInitialized &&  
                (ignoreInstallMode || Application.installMode != ApplicationInstallMode.Store))
            {
                LionDebug.Log("Showing Lion Debugger");

                if (EventSystem.current == null)
                {
                    new GameObject("EventSystem").AddComponent<EventSystem>();
                }

                instance.gameObject.SetActive(true);
            }
        }

        public static void Hide()
        {
            LionDebug.Log("Hiding Lion Debugger");
            instance.gameObject.SetActive(false);
        }

        public static DebugButton AddButton(string button, Action onClick, string page = "")
        {
            DebugButton newBtn = Instantiate(instance.buttonPrefab);
            newBtn.SetLabel(button);
            newBtn.AddClickAction(onClick);
            newBtn.ID = newBtn.GetInstanceID().ToString();

            AddElement(newBtn, page);
            return newBtn;
        }

        public static DebugInfoLabel AddInfo(string label, string page = "")
        {
            DebugInfoLabel newLabel = Instantiate(instance.labelPrefab);
            newLabel.SetLabel(label);
            newLabel.ID = newLabel.GetInstanceID().ToString();

            AddElement(newLabel, page);
            return newLabel;
        }

        public static DebugInfoLiveLabel AddInfo(string label, System.Func<string> returnStr, string page = "")
        {
            DebugInfoLiveLabel newLiveLabel = Instantiate(instance.liveLabelPrefab);
            newLiveLabel.SetLabel(label);
            newLiveLabel.SetLiveInfo(returnStr);
            newLiveLabel.ID = newLiveLabel.GetInstanceID().ToString();

            AddElement(newLiveLabel, page);
            return newLiveLabel;
        }

        public static void AddElement(ILionDebugElement element, string page = "")
        {
            ILionDebugTab tab = instance.GetDebuggerTab(page);
            if (tab == null)
            {
                tab = string.IsNullOrEmpty(page) ?
                    instance.GetDebuggerTab(DefaultTabTitle)
                    : instance.AddDebuggerTab(page);
            }

            tab.AddElement(element);
        }


        internal static void NextTab()
        {
            int nextTab = instance._currentTab + 1;
            if(nextTab >= instance._tabPages.Count)
            {
                nextTab = 0;
            }

            instance.GoToTabIndex(nextTab);
        }

        internal static void PrevTab()
        {
            int prevTab = instance._currentTab - 1;
            if (prevTab < 0)
            {
                prevTab = instance._tabPages.Count - 1;
            }

            instance.GoToTabIndex(prevTab);
        }

        /********************************************************************************
         * 
         * INSTANCE
         * 
         ********************************************************************************
         * Handles GUI-drawing window and inner content. Creates and updates window
         * styles internally.
         ********************************************************************************/
        private static LionDebugger _inst;
        private static LionDebugger instance { get
            {
                if(_inst == null)
                {
                    LionDebugger prefab = LionSettings.Debugging.Prefab;
                    if (prefab == null)
                    {
                        LionDebug.LogError("No debugger prefab found!");
                        return null;
                    }

                    _inst = LionDebugger.Instantiate(prefab);
                    _inst.BuildDefaultPages();
                    _inst.gameObject.SetActive(false);
                    DontDestroyOnLoad(instance);
                }

                return _inst;
            } }

        // pages
        private List<DebuggerTab> _tabPages = new List<DebuggerTab>();
        private int _currentTab = 0;

        // components
        public Transform tabsBarInner;
        public Transform tabsContent;

        // prefabs
        public DebuggerTab tabPrefab;
        public DebugButton buttonPrefab;
        public DebugInfoLabel labelPrefab;
        public DebugInfoLiveLabel liveLabelPrefab;

        private void BuildDefaultPages()
        {
            // add app elements to page
            LionDebugger.AddInfo("-App-", DefaultTabTitle);
            LionDebugger.AddInfo("App Identifier", () =>
            {
                return Application.identifier;
            }, DefaultTabTitle);
            
            LionDebugger.AddInfo("Version", () =>
            {
                return Application.version;
            }, DefaultTabTitle);
            
            // add lionkit elements to page
            LionDebugger.AddInfo("-Lion Kit-", DefaultTabTitle);
            LionDebugger.AddInfo("Version", () =>
            {
                return LionKit.Version;
            }, DefaultTabTitle);

            LionDebugger.AddInfo("GDPR Status", () =>
            {
                return GDPR.Status.ToString();
            }, DefaultTabTitle);

            // integration status
            if (LionSettings.AppLovin.Enabled)
            {
                LionDebugger.AddInfo("AppLovin Initialized: ", () =>
                {
                    return LionStudios.AppLovin.IsInitialized.ToString();
                }, DefaultTabTitle);
            }

            LionDebugger.AddButton("Show Mediation Debugger", () =>
            {
                MaxSdk.ShowMediationDebugger();
            }, DefaultTabTitle);


            // retention
            LionDebugger.AddInfo("Install Time", () => { return Retention.GetInstallTime(); }, AdsTabTitle);
            LionDebugger.AddInfo("Login Time", () => { return Retention.GetLastLoginTime(); }, AdsTabTitle);

            //RVs
            LionDebugger.AddInfo("RVs Watched", () => { return RewardedAd.GetTotalWatched().ToString(); }, AdsTabTitle);
            LionDebugger.AddInfo("RVs Clicked", () => { return RewardedAd.GetTotalClicked().ToString(); }, AdsTabTitle);

            // interstitial
            LionDebugger.AddInfo("Interstitials Watched", () => { return Interstitial.GetTotalWatched().ToString(); }, AdsTabTitle);

            LionDebugger.AddButton("Flush RVs Watched", () =>
            {
                RewardedAd.ClearTotalWatched();
                RewardedAd.ClearTotalClicked();
            }, AdsTabTitle);

            LionDebugger.AddButton("Flush Retention", () =>
            {
                Retention.ClearInstallTime();
                Retention.ClearLastLoginTime();
            }, AdsTabTitle);

            LionDebugger.AddButton("Flush Interstitials Watched", () =>
            {
                Interstitial.ClearTotalWatched();
            }, AdsTabTitle);
            
            // persistent data
            LionDebugger.AddInfo("-Persistent Data-", PersistentDataTabTitle);
            LionDebugger.AddButton("Delete Persistent Data", () =>
            {
                Database.DeleteAllData();
            }, PersistentDataTabTitle);
            GoToTabIndex(0);
        }

        private void OnEnable()
        {
            _refreshOpCache = true;
        }

        private Graphic[] _opacityElementCache = null;
        private bool _refreshOpCache = true;
        public void SetWindowOpacity(float opacity)
        {
            if(_opacityElementCache == null || _refreshOpCache == true)
            {
                _opacityElementCache = this.gameObject.GetComponentsInChildren<Graphic>();
                _refreshOpCache = false;
            }
            
            for(int i = 0; i < _opacityElementCache.Length; i++)
            {
                Color c = _opacityElementCache[i].color;
                c.a = opacity;
                _opacityElementCache[i].color = c;
            }
        }

        public void Close()
        {
            LionDebugger.Hide();
        }

        private void GoToTab(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return;
            }

            for (int i = 0; i < _tabPages.Count; i++)
            {
                if (_tabPages[i].ID == id)
                {
                    GoToTabIndex(i);
                    break;
                }
            }
        }

        private void GoToTabIndex(int index)
        {
            if(index < 0 || index >= _tabPages.Count)
            {
                return;
            }

            for(int i = 0; i < _tabPages.Count; i++)
            {
                if(i == index)
                {
                    _tabPages[i].Show();
                }
                else
                {
                    _tabPages[i].Hide();
                }
            }

            _currentTab = index;
        }

        private ILionDebugTab GetDebuggerTab(string id)
        {
            for(int i = 0; i < _tabPages.Count; i++)
            {
                if(_tabPages[i].ID == id)
                {
                    return _tabPages[i];
                }
            }
            return null;
        }

        private ILionDebugTab AddDebuggerTab(string id)
        {
            if(GetDebuggerTab(id) != null)
            {
                string msg = string.Format("Lion Debugger: Tab '{0}' already exists!", id);
                return null;
            }

            DebugButton newTabButton = DebugButton.Instantiate(this.buttonPrefab);
            newTabButton.SetLabel(id);
            newTabButton.AddClickAction(() =>
            {
                this.GoToTab(id);
            });

            DebuggerTab newTab = DebuggerTab.Instantiate(this.tabPrefab, tabsContent.transform);
                        
            // disable by default
            newTab.Hide();
            
            newTab.ID = id;

            // add page
            _tabPages.Add(newTab);

            // attach tab button
            newTabButton.transform.SetParent(this.tabsBarInner, false);


            return newTab;
        }
    }
}