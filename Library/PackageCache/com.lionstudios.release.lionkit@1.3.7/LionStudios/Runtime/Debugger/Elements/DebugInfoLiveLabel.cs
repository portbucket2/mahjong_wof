﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LionStudios.Debugging
{
    public class DebugInfoLiveLabel : DebuggerElement
    {
        public Text infoLabel = null;
        public Text liveLabel = null;

        private System.Func<string> _getStr = null;

        public void SetLabel(string str)
        {
            infoLabel.text = str;
        }

        public void SetLiveInfo(System.Func<string> returnStr)
        {
            _getStr = returnStr;
        }

        private void Update()
        {
            if(_getStr != null)
            {
                string str = _getStr.Invoke();
                liveLabel.text = str ?? "[NO DATA]";
            }
        }
    }
}