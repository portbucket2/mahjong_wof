﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Networking;

namespace LionStudios
{
    public class Dispatcher : MonoBehaviour
    {
        public new static Coroutine StartCoroutine(IEnumerator routine)
        {
            return (Singleton as MonoBehaviour).StartCoroutine(routine);
        }

        public static Dispatcher Singleton
        {
            get
            {
                if (_singleton == null)
                {
                    _singleton = new GameObject(nameof(Dispatcher)).AddComponent<Dispatcher>();
                    DontDestroyOnLoad(_singleton.gameObject);
                }
                return _singleton;
            }
        }
        static Dispatcher _singleton;
    }

    public class CoroutineAsyncBridge : INotifyCompletion
    {
        private Action _continuation;
        public bool IsCompleted { get; set; }

        private CoroutineAsyncBridge()
        {
            IsCompleted = false;
        }

        public static CoroutineAsyncBridge Start<T>(T awaitTarget)
        {
            var bridge = new CoroutineAsyncBridge();
            Dispatcher.StartCoroutine(bridge.Run(awaitTarget));
            return bridge;
        }

        IEnumerator Run<T>(T target)
        {
            yield return target;
            IsCompleted = true;
            _continuation();
        }

        public void OnCompleted(Action continuation)
        {
            this._continuation = continuation;
        }

        public void GetResult()
        {
            if (!IsCompleted) throw new InvalidOperationException("coroutine not yet completed");
        }
    }

    public class CoroutineAsyncBridge<T> : INotifyCompletion
    {
        readonly T result;
        Action continuation;
        public bool IsCompleted { get; private set; }

        CoroutineAsyncBridge(T result)
        {
            IsCompleted = false;
            this.result = result;
        }

        public static CoroutineAsyncBridge<T> Start(T awaitTarget)
        {
            var bridge = new CoroutineAsyncBridge<T>(awaitTarget);
            Dispatcher.StartCoroutine(bridge.Run(awaitTarget));
            return bridge;
        }

        IEnumerator Run(T target)
        {
            yield return target;
            IsCompleted = true;
            continuation();
        }

        public void OnCompleted(Action continuation)
        {
            this.continuation = continuation;
        }

        public T GetResult()
        {
            if (!IsCompleted) throw new InvalidOperationException("coroutine not yet completed");
            return result;
        }
    }

    public static class CoroutineAsyncExtensions
    {
        public static CoroutineAsyncBridge<UnityWebRequest> GetAwaiter(this UnityWebRequest webRequest)
        {
            return CoroutineAsyncBridge<UnityWebRequest>.Start(webRequest);
        }

        public static CoroutineAsyncBridge GetAwaiter(this Coroutine coroutine)
        {
            return CoroutineAsyncBridge.Start(coroutine);
        }

        public static CoroutineAsyncBridge<AsyncOperation> GetAwaiter(this AsyncOperation asyncOperation)
        {
            return CoroutineAsyncBridge<AsyncOperation>.Start(asyncOperation);
        }

        public static CoroutineAsyncBridge GetAwaiter(this IEnumerator coroutine)
        {
            return CoroutineAsyncBridge.Start(coroutine);
        }
    }
}