﻿using UnityEngine;
using com.adjust.sdk;
using com.adjust.sdk.purchase;
using LionStudios.Debugging;

namespace LionStudios
{
	public sealed class Adjust : BaseSdk<Adjust>
	{
		public enum SandboxMode
		{
			Default,
			On,
			Off
		}
		
		public static void Initialize()
		{
			string adjustAppToken = LionSettings.Adjust.Token;

			if (!string.IsNullOrEmpty(adjustAppToken))
			{
				AdjustEnvironment adjustEnv = LionSettings.Adjust.IsSandbox ? AdjustEnvironment.Sandbox : AdjustEnvironment.Production;
				var adjustConfig = new AdjustConfig(adjustAppToken, adjustEnv, true);

				AdjustLogLevel logLevel = AdjustLogLevel.Info;
				switch (LionSettings.Debugging.DebugLevel)
				{
					case LionDebug.DebugLogLevel.Default:
					case LionDebug.DebugLogLevel.Event:
						logLevel = AdjustLogLevel.Debug;
						break;
					case LionDebug.DebugLogLevel.Verbose:
						logLevel = AdjustLogLevel.Verbose;
						break;
					case LionDebug.DebugLogLevel.Warn:
						logLevel = AdjustLogLevel.Warn;
						break;
				}
				
				adjustConfig.setLogLevel(logLevel); // AdjustLogLevel.Suppress to disable logs
				adjustConfig.setSendInBackground(true);
				adjustConfig.setLogDelegate(msg => LionDebug.Log(msg));
				new GameObject("Adjust").AddComponent<com.adjust.sdk.Adjust>(); // do not remove or rename

				// Adjust.addSessionCallbackParameter("foo", "bar"); // if requested to set session-level parameters

				//adjustConfig.setAttributionChangedDelegate((adjustAttribution) => {
				//	Debug.LogFormat("Adjust Attribution Callback: ", adjustAttribution.trackerName);
				//});

				com.adjust.sdk.Adjust.start(adjustConfig);


				ADJPEnvironment adjustPurchaseEnv = LionSettings.Adjust.IsSandbox ? ADJPEnvironment.Sandbox : ADJPEnvironment.Production;
				/// Initializing Adjust Purchase Verification SDK
				var adjustPVConfig = new ADJPConfig(adjustAppToken, adjustPurchaseEnv);

				adjustPVConfig.SetLogLevel(ADJPLogLevel.Info); // ADJPLogLevel.Suppress to disable logs
				new GameObject("AdjustPurchase").AddComponent<AdjustPurchase>(); // do not remove or rename

				AdjustPurchase.Init(adjustPVConfig);
				Debug.Log("Adjust Init Complete");


				//GDPR.OnClosed += () => Debug.Log("OnClosed :: AnalyticsConsentGranted = " + GDPR.AnalyticsConsentGranted);
				GDPR.OnClosed += () =>
				{
					
#if !UNITY_EDITOR
					// com.adjust.sdk.Adjust.setEnabled(GDPR.AnalyticsConsentGranted);
					if (GDPR.AnalyticsConsentGranted == false)
						com.adjust.sdk.Adjust.gdprForgetMe();
#endif
				};

				Analytics.OnLogEventUA += _Singleton.OnEventLogged;
				_Singleton.OnInitComplete();
			}
		}

		protected override void LogEvent(LionGameEvent gameEvent)
		{
			AdjustEvent newEvent = new AdjustEvent(gameEvent.eventName);
			if (gameEvent.eventParams != null)
			{
				foreach (var p in gameEvent.eventParams)
				{
					newEvent.addCallbackParameter(p.Key, p.Value.ToString());
				}
			}
			com.adjust.sdk.Adjust.trackEvent(newEvent);
		}
	}
}