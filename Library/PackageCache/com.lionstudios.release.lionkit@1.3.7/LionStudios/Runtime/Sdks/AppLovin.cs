﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections.Generic;
using LionStudios.Ads;
using LionStudios.Debugging;

namespace LionStudios
{
	public class AppLovin : BaseSdk<AppLovin>
	{
		public static void Initialize()
		{
			MaxSdkCallbacks.OnSdkInitializedEvent += _Singleton.OnAppLovinSDKInitialized;

			MaxSdk.SetSdkKey(LionSettings.AppLovin.SDKKey);
			MaxSdk.SetUserId(SystemInfo.deviceUniqueIdentifier); // to-do: not sure if we're still supposed to use this. Need verification.
			MaxSdk.SetVerboseLogging(LionSettings.Debugging.DebugLevel == LionDebug.DebugLogLevel.Verbose);
			MaxSdk.InitializeSdk();

			if (GDPR.Completed)
				MaxSdk.SetHasUserConsent(GDPR.AdConsentGranted);

			GDPR.OnClosed += () => MaxSdk.SetHasUserConsent(GDPR.AdConsentGranted);
		}

		void OnAppLovinSDKInitialized(MaxSdkBase.SdkConfiguration sdkConfiguration)
		{
			if (MaxSdk.IsInitialized())
			{
#if DEVELOPMENT_BUILD || UNITY_EDITOR
				MaxSdk.ShowMediationDebugger();
#endif
				//Debug.Log("OnAppLovinSDKInitialized :: " + sdkConfiguration.ConsentDialogState);

				switch (sdkConfiguration.ConsentDialogState)
				{
					case MaxSdkBase.ConsentDialogState.Applies:
						// Show user consent dialog... 
						// Note: If we have previously completed GDPR, the dialog will not appear.
						GDPR.Initialize(GDPR.UserStatus.Applies);
						break;

					case MaxSdkBase.ConsentDialogState.DoesNotApply:
						// No need to show consent dialog, proceed with initialization
						GDPR.Initialize(GDPR.UserStatus.DoesNotApply);
						break;

					case MaxSdkBase.ConsentDialogState.Unknown:
						// Consent dialog state is unknown. Proceed with initialization, but check if the consent
						// dialog should be shown on the next application initialization
						GDPR.Initialize(GDPR.UserStatus.Unknown);

						MaxSdk.SetHasUserConsent(GDPR.AdConsentGranted);
						break;

					default:
						GDPR.Initialize(GDPR.UserStatus.NotSet);
						break;
				}

				InitAds();

				Analytics.OnLogEvent -= OnEventLogged;
				Analytics.OnLogEvent += OnEventLogged;

				var remoteData = LoadRemoteData<RemoteData>();
				if (remoteData.ab_experiment_group != "none")
					Analytics.LogEvent("ab_test", "ab_experiment_group", remoteData.ab_experiment_group);

				OnInitComplete();
				Debug.Log("MaxSDK init Complete.  Consent Dialog State = " + sdkConfiguration.ConsentDialogState);
			}
			else
			{
				Debug.Log("Failed to init MaxSDK");
			}
		}

		protected override void LogEvent(LionGameEvent eventData)
		{
			MaxSdk.TrackEvent(eventData.eventName, StringUtil.ToStringDict(eventData.eventParams));
		}

		class RemoteData
		{
			public string ab_experiment_group = "none";
		}

		public static T LoadRemoteData<T>() where T : class, new()
		{
			if (MaxSdk.IsInitialized() == false)
			{
				Debug.LogWarning("Aborting - Attempting to retrieve remote parameters for an instance before the Max Sdk has initialized. Try using 'AppLovin.WhenInitialized'");
				return null;
			}

			T newInst = new T();
			LoadRemoteData(newInst);
			return newInst;
		}

		public static void LoadRemoteData(object obj)
		{
			if (obj == null)
			{
				Debug.LogError("Aborting - Attempting to load remote data for a null object.");
				return;
			}

			if (MaxSdk.IsInitialized() == false)
			{
				Debug.LogError("Aborting - Attempting to retrieve remote parameters for an instance before the Max Sdk has initialized. Try using 'AppLovin.WhenInitialized'");
				return;
			}

			FieldInfo[] fields = obj.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);// BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetField);

			//Debug.Log("Fields Len = " + fields.Length);
			foreach (var field in fields)
			{
				//object defaultValue = field.GetValue(newInst);
				if ((field.IsDefined(typeof(SerializeField)) || field.Attributes == FieldAttributes.Public) &&
					!field.IsDefined(typeof(NonSerializedAttribute)))
				{
					//Debug.Log("Name = " + field.Name + " :: value = " + defaultValue + " :: Type = " + field.FieldType + " :: attr = " + field.Attributes);
					
					string remoteValStr = MaxSdk.VariableService.GetString(field.Name);
					if (string.IsNullOrEmpty(remoteValStr))
					{
#if DEVELOPMENT_BUILD
						if (!remoteDataValues.Contains(field.Name))
						{
							LionStudios.Debugging.LionDebugger.AddInfo(
								label: field.Name,
								returnStr: () => { return field.GetValue(obj).ToString(); },
								page: "Remote Data");
							remoteDataValues.Add(field.Name);
						}
#endif
						continue;
					}

					Debug.Log("Retrieved Remote Variable :: " + field.Name + " = " + remoteValStr);
					if (field.FieldType == typeof(bool))
					{
						if (bool.TryParse(remoteValStr, out bool value))
						{
							field.SetValue(obj, value);
						}
						// In case they have sent an int and are representing true and false with 1 and 0 respectively, we can try and handle that as well
						else if (int.TryParse(remoteValStr, out int intValue))
						{
							field.SetValue(obj, intValue == 1);
						}
					}
					else if (field.FieldType == typeof(float))
					{
						if (float.TryParse(remoteValStr, out float value))
						{
							field.SetValue(obj, value);
						}
					}
					else if (field.FieldType == typeof(double))
					{
						if (double.TryParse(remoteValStr, out double value))
						{
							field.SetValue(obj, value);
						}
					}
					else if (field.FieldType == typeof(int))
					{
						if (int.TryParse(remoteValStr, out int value))
						{
							field.SetValue(obj, value);
						}
					}
					else if (field.FieldType == typeof(long))
					{
						if (long.TryParse(remoteValStr, out long value))
						{
							field.SetValue(obj, value);
						}
					}
					else if (field.FieldType == typeof(string))
					{
						field.SetValue(obj, remoteValStr);
					}

                    if (LionSettings.Debugging.EnableDebugger
                        && !remoteDataValues.Contains(field.Name))
                    {
						LionStudios.Debugging.LionDebugger.AddInfo(
							label: field.Name,
							returnStr: () => { return field.GetValue(obj).ToString(); },
							page: "Remote Data");
						remoteDataValues.Add(field.Name);
                    }
                    
				}
			}
		}

		private static HashSet<string> remoteDataValues = new HashSet<string>();
		private void InitAds()
		{
			// Whenever GDPR is completed or updated we will want to (re)initialize ads so that we are loading and showing ads from the correct campaigns
			if (GDPR.Completed || GDPR.Status == GDPR.UserStatus.Unknown)
				InitializeAds();

			// Re-init ads whenever the GDPR dialog is closed
			GDPR.OnClosed += InitializeAds;
		}
		
		void InitializeAds()
		{
			if (LionSettings.AppLovin.Enabled == false) return;
			
#if UNITY_EDITOR || DEVELOPMENT_BUILD
			if (string.IsNullOrEmpty(LionSettings.AppLovin.InterstitialAdUnitId) == false)
			{

				LoadCallbackRequest loadInterstitialCallbacks = new LoadCallbackRequest();
				loadInterstitialCallbacks.OnLoaded += adUnitId =>
					Debug.Log("Loaded Interstitial Ad :: Ad Unit ID = " + adUnitId);
				loadInterstitialCallbacks.OnLoadFailed += (adUnitID, error) =>
					Debug.LogError(
						"Failed To Load Interstitial Ad :: Error = " + error + " :: Ad Unit ID = " + adUnitID);
				
				Interstitial.Load(loadInterstitialCallbacks);
			}

			if (string.IsNullOrEmpty(LionSettings.AppLovin.RewardedAdUnitId) == false)
			{
				LoadCallbackRequest loadRewardCallbacks = new LoadCallbackRequest();
				loadRewardCallbacks.OnLoaded +=
					adUnitId => Debug.Log("Loaded Reward Video :: Ad Unit ID = " + adUnitId);
				loadRewardCallbacks.OnLoadFailed += (adUnitID, error) =>
					Debug.LogError("Failed To Load Reward Video :: Error = " + error + " :: Ad Unit ID = " + adUnitID);
				
				RewardedAd.Load(loadRewardCallbacks);
			}

			if (!(Banner.Created || string.IsNullOrEmpty(LionSettings.AppLovin.BannerAdUnitId)))
			{
				LoadCallbackRequest loadBannerCallbacks = new LoadCallbackRequest();
				loadBannerCallbacks.OnLoaded += adUnitId => Debug.Log("Loaded Banner Ad :: Ad Unit ID = " + adUnitId);
				loadBannerCallbacks.OnLoadFailed += (adUnitID, error) =>
					Debug.LogError("Failed To Load Banner Ad :: Error = " + error + " :: Ad Unit ID = " + adUnitID);

				Banner.Create(MaxSdkBase.BannerPosition.BottomCenter, loadBannerCallbacks);
			}
			
#else
			if (string.IsNullOrEmpty(LionSettings.AppLovin.InterstitialAdUnitId) == false)
				Interstitial.Load();
			if (string.IsNullOrEmpty(LionSettings.AppLovin.RewardedAdUnitId) == false)
				RewardedAd.Load();
			if (!(Banner.Created || string.IsNullOrEmpty(LionSettings.AppLovin.BannerAdUnitId)))
				Banner.Create();
#endif
		}

		public static class Mediation
        {
			public static List<string> GetPrivacyLinks()
            {
				Dictionary<string, string> networksDict = new Dictionary<string, string>();
				Dictionary<string, string> usedNetworksDict = new Dictionary<string, string>();

				foreach (Network network in networks)
					networksDict[network.name] = network.privacyLink;

				foreach (string networkName in RewardedNetworks)
					if (networksDict.ContainsKey(networkName))
						usedNetworksDict[networkName] = networksDict[networkName];

				foreach (string networkName in InterstitialNetworks)
					if (networksDict.ContainsKey(networkName))
						usedNetworksDict[networkName] = networksDict[networkName];

				foreach (string networkName in BannerNetworks)
					if (networksDict.ContainsKey(networkName))
						usedNetworksDict[networkName] = networksDict[networkName];

				return new List<string>(usedNetworksDict.Values);
            }

			public static readonly List<string> RewardedNetworks = new List<string>()
			{
				"ADMOB_NETWORK",
				"FACEBOOK_MEDIATE",
				"UNITY_NETWORK",
				"MINTEGRAL_NETWORK",
				"TIKTOK_NETWORK",
				"VUNGLE_NETWORK",
				"FYBER_NETWORK",
				"INMOBI_NETWORK",
				"IRONSOURCE_NETWORK",
			};

			public static readonly List<string> InterstitialNetworks = new List<string>()
			{
				"ADMOB_NETWORK",
				"FACEBOOK_MEDIATE",
				"UNITY_NETWORK",
				"MINTEGRAL_NETWORK",
				"TIKTOK_NETWORK",
				"VUNGLE_NETWORK",
				"FYBER_NETWORK",
				"INMOBI_NETWORK",
				"IRONSOURCE_NETWORK",
			};

			public static readonly List<string> BannerNetworks = new List<string>()
			{
				"ADMOB_NETWORK",
				"FACEBOOK_MEDIATE",
				"MINTEGRAL_NETWORK",
				"AMAZON_NETWORK",
				"FYBER_NETWORK",
				"INMOBI_NETWORK",
				"SMAATO_NETWORK",
			};

			readonly static Network[] networks =
			{
				new Network { name = "ADMOB_NETWORK", privacyLink = "https://policies.google.com/privacy/update" },
				new Network { name = "ADCOLONY_NETWORK", privacyLink = "https://www.adcolony.com/privacy-policy/" },
				new Network { name = "AMAZON_NETWORK", privacyLink = "https://advertising.amazon.com/resources/ad-policy/en/gdpr" },
				new Network { name = "CHARTBOOST_NETWORK", privacyLink = "https://answers.chartboost.com/en-us/articles/200780269" },
				new Network { name = "FYBER_NETWORK", privacyLink = "https://fyber.com/Privacy-policy/" },
				new Network { name = "INMOBI_NETWORK", privacyLink = "https://www.inmobi.com/privacy-policy/" },
				new Network { name = "IRONSOURCE_NETWORK", privacyLink = "https://ironsource.mobi/privacypolicy.html" },
				new Network { name = "MINTEGRAL_NETWORK", privacyLink = "https://www.mintegral.com/en/privacy/" },
				new Network { name = "MYTARGET_NETWORK", privacyLink = "https://target.my.com/help/advertisers/legaldocuments/en" },
				new Network { name = "NEND_NETWORK", privacyLink = "https://www.fancs.com/en/privacy" },
				new Network { name = "PANGLE_NETWORK", privacyLink = "https://www.tiktok.com/legal/privacy-policy?lang=en#privacy-eea" },
				new Network { name = "SMAATO_NETWORK", privacyLink = "https://www.smaato.com/privacy/" },
				new Network { name = "TIKTOK_NETWORK", privacyLink = "https://www.tiktok.com/legal/privacy-policy?lang=en#privacy-eea" },
				new Network { name = "UNITY_NETWORK", privacyLink = "https://unity3d.com/fr/legal/privacy-policy" },
				new Network { name = "VERIZON_NETWORK", privacyLink = "https://www.verizon.com/about/privacy/advertising-programs-privacy-notice" },
				new Network { name = "YANDEX_NETWORK", privacyLink = "https://yandex.com/legal/confidential/" },
				new Network { name = "VUNGLE_NETWORK", privacyLink = "https://vungle.com/privacy/" },
				new Network { name = "FACEBOOK_MEDIATE", privacyLink = "https://www.facebook.com/policy.php" },
			};

			struct Network
            {
				public string name;
				public string privacyLink;
            }
		}
	}
}
