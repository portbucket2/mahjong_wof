﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections.Generic;

#if LK_USE_FIREBASE
using Firebase.Analytics;
using LionStudios.Debugging;
#endif

namespace LionStudios
{
	public class FirebaseSdk : BaseSdk<FirebaseSdk>
	{
		public const string PkgNameCoreApp = "com.google.firebase.app";
		public const string PkgNameAnalytics = "com.google.firebase.analytics";
		
		public const string coreAppMaxVersion = "7.1.0";
		public const string analyticsMaxVersion = "7.1.0";

#if LK_USE_FIREBASE
		public static void Initialize()
		{
			Debug.Log("Initialize Firebase");
			Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
			{
				var dependencyStatus = task.Result;
				if (dependencyStatus == Firebase.DependencyStatus.Available)
				{
					// Create and hold a reference to your FirebaseApp,
					// where app is a Firebase.FirebaseApp property of your application class.
					//app = Firebase.FirebaseApp.DefaultInstance;
					Debug.Log("Firebase: successfully managed dependencies and ready to use");
					_Singleton.OnInitComplete();
				}
				else
				{
					Debug.LogError($"Firebase: Could not resolve all Firebase dependencies: {dependencyStatus}");
					// Firebase Unity SDK is not safe to use here.
				}
			});

			if (LionSettings.Firebase.RegisterForStandardEvents)
			{
				LionDebug.Log("Firebase - Register for standard events");
				Analytics.OnLogEvent += _Singleton.OnEventLogged;
			}
			
			if (LionSettings.Firebase.RegisterForUAEvents)
			{
				Analytics.OnLogEventUA += _Singleton.OnEventLogged;	
			}
		}
#endif

		protected override void LogEvent(LionGameEvent eventData)
		{
#if LK_USE_FIREBASE
			if (string.IsNullOrEmpty(eventData.eventName))
				return;
			
			if (eventData.eventParams != null)
			{
				var parameters = new Parameter[eventData.eventParams.Count];
				int index = 0;
				foreach (var kvp in eventData.eventParams)
				{
					if (string.IsNullOrEmpty(kvp.Key) == false)
					{
						switch (kvp.Value)
						{
							case int value:
								parameters[index] = new Parameter(kvp.Key, value);
								break;
							case long value:
								parameters[index] = new Parameter(kvp.Key, value);
								break;
							case float value:
								parameters[index] = new Parameter(kvp.Key, value);
								break;
							case double value:
								parameters[index] = new Parameter(kvp.Key, value);
								break;
							default:
								parameters[index] = new Parameter(kvp.Key, kvp.Value.ToString());
								break;
						}
					}

					index++;
				}

				//log event to Firebase
				FirebaseAnalytics.LogEvent(eventData.eventName, parameters);
			}
			else
			{
				FirebaseAnalytics.LogEvent(eventData.eventName);
			}
			LionDebug.Log($"Log firebase event: {eventData.eventName} : Param Count = {eventData.eventParams?.Count ?? 0}");
#endif
		}
	}
}