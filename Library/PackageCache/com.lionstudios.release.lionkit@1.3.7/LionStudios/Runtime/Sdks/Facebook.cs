﻿using LionStudios.Debugging;
using UnityEngine;
#if UNITY_ANDROID
using System.Collections.Generic; // used in android builds
#elif UNITY_IOS
using System.Runtime.InteropServices; // used in ios builds
#endif

namespace LionStudios.Runtime
{
	public class Facebook : BaseSdk<Facebook>
	{
		public static void Initialize()
		{
			// Add check for valid facebook app id
			// grab this functionality from unity fb sdk source code

			_Singleton.OnFacebookInitComplete();
		}

		void OnFacebookInitComplete()
		{
			Analytics.OnLogEvent -= OnEventLogged;
			Analytics.OnLogEvent += OnEventLogged;
			OnInitComplete();
		}

		protected override void LogEvent(LionGameEvent gameEvent)// string eventName, Dictionary<string, object> eventData = null)
		{
			Debugging.LionDebug.Log("Tracking FB Event '" + gameEvent.eventName + "'", LionDebug.DebugLogLevel.Event);
#if UNITY_EDITOR
			return;
#elif UNITY_IOS
			int paramCount = gameEvent.eventParams.Count;
			string[] paramKeys = new string[paramCount];
			string[] paramValues = new string[paramCount];
			int index = 0;
			foreach (var kvp in gameEvent.eventParams)
            {
				paramKeys[index] = kvp.Key;
				paramValues[index] = kvp.Value.ToString();
				index++;
            }
			IOSFBAppEventsLogEvent(gameEvent.eventName, 0.0, paramCount, paramKeys, paramValues);
			
#elif UNITY_ANDROID
			using (AndroidJavaObject bundle = new AndroidJavaObject("android.os.Bundle"))
			{
				if (gameEvent.eventParams != null)
				{
					foreach (KeyValuePair<string, object> kvp in gameEvent.eventParams)
					{
						bundle.Call("putString", kvp.Key, kvp.Value.ToString());
					}
				}
				_Singleton.FacebookNative().Call("LogEvent", gameEvent.eventName, bundle);
			}
#else
#endif
		}

#if UNITY_IOS
		[DllImport("__Internal")]
		private static extern void IOSFBAppEventsLogEvent(string logEvent, double valueToSum, int numParams, string[] paramKeys, string[] paramVals);
#endif

#if UNITY_ANDROID
		AndroidJavaObject FacebookNative()
		{
			if (_FacebookJavaObj == null)
				_FacebookJavaObj = new AndroidJavaObject("com.lionstudios.lionkit.Facebook", GetCurrentActivity());

			return _FacebookJavaObj;
		}

		private static AndroidJavaObject GetCurrentActivity()
		{
			AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			return unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		}

		AndroidJavaObject _FacebookJavaObj;
#endif
	}
}
