﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Reflection;


namespace LionStudios
{
	public abstract class BaseSdk<T> where T : BaseSdk<T>, new()
	{
#region Static
		public static bool IsInitialized { get { return _Singleton._IsInitialized; } }

		/// <summary>
		/// This will execute when the Sdk is initialized.
		/// If it's already intitialized then it will immediately call the method.
		/// </summary>
		/// <param name="action">Action.</param>
		public static void WhenInitialized(Action action)
		{
			if (action != null)
			{
				if (IsInitialized)
				{
					action();
				}
				else
				{
					_Singleton._OnInitialized += action;
				}
			}
		}

		protected static T _Singleton
		{
			get
			{
				if (_SingletonInternal == null)
				{
					_SingletonInternal = new T();
				}
				return _SingletonInternal;
			}
		}

		static T _SingletonInternal;
#endregion

		protected void OnInitComplete()
		{
			_Singleton._IsInitialized = true;
			_Singleton._OnInitialized?.Invoke();
			ProcessEventQueue();
		}

		internal void OnEventLogged(LionGameEvent gameEvent)
        {
			if (_IsInitialized)
            {
				LogEvent(gameEvent);
            }
			else
			{
				_EventQueue.Enqueue(gameEvent);
			}
		}

		protected abstract void LogEvent(LionGameEvent gameEvent);

		void ProcessEventQueue()
		{
			while (_EventQueue.Count > 0)
				LogEvent(_EventQueue.Dequeue());
		}

		protected bool _IsInitialized;
		protected event Action _OnInitialized;

		static Queue<LionGameEvent> _EventQueue = new Queue<LionGameEvent>();
	}
}