﻿using System.Security;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using LionStudios.Debugging;

namespace LionStudios
{
    public static class Http 
    {
        class Game
        {
            public string name = null;
            public string bundleId = null;
            public string platform = null;
            public string lionKitVersion = null;
            public string unityVersion = null;
            public string gdprPrivacy = null;
        }

        private const string GameURL = "http://35.197.40.49:3000/api/games";
        private const string AuthString = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiJsaW9ua2l0IiwiaWF0IjoxNjAyMzY5Nzc3fQ.Rnm6d6jkmq-Geu6iYBmGZkgeeBScmmqqlJJhox6608U";
        
        static async void PostGame(Game game)
        {
            if (game == null)
                return;
            
            // try get game info
            

            // secure game info string
            using (SecureString postData = new SecureString())
            {
                foreach (char c in JsonUtility.ToJson(game))
                    postData.AppendChar(c);

                UnityWebRequest webRequest = new UnityWebRequest(GameURL, "PATCH")
                {
                    uploadHandler = (UploadHandler) new UploadHandlerRaw(Encoding.UTF8.GetBytes(postData.ToString())),
                    downloadHandler = (DownloadHandler) new DownloadHandlerBuffer()
                };
                
                webRequest.SetRequestHeader("Authorization", AuthString);
                webRequest.SetRequestHeader("Content-Type", "application/json");
                await webRequest.SendWebRequest();

                if (webRequest.isNetworkError)
                {
                    LionDebug.LogWarning($"Failed to post game data. Reason: {webRequest.error}");
                }
                
                postData.Dispose();
            }
        }

        public static void AddGame()
        {
            if (Application.installMode != ApplicationInstallMode.Store)
            {
                string platform = "unknown";
#if UNITY_EDITOR
                platform = "editor";
#elif UNITY_IOS
                platform = "ios";
#elif UNITY_ANDROID
                platform = "android";
#endif
                Game newGame = new Game
                {
                    name = Application.productName,
                    bundleId = Application.identifier,
                    platform = platform,
                    lionKitVersion = LionKit.Version,
                    unityVersion = Application.unityVersion,
                    gdprPrivacy = LionSettings.GDPR.PrivacyLinks.Count > 0 ? "GDPR Privacy Exists" : "GDPR Privacy Links Not Filled"
                };
                
                PostGame(newGame);
            }
        }
    }
}

