﻿using UnityEngine;
using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace LionStudios
{
    //[CreateAssetMenu(menuName = "LionStudios/Dep")]
    public class LionDep : ScriptableObject
    {
        public List<PlistEntry> plistEntries = null;
        public List<iosPod> iosPods = null;
        public List<androidPackage> androidPackages = null;
    }

    [Serializable]
    public class PlistEntry
    {
        public string key = null;
        public string value = null;
    }

    [Serializable]
    public class iosPod
    {
        [XmlAttribute]
        public string name = null;

        [XmlAttribute]
        public string version = null;

        [XmlAttribute]
        public bool bitcodeEnabled = true;

        [XmlAttribute]
        public string minTargetSdk = "8.0";
    }

    [Serializable]
    public class androidPackage
    {
        [XmlAttribute]
        public string spec;

        public androidSdkPackageIds androidSdkPackageIds;
    }

    [Serializable]
    public class androidSdkPackageIds
    {
        [XmlElement(ElementName = "androidSdkPackageId")]
        public List<string> ids;
    }

    public class dependencies
    {
        public List<iosPod> iosPods;
        public List<androidPackage> androidPackages;
    }
}