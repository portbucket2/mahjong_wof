using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LionStudios.Ads
{
    public class Banner : BaseAdType<Banner>
    {
        #region Persistent Data

        private Data _persistentData;

        [Serializable]
        internal class Data : PersistentData<Banner.Data>
        {
            [SerializeField] public int _totalClicked = 0;
        }

        #endregion

        #region Remote Config

        [Serializable]
        internal class RemoteConfig
        {
            public bool _bannersDisabled = false;
        }

        public static bool BannersDisabled = false;

        #endregion

        #region Static
        public static bool IsShowing { get; private set; }

        public static bool IsAdReady
        {
            get { return _Singleton != null && _Singleton.IsAdReady_Internal(null); }
        }
        
        public static bool Created { get; private set; }
        
        public static void Create(MaxSdk.BannerPosition bannerPosition = MaxSdkBase.BannerPosition.BottomCenter, Ads.LoadCallbackRequest loadCallbackRequest = null)
        {
            if (_Singleton == null)
            {
                _Singleton = new Banner();
            }

            if (BannersDisabled == false)
            {
                _Singleton.CreateBanner(bannerPosition, loadCallbackRequest);
            }
        }

        public static void Destroy()
        {
            _Singleton?.DestroyBanner();
        }

        public static void Show(Ads.ShowAdRequest request = null)
        {
            if (_Singleton == null)
            {
                _Singleton = new Banner();
            }

            if (BannersDisabled == false && !GDPR.Active)
            {
                ShowAdInternal(request);
                IsShowing = true;
            }
        }

        public static void Hide()
        {
            if (_Singleton != null)
                MaxSdk.HideBanner(_Singleton._AdUnitId);
            
            IsShowing = false;
        }

        public static void SetBannerBackgroundColor(Color color)
        {
            if (!string.IsNullOrEmpty(_Singleton._AdUnitId))
                MaxSdk.SetBannerBackgroundColor(_Singleton._AdUnitId, color);
        }

        #endregion

        #region Instance

        public Banner() : base()
        {
            // load persistent data
            _persistentData = Data.Load();

            RemoteConfig config = new RemoteConfig { _bannersDisabled = LionSettings.RemoteConfig.BannersDisabled };

            AppLovin.LoadRemoteData(config);
            BannersDisabled = config._bannersDisabled;

            GDPR.OnOpen += () =>
            {
                _BannerWasOpen = IsShowing;
                Hide();
            };

            GDPR.OnClosed += () =>
            {
                //Debug.Log("On GDPR Closed - BannerWasOpen = " + _BannerWasOpen);
                if (_BannerWasOpen)
                    Show(_ShowRequest);
            };
        }

        private bool _BannerWasOpen;


        protected override void InitCallbacks()
        {
            // Attach callback
            MaxSdkCallbacks.OnBannerAdLoadedEvent += OnLoadedEvent;
            MaxSdkCallbacks.OnBannerAdLoadFailedEvent += OnLoadFailedEvent;

            MaxSdkCallbacks.OnBannerAdExpandedEvent += OnDisplayedEvent;
            MaxSdkCallbacks.OnBannerAdClickedEvent += OnClickedEvent;
            MaxSdkCallbacks.OnBannerAdCollapsedEvent += OnHiddenEvent;
        }

        protected override string GetAdUnitId()
        {
            return LionSettings.AppLovin.BannerAdUnitId;
        }

        protected override void OnLoadedEvent(string adUnitId)
        {
            base.OnLoadedEvent(adUnitId);
            Created = true;
        }

        void CreateBanner(MaxSdk.BannerPosition bannerPosition, LoadCallbackRequest loadCallbackRequest = null)
        {
            Debug.Log("Create Banner");
            _LoadRequest = loadCallbackRequest;

            if (GDPR.Active)
            {
                Debug.LogWarning(
                    "LionAdManager :: Failed to create banner :: Waiting for user to accept GDPR settings");
                return;
            }

            if (!string.IsNullOrEmpty(GetAdUnitId()))
            {
                MaxSdk.CreateBanner(_AdUnitId, bannerPosition);
                MaxSdk.SetBannerBackgroundColor(_AdUnitId, LionSettings.AppLovin.BannerAdBackgroundColor);
            }
            else
            {
                Debug.LogErrorFormat(
                    "You are attempting to create a banner ad, but the Applovin Banner Ad Unit Id is null or empty.  Please correct this in the LionKit Settings Panel.");
            }
        }

        void DestroyBanner()
        {
            MaxSdk.DestroyBanner(_AdUnitId);
        }

        protected override void LoadAd()
        {
            
        }

        protected override bool IsAdReady_Internal(Ads.ShowAdRequest request)
        {
            return true;
        }

        protected override void ShowAd()
        {
            //Debug.Log("Show Banner :: " + _AdUnitId);
            if (!string.IsNullOrEmpty(GetAdUnitId()))
            {
                MaxSdk.ShowBanner(_AdUnitId);
            }
            else
            {
                Debug.LogErrorFormat(
                    "You are attempting to show a banner ad, but the Applovin Banner Ad Unit Id is null or empty.  Please correct this in the LionKit Settings Panel.");
            }
        }

        #endregion
    }
}