﻿using System.Collections;
using UnityEngine;
using LionStudios;
using LionStudios.Debugging;

#if LK_USE_APP_REVIEW
#if UNITY_ANDROID
using Google.Play.Review;
#elif UNITY_IOS
using UnityEngine.iOS;
#endif
#endif

public class LionInAppReview
{
	public const string GoogleCommonPkgId = "com.google.play.common";
	public const string GoogleCommonPkgVer = "1.3.0";
	
	public const string GoogleReviewPkgId = "com.google.play.review";
	public const string GoogleReviewPkgVer = "1.3.0";

#if LK_USE_APP_REVIEW
	#if UNITY_ANDROID
	private static Coroutine _currentReview = null;
	private static ReviewManager _reviewManager = null;
	private static PlayReviewInfo _reviewInfo = null;
	
	public static void TryGetReview()
	{
		if (_currentReview != null)
		{
			LionKit.HNDL.StopCoroutine(_currentReview);
			_currentReview = null;
		}
		
		if (_reviewManager == null)
		{
			_reviewManager = new ReviewManager();
		}
		_currentReview = LionKit.HNDL.StartCoroutine(AndroidReview());
	}
	
	static IEnumerator AndroidReview()
	{
		LionDebug.Log("Requesting App Review...",
			LionDebug.DebugLogLevel.Verbose);
		
		var requestFlowOperation = _reviewManager.RequestReviewFlow();
		yield return requestFlowOperation;

		if (requestFlowOperation.Error != ReviewErrorCode.NoError)
		{
			LionDebug.Log( requestFlowOperation.Error.ToString());
			yield break;
		}

		_reviewInfo = requestFlowOperation.GetResult();
		var launchFlowOperation = _reviewManager.LaunchReviewFlow(_reviewInfo);
		yield return launchFlowOperation;

		_reviewInfo = null;
		if (launchFlowOperation.Error != ReviewErrorCode.NoError)
		{
			LionDebug.Log(launchFlowOperation.Error.ToString());
			yield break;
		}

		LionDebug.Log("Review request complete!",
			LionDebug.DebugLogLevel.Verbose);
	}
	#endif
	
	#if UNITY_IOS
	public static void TryGetReview()
	{
		Device.RequestStoreReview();
	}
	#endif

#endif
}
