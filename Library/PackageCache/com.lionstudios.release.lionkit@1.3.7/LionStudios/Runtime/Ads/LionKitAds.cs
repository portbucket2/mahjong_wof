using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LionStudios.Ads
{
    public class LoadCallbackRequest
    {
        public Action<string> OnLoaded { get; set; }
        public Action<string, int> OnLoadFailed { get; set; }
    }
    
    public class ShowAdRequest
    {

        public ShowAdRequest()
        {
        }

        public ShowAdRequest(string placement = null)
        {
            SetPlacement(placement);
        }

        /// <summary>
        /// Set the ad placement to have it passed as a parameter with ad events.
        /// </summary>
        /// <param name="placement"></param>
        public void SetPlacement(string placement)
        {
            if (string.IsNullOrEmpty(placement) == false) EventParams[Analytics.Key.Param.placement] = placement;
        }

        /// <summary>
        /// Set the level to have it passed as a parameter with ad events and to determine if the player should be shown interstitials.
        /// </summary>
        /// <param name="level"></param>
        public void SetLevel(int level)
        {
            if (level >= 0) EventParams[Analytics.Key.Param.level] = level;
        }

        public bool LevelSet()
        {
            return EventParams.ContainsKey(Analytics.Key.Param.level);
        }

        /// <summary>
        /// Get the level.
        /// </summary>
        /// <param name="level"></param>
        public int GetLevel()
        {
            if (EventParams.ContainsKey(Analytics.Key.Param.level)) return (int) EventParams[Analytics.Key.Param.level];

            return 0;
        }

        /// <summary>
        /// Sets or adds an event param that will be fired with all ad callbacks
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="paramValue"></param>
        public void SetEventParam(string paramName, object paramValue)
        {
            if (string.IsNullOrEmpty(paramName) == false) EventParams[paramName] = paramValue;
        }

        /// <summary>
        /// Removes an event param
        /// </summary>
        /// <param name="paramName"></param>
        public void RemoveEventParam(string paramName)
        {
            if (EventParams.ContainsKey(paramName)) EventParams.Remove(paramName);
        }

        /// <summary>
        /// Clears the event parameters
        /// </summary>
        public void ClearEventParams()
        {
            EventParams.Clear();
        }

        /// <summary>
        /// If true, analytics events will be fired when the ad is displayed and when watched
        /// </summary>
        public bool sendAnalyticsEvents = true;

        /// <summary>
        /// Event params that will be sent with an analytics events 
        /// </summary>
        internal readonly Dictionary<string, object> EventParams = new Dictionary<string, object>();

        /// <summary>
        /// Fired when an ad is displayed (may not be received by Unity until the ad closes)
        /// </summary>
        public Action<string> OnDisplayed;

        /// <summary>
        /// Fired when an ad is clicked (may not be received by Unity until the ad closes)
        /// </summary>
        public Action<string> OnClicked;

        /// <summary>
        /// Fired when an ad is hidden (this happens when the ad is closed or dismissed by the user)
        /// </summary>
        public Action<string> OnHidden;

        /// <summary>
        /// Fired when an ad fails to play. Includes the error message.
        /// </summary>
        public Action<string, int> OnFailedToDisplay;

        /// <summary>
        /// Fired when a rewarded video completes. Includes information about the reward
        /// </summary>
        public Action<string, MaxSdk.Reward> OnReceivedReward;
    }

    public abstract class BaseAdType<T> where T : BaseAdType<T>
    {
        protected static T _Singleton;

        protected BaseAdType()
        {
            _Singleton = this as T;
            TimeLastShown = float.MinValue;
            _AdUnitId = GetAdUnitId();
            InitCallbacks();
            LoadAd();
        }
        
        protected abstract void InitCallbacks();
        protected abstract string GetAdUnitId();
        protected abstract void LoadAd();
        protected abstract bool IsAdReady_Internal(Ads.ShowAdRequest request);
        protected abstract void ShowAd();

        public void LoadAd(Ads.LoadCallbackRequest request)
        {
            if (LionSettings.AppLovin.Enabled == false) return;
            if (GDPR.Active)
            {
                Debug.LogWarning("LionAdManager :: Failed to load ad :: Waiting for user to accept GDPR settings");
                return;
            }

            _LoadRequest = request;
            LoadAd();
        }

        protected virtual void OnLoadedEvent(string adUnitId)
        {
            // Ad is ready to be shown. MaxSdk.IsInterstitialReady(interstitialAdUnitId) will now return 'true'
            _LoadRequest?.OnLoaded?.Invoke(adUnitId);
        }

        protected void OnLoadFailedEvent(string adUnitId, int errorCode)
        {
            _LoadRequest?.OnLoadFailed?.Invoke(adUnitId, errorCode);

            // Ad failed to load. We recommend re-trying loading the ad in 5 seconds.
            LionKit.Invoke(LoadAd, 5f);
        }

        Dictionary<string, object> _FailedToDisplayEventParams = new Dictionary<string, object>();

        protected void OnFailedToDisplayEvent(string adUnitId, int errorCode)
        {
            if (_ShowRequest?.OnFailedToDisplay != null)
            {
                _ShowRequest.OnFailedToDisplay(adUnitId, errorCode);

                if (_ShowRequest.sendAnalyticsEvents)
                {
                    _FailedToDisplayEventParams.Clear();
                    foreach (var kvp in _ShowRequest.EventParams) _FailedToDisplayEventParams[kvp.Key] = kvp.Value;

                    _FailedToDisplayEventParams["errorCode"] = errorCode;

                    if (this is Ads.RewardedAd)
                        Analytics.Events.RewardVideoFailedToDisplay(_FailedToDisplayEventParams);
                    else if (this is LionStudios.Ads.Interstitial)
                        Analytics.Events.InterstitialFailedToDisplay(_FailedToDisplayEventParams);
                }
            }

            // Ad failed to display. We recommend loading the next ad
            LoadAd();
        }

        protected void OnHiddenEvent(string adUnitId)
        {
            if (_ShowRequest != null && _ShowRequest.OnHidden != null)
            {
                _ShowRequest.OnHidden(adUnitId);

                if (_ShowRequest.sendAnalyticsEvents)
                {
                    if (this is Ads.RewardedAd)
                        Analytics.Events.RewardVideoEnd(_ShowRequest.EventParams);
                    else if (this is Ads.Interstitial) Analytics.Events.InterstitialEnd(_ShowRequest.EventParams);
                }
            }

            TimeLastShown = Time.time;
            // Interstitial ad is hidden. Pre-load the next ad
            LoadAd();
        }

        protected void OnDisplayedEvent(string adUnitId)
        {
            if (_ShowRequest != null && _ShowRequest.OnDisplayed != null)
            {
                _ShowRequest.OnDisplayed(adUnitId);

                if (_ShowRequest.sendAnalyticsEvents)
                {
                    if (this is Ads.RewardedAd)
                        Analytics.Events.RewardVideoStart(_ShowRequest.EventParams);
                    else if (this is Ads.Interstitial) Analytics.Events.InterstitialStart(_ShowRequest.EventParams);
                }
            }

            TimeLastShown = Time.time;
        }

        protected virtual void OnClickedEvent(string adUnitId)
        {
            if (_ShowRequest != null && _ShowRequest.OnClicked != null)
            {
                _ShowRequest.OnClicked(adUnitId);

                if (_ShowRequest.sendAnalyticsEvents)
                {
                    if (this is Ads.RewardedAd)
                        Analytics.Events.RewardVideoClicked(_ShowRequest.EventParams);
                    else if (this is Ads.Interstitial)
                        Analytics.Events.InterstitalVideoClicked(_ShowRequest.EventParams);
                }
            }
        }

        protected static void ShowAdInternal(Ads.ShowAdRequest request = null)
        {
            //Debug.Log("Show Rewarded :: " + _AdUnitId);
            if (request != null && request.sendAnalyticsEvents)
            {
                if (_Singleton is Ads.RewardedAd)
                    Analytics.Events.RewardVideoShowRequest(request.EventParams);
                else if (_Singleton is Ads.Interstitial) Analytics.Events.InterstitialShowRequest(request.EventParams);
            }

            //Debug.Log("Show " + _Singleton.GetType().Name + " :: Ready = " + _Singleton.IsAdReady());
            if (GDPR.Active)
            {
                Debug.LogWarning("LionAdManager :: Failed to show Ad :: Waiting for user to accept GDPR settings");
                return;
            }
            
            _Singleton._ShowRequest = request;
            if (_Singleton.IsAdReady_Internal(request))
            {
                _Singleton.ShowAd();
            }
            else
            {
                _Singleton.OnFailedToDisplayEvent(_Singleton._AdUnitId, -1);
                Debug.LogWarning("LionAdManager :: Failed to show Ad :: Ad not ready");
            }
        }
        
        public bool AdReady(ShowAdRequest request)
        {
            return IsAdReady_Internal(request);
        }

        public static float TimeLastShown { get; protected set; }

        protected string _AdUnitId;
        protected Ads.ShowAdRequest _ShowRequest;
        protected Ads.LoadCallbackRequest _LoadRequest;
    }
}