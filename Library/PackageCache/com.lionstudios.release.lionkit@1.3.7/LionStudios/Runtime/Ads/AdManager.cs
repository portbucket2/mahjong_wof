﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace LionStudios
{
    /// <summary>
    /// AdManager handles initialization of max ad system
    ///
    /// Ad callbacks should be assigned be delegated by creating a new callback class and passing that with calls to show.
    /// </summary>
    public static class AdManager
    {
        [Obsolete("AdManager.LoadCallbackRequest is obsolete. Use LionStudios.Ads.LoadCallbackRequest instead")]
        public class LoadCallbackRequest : Ads.LoadCallbackRequest {}
        
        [Obsolete("AdManager.ShowAdRequest is obsolete. Use LionStudios.Ads.ShowAdRequest instead")]
        public class ShowAdRequest : Ads.ShowAdRequest {}

        public sealed class Interstitial
        {
            [Obsolete("'LionStudios.AdManager.Interstitial' is deprecated. Use 'LionStudios.Ads.Interstitial' instead")]
            public static void Load(Ads.LoadCallbackRequest loadCallbackRequest = null)
            {
                LionStudios.Ads.Interstitial.Load(loadCallbackRequest);
            }

            [Obsolete("'LionStudios.AdManager.Interstitial' is deprecated. Use 'LionStudios.Ads.Interstitial' instead")]
            public static void Show(Ads.ShowAdRequest request = null, int level = -1)
            {
                LionStudios.Ads.Interstitial.Show(request, level);
            }

            [Obsolete("'LionStudios.AdManager.Interstitial' is deprecated. Use 'LionStudios.Ads.Interstitial' instead")]
            public static bool IsAdReady { get { return Ads.Interstitial.IsAdReady; } }
        }

        public sealed class Rewarded
        {
            [Obsolete("'LionStudios.AdManager.Rewarded' is deprecated. Use 'LionStudios.Ads.RewardedAd' instead")]
            public static void Load(Ads.LoadCallbackRequest loadCallbackRequest = null)
            {
                Ads.RewardedAd.Load(loadCallbackRequest);
            }

            /// <summary>
            /// Call this to show a rewarded ad.
            /// The optional 'request' param contains callbacks and event data and will be fired when various events occur within the lifetime of the ad.
            /// The optional 'level' param allows our level to automatically be logged when showing ads.
            /// </summary>
            /// <param name="request">The optional 'request' param contains callbacks and event data and will be
            /// fired when various events occur within the lifetime of an ad.</param>
            /// <param name="level">The optional 'level' param allows our level to automatically be logged when showing ads.</param>
            [Obsolete("'LionStudios.AdManager.Rewarded' is deprecated. Use 'LionStudios.Ads.RewardedAd' instead")]
            public static void Show(Ads.ShowAdRequest request = null, int level = -1)
            {
                Ads.RewardedAd.Show(request, level);
            }
            
            [Obsolete("'LionStudios.AdManager.Rewarded' is deprecated. Use 'LionStudios.Ads.RewardedAd' instead")]
            public static bool IsAdReady { get { return Ads.RewardedAd.IsAdReady; } }
        }

        public sealed class Banner
        {
            [Obsolete("'LionStudios.AdManager.Banner' is deprecated. Use 'LionStudios.Ads.Banner' instead")]
            public static void Create(MaxSdk.BannerPosition bannerPosition = MaxSdkBase.BannerPosition.BottomCenter,
                LoadCallbackRequest loadCallbackRequest = null)
            {
                Ads.Banner.Create(bannerPosition, loadCallbackRequest);
            }

            public static void Destroy()
            {
                Ads.Banner.Destroy();
            }

            public static void Show(Ads.ShowAdRequest request = null)
            {
                Ads.Banner.Show(request);
            }

            public static void Hide()
            {
                Ads.Banner.Hide();
            }

            public static bool IsAdReady
            {
                get { return Ads.Banner.IsAdReady; }
            }

            public static void SetBannerBackgroundColor(Color color)
            {
                Ads.Banner.SetBannerBackgroundColor(color);
            }
        }
    }
}