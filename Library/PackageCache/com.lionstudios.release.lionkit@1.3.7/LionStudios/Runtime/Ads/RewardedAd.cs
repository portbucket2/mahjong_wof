﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LionStudios.Ads
{
	public class RewardedAd : BaseAdType<RewardedAd>
	{
		#region Persistent Data
		private Data _persistentData;
		internal class Data : PersistentData<RewardedAd.Data>
		{
			public int totalWatched = 0;
			public int totalClicked = 0;
		}
		#endregion
		
		#region Keys
		const string key_event_rv_show_request = "rv_show_request";
		const string key_event_rv_failed_to_display = "rv_failed_to_display";
		const string key_event_rv_start = "rv_start";
		const string key_event_rv_end = "rv_end";
		const string key_event_rv_reward_received = "rv_reward_received";
		const string key_event_rv_click = "rv_click";
		const string key_event_rv_watch_count = "watch_rewarded_{0}";
		const string key_event_rv_click_count = "click_rewarded_{0}";
		#endregion

		#region Static
		/// <summary>
		/// Call this to determine if a rewarded ad is ready to be displayed.
		/// </summary>
		public static bool IsAdReady
		{
			get { return _Singleton != null && _Singleton.IsAdReady_Internal(null); }
		}

		public static void Load(Ads.LoadCallbackRequest loadCallbackRequest = null)
		{
			if (_Singleton == null)
			{
				_Singleton = new RewardedAd();
			}

			_Singleton._persistentData = RewardedAd.Data.Load();
			_Singleton.LoadAd(loadCallbackRequest);
		}

		/// <summary>
		/// Call this to show a rewarded ad.
		/// The optional 'request' param contains callbacks and event data and will be fired when various events occur within the lifetime of the ad.
		/// The optional 'level' param allows our level to automatically be logged when showing ads.
		/// </summary>
		/// <param name="request">The optional 'request' param contains callbacks and event data and will be
		/// fired when various events occur within the lifetime of an ad.</param>
		/// <param name="level">The optional 'level' param allows our level to automatically be logged when showing ads.</param>
		public static void Show(Ads.ShowAdRequest request = null, int level = -1)
		{
			if (_Singleton == null)
				_Singleton = new RewardedAd();

			if (request != null && level >= 0)
				request.SetLevel(level);

			ShowAdInternal(request);
		}
		
		public static int GetTotalWatched()
		{
			return _Singleton._persistentData.totalWatched;
		}

		public static int GetTotalClicked()
		{
			return _Singleton._persistentData.totalClicked;
		}

		public static void ClearTotalWatched()
		{
			_Singleton._persistentData.totalWatched = 0;
			_Singleton._persistentData.SaveLocal();
		}

		public static void ClearTotalClicked()
		{
			_Singleton._persistentData.totalClicked = 0;
			_Singleton._persistentData.SaveLocal();
		}

		internal static void ThrowAdShowRequest(Dictionary<string, object> payload)
		{
			// throw start
			Analytics.LogEvent(key_event_rv_show_request, payload);
		}

		internal static void ThrowAdFailedToDisplay(Dictionary<string, object> payload)
		{
			// throw start
			Analytics.LogEvent(key_event_rv_failed_to_display, payload);
		}

		internal static void ThrowAdStart(Dictionary<string, object> payload)
		{
			// throw start
			Analytics.LogEvent(key_event_rv_start, payload);
		}

		internal static void ThrowAdEnd(Dictionary<string, object> payload)
		{
			Analytics.LogEvent(key_event_rv_end, payload);
		}

		internal static void ThrowAdRewardReceived(Dictionary<string, object> payload)
		{
			// throw watch count
			int watchCount = GetTotalWatched();
			_Singleton._persistentData.totalWatched = ++watchCount;
			_Singleton._persistentData.SaveLocal();
			
			if (watchCount == 5 || watchCount == 15)
				Analytics.LogEventUA(string.Format(key_event_rv_watch_count, watchCount));
			
			Analytics.LogEvent(key_event_rv_reward_received, payload);
		}

		internal static void ThrowAdClicked(Dictionary<string, object> eventParams)
		{
			// click count
			// int clickCount = GetTotalClicked();
			// _Singleton._persistentData.totalClicked = ++clickCount;
			// _Singleton._persistentData.SaveLocal();
			//
			// Analytics.LogEventUA(string.Format(key_event_rv_click_count, clickCount));
			Analytics.LogEvent(key_event_rv_click, eventParams);
		}
		#endregion
		
		#region Instance
		protected override void InitCallbacks()
            {
                // Attach callback
                MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnLoadedEvent;
                MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnLoadFailedEvent;

                MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnDisplayedEvent;
                MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;
                MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnHiddenEvent;
                MaxSdkCallbacks.OnRewardedAdClickedEvent += OnClickedEvent;
                MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnFailedToDisplayEvent;
            }

        protected override string GetAdUnitId()
        {
            return LionSettings.AppLovin.RewardedAdUnitId;
        }

        protected override void LoadAd()
        {
            if (!string.IsNullOrEmpty(_AdUnitId))
            {
                MaxSdk.LoadRewardedAd(_AdUnitId);
            }
            else
            {
                Debug.LogErrorFormat("You are attempting to load a rewarded ad, but the Applovin Rewarded Ad Unit Id is null or empty.  Please correct this in the LionKit Settings Panel.");
            }
        }

        protected override bool IsAdReady_Internal(Ads.ShowAdRequest request)
        {
            return MaxSdk.IsRewardedAdReady(_AdUnitId);
        }

        protected override void ShowAd()
        {
            if (!string.IsNullOrEmpty(_Singleton.GetAdUnitId()))
            {
                MaxSdk.ShowRewardedAd(_AdUnitId);
            }
            else
            {
                Debug.LogErrorFormat("You are attempting to show a rewarded ad, but the Applovin Rewarded Ad Unit Id is null or empty.  Please correct this in the LionKit Settings Panel.");
            }
        }

        private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
            {
                if (_ShowRequest != null && _ShowRequest.OnReceivedReward != null)
                {
                    _ShowRequest.OnReceivedReward(adUnitId, reward);
                    if (_ShowRequest != null && _ShowRequest.sendAnalyticsEvents)
                        Analytics.Events.RewardVideoRewardReceived(_ShowRequest.EventParams);

                    TimeLastShown = Time.time;
                }
                else
                {
                    Debug.LogWarningFormat("Rewarded ad failed: No reward specified for OnReceivedReward: {0}", adUnitId);
                }
            }
		#endregion
	}
}