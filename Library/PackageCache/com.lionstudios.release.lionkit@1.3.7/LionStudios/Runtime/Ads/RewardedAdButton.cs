﻿using UnityEngine;

namespace LionStudios
{
    public abstract class RewardedAdButton : AdButton
    {
        sealed protected override void Load()
        {
            // Initialize .. no problem if this is called multiple times
            Ads.RewardedAd.Load(_LoadRewardCallbacks);
        }

        sealed protected override void Show()
        {
            Debug.Log("Clicked Show Rewarded Button");
            Ads.RewardedAd.Show(_ShowRewardCallbacks);
        }
    }
}
