﻿/**
 * Important:  AppLovinCrossPromo.Init() must be called before interacting 
 * with the CrossPromo class
 */
using System;
using System.Collections;
using System.Collections.Generic;
using LionStudios;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(Outline))]
public class CrossPromo : MonoBehaviour
{
    static CrossPromo _Singleton;
    static CrossPromo Singleton
    {
        get
        {
            if (_Singleton == null)
            {
                //attempt to find the singelton in the scene
                _Singleton = FindObjectOfType(typeof(CrossPromo)) as CrossPromo;
            }
            return _Singleton;
        }
    }

    #region Callbacks
    //cross promo event callbacks
    public static event Action OnCrossPromoShownEvent;
    public static event Action OnCrossPromoHiddenEvent;
    #endregion
    

    #region Static Methods
    /// <summary>
    /// Public acces to determine if crosspromo is active
    /// </summary>
    /// <returns></returns>
    public static bool Active {
        get
        {
            if (Singleton == null)
            {
                return false;    
            }
            return Singleton._Active;
        }
    }

    /// <summary>
    /// Public access to show the cross promo
    /// </summary>
    /// <param name="crossPromoImage"></param>
    public static void Show()
    {
        if (Singleton)
            Singleton.ShowCrossPromo();
    }

    /// <summary>
    /// Public access to show the cross promo.  This is an override and ignores the game object state and UI integration
    /// </summary>
    /// <param name="crossPromoImage"></param>
    public static void Show(float xPercentage, float yPercentage, float widthPercentage, float heightPercentage, float rotation)
    {
        if (Singleton)
            Singleton.ShowCrossPromo(xPercentage, yPercentage, widthPercentage, heightPercentage, rotation);
    }

    /// <summary>
    /// Public access to hide the cross promo
    /// </summary>
    /// <param name="crossPromoImage"></param>
    public static void Hide()
    {
        if (Singleton)
            Singleton.HideCrossPromo();
    }
    #endregion

    private void Awake()
    {
        _Singleton = this;
        GDPR.OnOpen += () =>
        {
            _CrossPromoWasOpen = _Active;
            Hide();
        };

        GDPR.OnClosed += () =>
        {
            if (_CrossPromoWasOpen)
                Show();
        };
    }

    private bool _CrossPromoWasOpen;

    private void Start()
    {
#if UNITY_EDITOR
        ModifyPlaceholderImage();
#else
        GetComponent<Image>().enabled = false;
        GetComponent<Outline>().enabled = false;
#endif
        Debug.Log("Start - " + this + " :: Active = " + _Active);
        HideCrossPromo();
    }

    private void ModifyPlaceholderImage()
    {
        //modify image
        Image img = GetComponent<Image>();
        img.color = Color.cyan;
        //modify outline
        Outline outline = GetComponent<Outline>();
        outline.effectDistance = Vector3.one * 3f;
        //create and modify text
        GameObject targetTextGO = new GameObject("crossPromoText");
        targetTextGO.transform.SetParent(transform);
        targetTextGO.transform.localPosition = Vector3.zero;
        targetTextGO.transform.localRotation = Quaternion.Euler(Vector3.zero);
        Text targetText = targetTextGO.AddComponent<Text>();
        targetText.text = "Cross Promo Target";
        targetText.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
        targetText.resizeTextForBestFit = true;
        targetText.color = Color.black;
    }

    void OnDisable()
    {
        HideCrossPromo();
        Debug.Log("OnDisable - " + this);
    }

    void OnEnable()
    {
        // If another instance exists hide it and make this the new singleton
        if (_Singleton != null && _Singleton != this)
        {
            _Singleton.HideCrossPromo();
            _Singleton = this;
        }

        ShowCrossPromo();
        Debug.Log("OnEnable - " + this);
    }

    /// <summary>
    /// Show Cross Promo ad.
    /// </summary>
    /// <param name="crossPromoImage">RectTransform that is a placeholder for where the Cross Promo should be displayed.</param>
    public void ShowCrossPromo()
    {
        var rDegrees = transform.localRotation.eulerAngles.z;

        RectTransform rectTransform = GetComponent<RectTransform>();
        rectTransform.localRotation = Quaternion.Euler(rectTransform.localRotation.eulerAngles.x, rectTransform.localRotation.eulerAngles.y, 0);
        rectTransform.ForceUpdateRectTransforms();

        var worldCorners = new Vector3[4];
        rectTransform.GetWorldCorners(worldCorners);

        Camera rectCam;

        if (rectTransform.GetComponentInParent<Canvas>().renderMode == RenderMode.ScreenSpaceOverlay)
        {
            rectCam = null;
        }
        else
        {
            rectCam = rectTransform.GetComponentInParent<Canvas>().worldCamera;
        }

        var rectScreenTopLeft = RectTransformUtility.WorldToScreenPoint(rectCam, worldCorners[1]);
        var rectScreenBottomRight = RectTransformUtility.WorldToScreenPoint(rectCam, worldCorners[3]);

        var canvasSize = Mathf.Min(Screen.width, Screen.height);

        var rectViewportTopLeftX = rectScreenTopLeft.x / Screen.width * 100f;
        var rectViewportTopLeftY = 100f - rectScreenTopLeft.y / Screen.height * 100f;

        var widthP = (rectScreenBottomRight.x - rectScreenTopLeft.x) / canvasSize * 100f;
        var heightP = (rectScreenTopLeft.y - rectScreenBottomRight.y) / canvasSize * 100f;

        rectTransform.localRotation = Quaternion.Euler(rectTransform.localRotation.eulerAngles.x, rectTransform.localRotation.eulerAngles.y, rDegrees);

        ShowCrossPromo(rectViewportTopLeftX, rectViewportTopLeftY, Mathf.Abs(widthP), Mathf.Abs(heightP), -rDegrees);
    }

    /// <summary>
    /// Show Cross Promo ad.
    /// </summary>
    /// <param name="xPercentage">Top-left X position expressed as a percentage of the screen width (i.e. 20.0).</param>
    /// <param name="yPercentage">Top-left Y position expressed as a percentage of the screen height (i.e. 30.0).</param>
    /// <param name="widthPercentage">Width expressed as a percentage of the minimum value between screen width and height (i.e. 25.0).</param>
    /// <param name="heightPercentage">Height expressed as a percentage of the minimum value between screen width and height (i.e. 25.0).</param>
    /// <param name="rotation">Clock-wise rotation expressed in degrees (i.e., 45).</param>
    public void ShowCrossPromo(float xPercentage, float yPercentage, float widthPercentage, float heightPercentage, float rotation)
    {
        if (_Active || GDPR.Active)
            return;

        gameObject.SetActive(true);
        enabled = true;

        if (Singleton.gameObject.activeInHierarchy)
        {
            AppLovinCrossPromo.Init();
            AppLovinCrossPromo.Instance().ShowMRec(Mathf.Clamp(xPercentage, 0f, 100f), Mathf.Clamp(yPercentage, 0f, 100f), Mathf.Clamp(widthPercentage, 0f, 100f), Mathf.Clamp(heightPercentage, 0f, 100f), rotation);
            _Active = true;
            OnCrossPromoShownEvent?.Invoke();
        }
        else
            Debug.LogWarning("Attempting to show CrossPromo but it is not active in the hierarchy.");
    }

    /// <summary>
    /// Hide currently visible Cross Promo ad.
    /// </summary>
    public void HideCrossPromo()
    {
        if (!_Active)
            return;

        gameObject.SetActive(false);
        AppLovinCrossPromo.Init();
        AppLovinCrossPromo.Instance().HideMRec();
        _Active = false;
        OnCrossPromoHiddenEvent?.Invoke();
    }

    bool _Active = false;
}
