﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LionStudios
{
    [RequireComponent(typeof(Button))]
    public abstract class AdButton : MonoBehaviour// Button
    {
        public string placement;
        public int level = -1;
        public bool sendAnalyticsEvents = true;

        // Start is called before the first frame update
        protected virtual void Start()
        {
            CreateAdRequests();
            _Button = GetComponent<Button>();
            _Button.onClick.AddListener(Show);
        }

        protected abstract void Show();

        protected abstract void Load();

        // Update is called once per frame
        void CreateAdRequests()
        {
            // Rewarded
            // Create callbacks for load and show
            _LoadRewardCallbacks = new Ads.LoadCallbackRequest
            {
                OnLoaded = OnLoaded,
                OnLoadFailed = OnLoadedFailed
            };

            _ShowRewardCallbacks = new Ads.ShowAdRequest
            {
                OnDisplayed = OnDisplayed,
                OnClicked = OnClicked,
                OnReceivedReward = OnReceivedReward,
                OnHidden = OnClose,
                OnFailedToDisplay = OnFailedToDisplay
            };

            _ShowRewardCallbacks.SetPlacement(placement);
            _ShowRewardCallbacks.SetLevel(level);
        }

        public virtual void OnLoaded(string adUnitId)
        {
            Debug.Log("Loaded Reward Video :: Ad Unit ID = " + adUnitId);
        }

        public virtual void OnLoadedFailed(string adUnitId, int error)
        {
            Debug.LogError("Failed To Load Reward Video :: Error = " + error + " :: Ad Unit ID = " + adUnitId);
        }

        public virtual void OnDisplayed(string adUnitId)
        {
            Debug.Log("Displayed Reward Video :: Ad Unit ID = " + adUnitId);
        }

        public virtual void OnClicked(string adUnitId)
        {
            Debug.Log("Clicked Reward Video :: Ad Unit ID = " + adUnitId);
        }

        public virtual void OnClose(string adUnitId)
        {
            Debug.Log("Closed Reward Video :: Ad Unit ID = " + adUnitId);
        }

        public virtual void OnFailedToDisplay(string adUnitId, int error)
        {
            Debug.LogError("Failed To Display Reward Video :: Error = " + error + " :: Ad Unit ID = " + adUnitId);
        }

        public virtual void OnReceivedReward(string adUnitId, MaxSdkBase.Reward reward)
        {
            Debug.Log("Received Reward Event :: Reward = " + reward + " :: Ad Unit ID = " + adUnitId);
        }

        protected Ads.LoadCallbackRequest _LoadRewardCallbacks;
        protected Ads.ShowAdRequest _ShowRewardCallbacks;
        protected Button _Button;
    }
}
