﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LionStudios
{
	public struct LionGameEvent
	{
		public string eventName;
		public Dictionary<string, object> eventParams;

		public LionGameEvent(string _eventName)
		{
			eventName = _eventName;
			eventParams = new Dictionary<string, object>();
		}

		public LionGameEvent(string _eventName, Dictionary<string, object> _eventParams)
		{
			eventName = _eventName;
			eventParams = _eventParams;
		}
	}
}