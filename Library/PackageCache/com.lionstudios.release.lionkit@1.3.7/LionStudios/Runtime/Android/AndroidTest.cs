﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidTest : MonoBehaviour
{
#if UNITY_ANDROID

    AndroidJavaObject TestClass()
    {
        if (_AndroidJavaObject == null)
            _AndroidJavaObject = new AndroidJavaObject("com.lionstudios.release.lionkit.TestClass");

        return _AndroidJavaObject;
    }

    public void CallTestMethod()
    {
        Debug.Log("Calling Java code");
        TestClass().Call("testMethod", "hoooochy yodaaa");
        //AndroidJavaClass TestClass = new AndroidJavaClass("com.lionstudios.release.lionkittestapp.TestClass");
        //TestClass.Call("testMethod", new object[] { "hoooochy yodaaa" });
        Debug.Log("Finished Calling Java code");
    }

    private static AndroidJavaObject GetCurrentActivity()
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        return unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
    }

    AndroidJavaObject _AndroidJavaObject;
#endif
}
