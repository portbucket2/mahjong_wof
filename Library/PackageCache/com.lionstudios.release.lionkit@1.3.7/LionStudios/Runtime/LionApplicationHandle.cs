using UnityEngine;
using LionStudios.Debugging;
using System;
using System.Reflection;
    
namespace LionStudios
{
    [RequireComponent(typeof(DebugSwipe))]
    public class LionApplicationHandle : MonoBehaviour
    {
        public static LionApplicationHandle HNDL
        {
            get
            {
                if (LionKit.IsInitialized)
                {
                    return LionKit.HNDL;
                }
                return null;
            }
        }
        
        public void OnApplicationPause(bool pause)
        {
            if (!pause)
            {
                Analytics.Events.GameStarted();
            }
        }
        
        private void Update()
        {
            if (Input.GetKeyDown("`"))
            {
                if (!LionDebugger.IsShowing())
                {
                    LionDebugger.Show();
                }
                else
                {
                    LionDebugger.Hide();
                }
            }
        }
    }
}