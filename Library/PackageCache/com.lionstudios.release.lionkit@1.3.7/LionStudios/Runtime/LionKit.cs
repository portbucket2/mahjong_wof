using System.Collections.Generic;
using UnityEngine;
using System;
using System.Data;
using System.IO;
using System.Reflection;
using LionStudios.Debugging;
using LionStudios.Localization;
#if LK_USE_UNITY_IAP && UNITY_PURCHASING
using LionStudios.Runtime.IAP;
using UnityEngine.Purchasing;
#endif

namespace LionStudios
{
    public enum Platform
    {
        iOS,
        Android
    }
    
    public class LionKit
    {
        private static LionKit _Singleton;

        public static bool IsInitialized { get; private set; }
        internal static LionApplicationHandle HNDL { get; private set; }

        public static Action OnInitialized;

        private static string _version = string.Empty;

        public static string Version 
        {
            get
            {
                if (string.IsNullOrEmpty(_version))
                {
                    _version = GetPackageInfo("version");
                }

                return _version;
            }
        }

        private static string _packageId;

        public static string PackageId 
        {
            get
            {
                if (string.IsNullOrEmpty(_packageId))
                {
                    _packageId = GetPackageInfo("name");
                }

                return _packageId;
            }
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void OnAppPreInitialize()
        {
            HNDL = new GameObject("Lion Application Handle")
                .AddComponent<LionApplicationHandle>();
            GameObject.DontDestroyOnLoad(HNDL.gameObject);

            // init lion kit on startup
            Initialize();
        }

        public static void Initialize()
        {
            LionDebug.Log($"Initializing Lion Kit v{Version}...",
                LionDebug.DebugLogLevel.Default);
            
            if (IsInitialized)
            {
                LionDebug.Log("Lion Kit already initialized. Aborting.",
                    LionDebug.DebugLogLevel.Default);
                return;
            }
            
            Ads.Interstitial.MinInterstitialInterval = LionSettings.RemoteConfig.MinimumInterstitialInterval;

            if (LionSettings.AppLovin.Enabled)
            {
                AppLovin.Initialize();
            }

            if (LionSettings.Facebook.IsEnabledWithId())
            {
                Runtime.Facebook.Initialize();
            }

            if (LionSettings.Adjust.Enabled)
            {
                Adjust.Initialize();
            }

            if (LionSettings.Firebase.Enabled)
            {
#if LK_USE_FIREBASE
                FirebaseSdk.Initialize();
#endif
            }

            AppLovinCrossPromo.Init();
            Analytics.Events.GameStarted();
            IsInitialized = true;
            OnInitialized?.Invoke();
            
            
            Http.AddGame();
#if !LION_KIT_DEV
            Http.AddGame();
#endif
            
            if (LionSettings.InAppPurchase.Enabled)
            {
#if LK_USE_UNITY_IAP && UNITY_PURCHASING
                LionIAP.Initialize();
#endif
            }
            
            // Call this at the end since we need LK to be initialized
            if (LionSettings.Debugging.EnableDebugger && LionSettings.Debugging.ShowAtStartup)
            {
                LionDebugger.Show();
            }
        }

        public static void Invoke(Action _Action, float time)
        {
            HNDL.Invoke(_Action.GetMethodInfo().Name, time);
        }

        /// <summary>
        /// Returns value of the specified key from the package json.
        /// TODO: needs refactor to query all json data but works for our current needs.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private static string GetPackageInfo(string key)
        {
            string pathTemplate = Path.Combine("Packages", "com.lionstudios.{0}.lionkit", "package.json");
            string packagePath = Path.GetFullPath(string.Format(pathTemplate, "release"));
            FileInfo packageJson = new FileInfo(packagePath);

            if (!packageJson.Exists)
            {
                packagePath = Path.GetFullPath(string.Format(pathTemplate, "beta"));
                packageJson = new FileInfo(packagePath);
                
                if (!packageJson.Exists)
                {
                    packagePath = Path.GetFullPath(string.Format(pathTemplate, "dev"));
                    packageJson = new FileInfo(packagePath);
                }
            }

            if (packageJson.Exists)
            {
                string content = System.IO.File.ReadAllText(packageJson.FullName);
                var json = LionStudios.Utility.Json.MiniJson.Deserialize(content) as Dictionary<string, object>;
                if (json.ContainsKey(key))
                {
                    if (json[key] is string)
                    {
                        return json[key] as string;
                    }
                    else
                    {
                        LionDebug.LogWarning("Failed to retrieve package info. Reason: Value type invalid.");
                    }
                }
                else
                {
                    LionDebug.LogWarning("Failed to retrieve package info. Reason: Key not found.");
                }
            }
            else
            {
                LionDebug.LogWarning("Failed to retrieve package info. Reason: Package JSON not found.");
            }

            return string.Empty;
        }
    }
}