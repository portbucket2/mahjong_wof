#pragma warning disable 0414
using System;
using System.IO;
using System.Collections.Generic;
using LionStudios.Debugging;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

#if LK_USE_UNITY_IAP && UNITY_PURCHASING
using Google;
using com.adjust.sdk;
using com.adjust.sdk.purchase;
using UnityEngine.Purchasing;
#endif

namespace LionStudios.Runtime.IAP
{
    public sealed class LionIAP :
        MonoBehaviour
#if LK_USE_UNITY_IAP && UNITY_PURCHASING
        ,IStoreListener
#endif
    {
        private static LionIAP _instance;
        private const string connectSettingsPath = "ProjectSettings/UnityConnectSettings.asset";
        private const string purchasingPath = "Plugins/UnityPurchasing/";
        private const string purchasingSettings = "UnityPurchasingSettings";
        private const string analyticsSettings = "UnityAnalyticsSettings";
        public static bool IAP_Service_Enabled()
        {
            string pathToConnectSettings = Path.Combine(Application.dataPath, "../", connectSettingsPath);

            if (File.Exists(connectSettingsPath))
            {
                string content = File.ReadAllText(connectSettingsPath);
                
                int purchaseSettingsIdx = content.IndexOf(purchasingSettings);
                content = content.Substring(purchaseSettingsIdx);

                int analyticsSectionIdx = content.IndexOf(analyticsSettings);
                content = content.Substring(0, analyticsSectionIdx);
                
                if (content.Contains("m_Enabled: 1"))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IAP_Purchasing_Installed()
        {
            const int minPurchasingDirCount = 5;
            const int minPurchasingFileCount = 10;

            string pathToPurchasing = Path.Combine(Application.dataPath, purchasingPath);
            DirectoryInfo purchasingDir = new DirectoryInfo(pathToPurchasing);
            DirectoryInfo[] subDirectories = purchasingDir.GetDirectories();
            
            if (purchasingDir.Exists && subDirectories.Length > minPurchasingDirCount)
            {
                List<FileInfo> subFiles = new List<FileInfo>();
                foreach (DirectoryInfo dir in subDirectories)
                {
                    subFiles.AddRange(dir.GetFiles());
                }

                return subFiles.Count > minPurchasingFileCount;
            }

            return false;
        }
        
#if LK_USE_UNITY_IAP && UNITY_PURCHASING
        internal static readonly ProductConfig Product_No_Ads_Ios = new ProductConfig(
            "prod_no_adverts_ios",
            ProductType.Subscription,
            EStoreAvailability.AppleAppStore);

        internal static readonly ProductConfig Product_No_Ads_Android = new ProductConfig(
            "prod_no_adverts_ios",
            ProductType.Subscription,
            EStoreAvailability.GooglePlayStore);
        
        private static IStoreController _storeController;
        private static IExtensionProvider _storeExtensionProvider;

        public delegate PurchaseProcessingResult ProcessPurchaseDelegate(PurchaseEventArgs args); 
        public delegate PurchaseProcessingResult PurchaseFailedDelegate(Product product, PurchaseFailureReason reason);
        
        /// <summary>
        /// Fires when Unity Purchase has successfully processed the purchase.
        /// State can be pending or completed. Null-check returned product for status.
        /// </summary>
        public static event ProcessPurchaseDelegate ProcessPurchaseSuccess = null;
        
        /// <summary>
        /// Fires when TryPurchase has returned a failure. 
        /// </summary>
        public static event PurchaseFailedDelegate ProcessPurchaseFailed = null;
        
        /// <summary>
        /// Fires when Unity Purchase has successfully processed the purchase and purchase
        /// has been successfuly validated by AdjustPurchase.
        /// </summary>
        public static event ProcessPurchaseDelegate PurchaseSuccessAndValidated = null;

        internal static void Initialize(ConfigurationBuilder builder = null)
        {
            LionIAP iap = LionApplicationHandle.HNDL.GetComponent<LionIAP>();
            if (iap == null)
            {
                _instance = iap = LionApplicationHandle.HNDL.gameObject.AddComponent<LionIAP>();
                if (builder == null)
                {
                    builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
                    
                    /// add pre-configured products
                    foreach (ProductConfig p in GetAllProducts())
                    {
                        switch (p.productAvailability)
                        {
                            case EStoreAvailability.Both:
                                builder.AddProduct(p.productId, p.productType, new IDs()
                                {
                                    {p.productId, AppleAppStore.Name},
                                    {p.productId, GooglePlay.Name}
                                });
                                break;
                            case EStoreAvailability.AppleAppStore:
                                builder.AddProduct(p.productId, p.productType, new IDs()
                                {
                                    {p.productId, AppleAppStore.Name}
                                });
                                break;
                            case EStoreAvailability.GooglePlayStore:
                                builder.AddProduct(p.productId, p.productType, new IDs()
                                {
                                    {p.productId, GooglePlay.Name}
                                });
                                break;
                        }
                    }
                }
                
                LionDebug.Log($"Initializing Lion Kit In-App Purchases");
                UnityPurchasing.Initialize(iap, builder);
            }
        }
        
        public static bool IsInitialized()
        {
            return _storeController != null && _storeExtensionProvider != null;
        }

        public static void PurchaseProduct(string productId)
        {
            if (_instance == null || !IsInitialized())
            {
                LionDebug.LogWarning(
                    "Failed to purchase product. LionIAP not ready, call LionIAP.Initialize() before purchasing products.");
                return;
            }
            
            _instance.TryPurchaseProduct(productId);
        }

        private void TryPurchaseProduct(string productId)
        {
            if (IsInitialized())
            {
                Product product = _storeController.products.WithID(productId);
                if (product != null && product.availableToPurchase)
                {
                    LionDebug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id),
                        LionDebug.DebugLogLevel.Event);
                    
                    _storeController.InitiatePurchase(product);
                }
                else
                {
                    LionDebug.Log(
                        "BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            else
            {
                LionDebug.Log($"Failed to purchase product. Product Id '{productId}' not found.");
            }
        }

        public static void RestorePurchases()
        {
            if (_instance == null || !IsInitialized())
            {
                LionDebug.LogWarning(
                    "Failed to restore purchases. LionIAP not ready, call LionIAP.Initialize() before restoring purchases.");
                return;
            }

            _instance.TryRestorePurchases();
        }
        
        private void TryRestorePurchases()
        {
            if (!IsInitialized())
            {
                LionDebug.Log("Failed to restore purchases. IAP not initialized.");
                return;
            }

            if (!LionSettings.InAppPurchase.AllowRestorePurchases)
            {
                LionDebug.Log("Failed to restore purchases. Purchase restore is not enabled. Check IAP settings...");
                return;
            }
            
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                var apple = _storeExtensionProvider.GetExtension<IAppleExtensions>();
                apple.RestoreTransactions((result) =>
                {
                    LionDebug.Log("RestorePurchases continuing: " + result +
                                  ". If no further messages, no purchases available to restore.");
                });
            }
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                LionDebug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            _storeController = controller;
            _storeExtensionProvider = extensions;
            LionDebugger.AddButton("Try Purchase Product", () =>
            {
                this.TryPurchaseProduct("000000");
            }, "IAP");

            LionDebugger.AddButton("Try Restore Purchases", this.TryRestorePurchases, "IAP");
            LionDebug.Log("LionIAP Initialized!");
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            LionDebug.Log($"Failed to init LionIAP. (Reason:{error})");
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            LionDebug.Log($"Processing Purchase: {args.purchasedProduct}", LionDebug.DebugLogLevel.Verbose);
            
            PurchaseProcessingResult result = PurchaseProcessingResult.Pending;
            if (LionSettings.InAppPurchase.ValidatePurchases)
            {
                ValidateAndTrackPurchase(args, (validationState) =>
                {
                    if (validationState == ADJPVerificationState.ADJPVerificationStatePassed)
                    {
                        result = PurchaseProcessingResult.Complete;
                        PurchaseSuccessAndValidated?.Invoke(args);
                        LionDebug.Log($"Adjust Purchase Verification PASSED for product '{args.purchasedProduct}'", LionDebug.DebugLogLevel.Verbose);
                    }else if (validationState == ADJPVerificationState.ADJPVerificationStateFailed)
                    {
                        LionDebug.Log($"Adjust Purchase Verification FAILED for product '{args.purchasedProduct}'", LionDebug.DebugLogLevel.Warn);
                    }
                    else
                    {
                        LionDebug.Log($"Adjust Purchase Verification '{validationState.ToString()}' for product '{args.purchasedProduct}'", LionDebug.DebugLogLevel.Warn);
                    }
                });
            }
            else
            {
                result = (args.purchasedProduct != null && !string.IsNullOrEmpty(args.purchasedProduct.transactionID))
                    ? PurchaseProcessingResult.Complete
                    : PurchaseProcessingResult.Pending;
            }

            ProcessPurchaseSuccess?.Invoke(args);
            return result;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
        {
            ProcessPurchaseFailed?.Invoke(product, reason);
            LionDebug.Log(string.Format("OnPurchaseFailed. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, reason));
        }

        public static ProductConfig[] GetAllProducts()
        {
            return LionSettings.InAppPurchase.GetAllProducts();
        }

        #region AdjustValidation
        public void ValidateAndTrackPurchase(PurchaseEventArgs purchaseEventArgs,
            Action<ADJPVerificationState> resultCallback)
        {
            var lPrice = purchaseEventArgs.purchasedProduct.metadata.localizedPrice;
            var price = decimal.ToDouble(lPrice);
            var currencyCode = purchaseEventArgs.purchasedProduct.metadata.isoCurrencyCode;
            var transactionID = purchaseEventArgs.purchasedProduct.transactionID;
            var productID = purchaseEventArgs.purchasedProduct.definition.id;
            var receiptDict = (Dictionary<string, object>)MiniJson.JsonDecode(purchaseEventArgs.purchasedProduct.receipt);
            var payload = (receiptDict != null && receiptDict.ContainsKey("Payload")) ? (string)receiptDict["Payload"] : "";
            
            System.Action<ADJPVerificationInfo> verificationCb = (verificationInfo) =>
            {
                AdjustEvent adjustEvent = null;
                switch (verificationInfo.VerificationState)
                {
                    case ADJPVerificationState.ADJPVerificationStatePassed:
                        adjustEvent = new AdjustEvent("iap_purchase");
                        adjustEvent.setRevenue(price, currencyCode);
                        adjustEvent.setTransactionId(transactionID); // in-SDK deduplication
                        adjustEvent.addCallbackParameter("productID", productID);
                        adjustEvent.addCallbackParameter("transactionID", transactionID);
                        com.adjust.sdk.Adjust.trackEvent(adjustEvent);
                        break;
                    case ADJPVerificationState.ADJPVerificationStateFailed:
                        adjustEvent = new AdjustEvent("purchase_failed");
                        adjustEvent.addCallbackParameter("productID", productID);
                        adjustEvent.addCallbackParameter("transactionID", transactionID);
                        com.adjust.sdk.Adjust.trackEvent(adjustEvent);
                        break;
                    case ADJPVerificationState.ADJPVerificationStateUnknown:
                        adjustEvent = new AdjustEvent("purchase_unknown");
                        adjustEvent.addCallbackParameter("productID", productID);
                        adjustEvent.addCallbackParameter("transactionID", transactionID);
                        com.adjust.sdk.Adjust.trackEvent(adjustEvent);
                        break;
                    default:
                        adjustEvent = new AdjustEvent("purchase_notverified");
                        adjustEvent.addCallbackParameter("productID", productID);
                        adjustEvent.addCallbackParameter("transactionID", transactionID);
                        com.adjust.sdk.Adjust.trackEvent(adjustEvent);
                        break;
                }
                
                if (resultCallback != null)
                {
                    resultCallback(verificationInfo.VerificationState.Value);
                }
            };
            
#if UNITY_IOS
            AdjustPurchase.VerifyPurchaseiOS(payload, transactionID, productID, verificationCb);
#elif UNITY_ANDROID
            var jsonDetailsDict = (!string.IsNullOrEmpty(payload)) ? (Dictionary<string, object>)MiniJson.JsonDecode(payload) : null;
            var json = (jsonDetailsDict != null && jsonDetailsDict.ContainsKey("json")) ? (string)jsonDetailsDict["json"] : "";
            var gpDetailsDict = (!string.IsNullOrEmpty(json)) ? (Dictionary<string, object>)MiniJson.JsonDecode(json) : null;
            var purchaseToken = (gpDetailsDict != null && gpDetailsDict.ContainsKey("purchaseToken")) ?
            (string)gpDetailsDict["purchaseToken"] : "";
            AdjustPurchase.VerifyPurchaseAndroid(productID, purchaseToken, "", verificationCb);
#endif
        }
        #endregion
#endif
    }
}