using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

#if UNITY_EDITOR
using UnityEditor;
using System.IO;
#endif

#if LK_USE_UNITY_IAP && UNITY_PURCHASING
using UnityEngine.Purchasing;
#endif

namespace LionStudios.Runtime.IAP
{
#if LK_USE_UNITY_IAP && UNITY_PURCHASING
    public enum EStoreAvailability
    {
        Both = 0,
        AppleAppStore,
        GooglePlayStore,
    }

    [Serializable]
    public class ProductConfig
    {
        [SerializeField] public bool productEnabled = true;
        [SerializeField] public string productId = "new product";
        [SerializeField] public ProductType productType = ProductType.Consumable;
        [SerializeField] public EStoreAvailability productAvailability = EStoreAvailability.Both;

        public ProductConfig(){}
        public ProductConfig(string productId,
            ProductType productType = ProductType.Consumable,
            EStoreAvailability productAvailability = EStoreAvailability.Both)
        {
            this.productId = productId;
            this.productType = productType;
            this.productAvailability = productAvailability;
        }
    }
#endif
}