﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelClip : MonoBehaviour
{
    [SerializeField] int multiplayer = 0;
    [SerializeField] WheelOfFortune fortuna;

    EdgeCollider2D col;
    Animator animator;
    readonly string WHEEL = "wheel";
    readonly string MOVE = "move";
    bool rotationStopped = false;
    bool rotationStarted = false;

    private void Start()
    {
        col = GetComponent<EdgeCollider2D>();
        //col.enabled = false;
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(WHEEL))
        {
            if (rotationStarted)
            {
                animator.SetTrigger(MOVE);
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag(WHEEL))
        {
            if (rotationStopped)
            {
                multiplayer = collision.GetComponent<WheelPart>().multi;
                //Debug.Log("Hit coll: " + multiplayer);
                col.enabled = false;
                fortuna.SetMultiplayer(multiplayer);
                rotationStopped = false;
            }            
        }
    }
    public void StartRotation()
    {
        rotationStarted = true;
    }
    public void GetWheelValue()
    {
        rotationStopped = true;
        col.enabled = true;
    }
    public void Reset()
    {
        rotationStopped = false;
        col.enabled = true;
    }
}
