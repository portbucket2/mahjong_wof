﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ButtonSpawner : MonoBehaviour
{

    [SerializeField] bool isPreDefined = false;
    [SerializeField] Transform parentPanel;
    [SerializeField] GameObject buttonPrefab;


    [Header("Debug Only No Input")]
    [SerializeField] GridLayoutGroup layoutGroup;
    [SerializeField] string inputWord;
    [SerializeField] int number;
    [SerializeField] List<char> characters;
    [SerializeField] string outputWord;

    [SerializeField] List<GameObject> buttons;
    [SerializeField] List<char> layerWord;

    GameController gameController;
    GameLogic gameLogic;
    private void Awake()
    {
        buttons = new List<GameObject>();
        if (isPreDefined)
        {
            int count = transform.childCount;
            for (int i = 0; i < count; i++)
            {
                buttons.Add(transform.GetChild(i).gameObject);
            }
        }
    }
    void Start()
    {
        gameController = GameController.GetController();
        gameLogic = gameController.GetGameLogic();
        
        layoutGroup = GetComponent<GridLayoutGroup>();
    }
    public void SetSpawner(string _inputWord, string connectingLetter, string connectingLetter2 = "-1")
    {
        inputWord = _inputWord;

        number = inputWord.Length;

        characters = new List<char>();
        for (int i = 0; i < number; i++)
        {
            characters.Add(inputWord[i]);
        }
        for (int i = 0; i < number; i++)
        {
            characters.Add(inputWord[i]);
        }

        //Debug.Log("word length: " + inputWord.Length);
        //Debug.Log("Number of words: " + CountWords(inputWord));
        int ceilNumber = number * 2;
        float tempFloat = (float)ceilNumber / 3f;
        //Debug.Log("tempFloat: " + tempFloat);
        ceilNumber = Mathf.CeilToInt(tempFloat);
        //Debug.Log("ceilNumber: " + ceilNumber);
        int cellNumber = ceilNumber * 3;

        for (int i = 0; i < cellNumber - number * 2; i++)
        {
            characters.Add(RandomLetter());
        }

        Shuffle(characters);

        outputWord = "";
        for (int i = 0; i < cellNumber; i++)
        {
            outputWord += characters[i];
        }

        for (int i = 0; i < cellNumber; i++)
        {
            GameObject newButton = Instantiate(buttonPrefab, parentPanel) as GameObject;
            newButton.transform.GetComponent<ButtonFunctions>().SetButtonValues(outputWord[i].ToString());
            newButton.SetActive(true);
            buttons.Add(newButton);
        }
        //connecting word
        if (connectingLetter != "0")
        {
            GameObject connectingButton = Instantiate(buttonPrefab, parentPanel) as GameObject;
            connectingButton.transform.GetComponent<ButtonFunctions>().SetButtonValues(connectingLetter);
            connectingButton.SetActive(true);
            buttons.Add(connectingButton);
        }
        if (connectingLetter2 != "-1")
        {
            GameObject connectingButton = Instantiate(buttonPrefab, parentPanel) as GameObject;
            connectingButton.transform.GetComponent<ButtonFunctions>().SetButtonValues(connectingLetter2);
            connectingButton.SetActive(true);
            buttons.Add(connectingButton);
        }

        Invoke("GridLayerOff", 0.5f);
    }
    public void SetSpawner(List<char> totalWord, int start, int end)
    {
        layerWord = new List<char>();
        int total = totalWord.Count;
        for (int i = start; i < end; i++)
        {
            layerWord.Add(totalWord[i]);
        }
        //inputWord = _inputWord;
        Shuffle(layerWord);
        number = layerWord.Count;
        if (number == buttons.Count)
        {
            for (int i = 0; i < number; i++)
            {
                buttons[i].GetComponent<ButtonFunctions>().SetButtonValues(layerWord[i].ToString());
            }
        }

    }
    
    private List<char> Shuffle(List<char> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
        return ts;
    }
    public void AllButtonsInteractable(bool isInteractable)
    {
        int max = buttons.Count;
        for (int i = 0; i < max; i++)
        {
            buttons[i].transform.GetComponent<ButtonFunctions>().Interactable(isInteractable);
        }
    }
    public bool CheckIsLayerCompletion()
    {
        bool isComplete = false;
        int max = buttons.Count;
        for (int i = 0; i < max; i++)
        {
            if (buttons[i].gameObject.activeInHierarchy)
            {
                isComplete = false;
                break;
            }
            else
            {
                isComplete = true;
            }
        }
        //Debug.Log("layer check....." + isComplete);
        return isComplete;
    }

    char RandomLetter()
    {
        //char c = (char)('A' + Random.Range(0, 26));
        char c = (char)('0' + Random.Range(0, 9));
        return c;
    }
    int CountWords(string word)
    {
        int count = 1;
        int max = word.Length;
        for (int i = 0; i < max; i++)
        {
            if (word[i] == ' ')
            {
                count++;
            }
        }
        return count;
    }
    void GridLayerOff()
    {
        layoutGroup.enabled = false;



        //float x = Screen.width;
        //float y = Screen.height;
        //float xs = (float)x * 0.25f;
        //float ys = (float)y * 0.25f;
        //Debug.Log("x/y: " + x + "/" + y);
        //Debug.Log("xs/ys: " + xs + "/" + ys);

        //int max = buttons.Count;
        //for (int i = 0; i < max; i++)
        //{
        //    float xRand = Random.Range(-450f, 450f);
        //    float yRand = (-1) * Random.Range(-500f, 500f);
        //    Debug.Log("xRand: " + xRand);
        //    Debug.Log("yRand: " + yRand);
        //    buttons[i].transform.DOLocalMove(new Vector3(xRand, yRand, 0f),1f);
        //}
    }
}
