﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class VKeyboard : MonoBehaviour
{
    public TextMeshProUGUI textOnField;
    public TextMeshProUGUI usernameField;
    [SerializeField] private string sentence;
    [SerializeField] private string username;
    int letterIndex = 0;
    int usrnameIndex = 0;

    private float m_TimeStamp;
    private bool cursor;
    private string cursorChar = "";
    

    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void Update()
    {
        usernameField.text = username;
        textOnField.text = sentence + cursorChar;
        if (letterIndex <= 0)
        {      
            letterIndex = 0;
        }

        if (Time.time - m_TimeStamp >= 0.5)
        {
            m_TimeStamp = Time.time;
            if (cursor == false)
            {
                cursor = true;
                cursorChar += "|";
            }
            else
            {
                cursor = false;
                if (cursorChar.Length != 0)
                {
                    cursorChar = cursorChar.Substring(0, cursorChar.Length - 1);
                }
            }
        }
            
    }

    public void InputText(string letter)
    {
        //if(GameManager.instance.currentState == GameState.LEVEL_INITIALIZING)
        //{
        //    usrnameIndex++;
        //    username += letter.ToLower();
        //}
        //else
        //{
        //    letterIndex++;
        //    sentence += letter.ToLower();
        //}

        letterIndex++;
        sentence += letter.ToUpper();

    }

    public void BackSpace()
    {
        //if(GameManager.instance.currentState == GameState.LEVEL_INITIALIZING)
        //{
        //    if (usrnameIndex >= 0)
        //    {
        //        usrnameIndex--;

        //    }
        //    if (username.Length > 0)
        //    {
        //        username = username.Remove(username.Length - 1);
        //    }
        //}
        //else
        //{
        //    if (letterIndex >= 0)
        //    {
        //        letterIndex--;

        //    }
        //    if (sentence.Length > 0)
        //    {
        //        sentence = sentence.Remove(sentence.Length - 1);
        //    }

        //}


        if (letterIndex >= 0)
        {
            letterIndex--;

        }
        if (sentence.Length > 0)
        {
            sentence = sentence.Remove(sentence.Length - 1);
        }


    }
    public void SaveUsername()
    {
        //UIManager.instance.SaveUser(username);
    }
    public void SendMessage()
    {
        if(sentence!= null)
        {
            //UIManager.instance.UserReply(sentence);
            //UIManager.instance.CheckReply(sentence);
            sentence = null;
        }
        
    }
    public string GetSentence()
    {
        string temp = "";
        if (sentence != null)
        {
            //UIManager.instance.UserReply(sentence);
            //UIManager.instance.CheckReply(sentence);
            temp =  sentence;
        }
        return temp;
    }

}
