﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class FakeButton : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI letterText;
    [SerializeField] Sprite normalSprite;
    [SerializeField] Sprite filledSprite;
    [SerializeField] Image cloverImage;
    [SerializeField] List<Sprite> clovers;
    [Header("debug auto fill")]
    [SerializeField] string letter;
    [SerializeField] Image image;
    [SerializeField] bool scaled = false;
    GameController gameController;
    Vector3 scaling = new Vector3(0.5f,0.5f,0.5f);
    void Awake()
    {
        image = GetComponent<Image>();        
    }
    private void Start()
    {
        gameController = GameController.GetController();
    }

    public void SetFakeButton(string _letter)
    {
        if(cloverImage)
            cloverImage.gameObject.SetActive(false);
        if (_letter == " ")
        {
            //image.sprite = filledSprite;
        }
        else
        {
            if (!scaled)
            {
                GetComponent<RectTransform>().DOScale(scaling, 0.01f).SetEase(Ease.Flash).OnComplete(
                    () => GetComponent<RectTransform>().DOScale(Vector3.one, 0.5f));
                scaled = true;
            }
            //image.sprite = normalSprite;
           
        }
        letter = _letter;
        if (int.TryParse(letter, out int result))
        {
            cloverImage.sprite = clovers[result];
            cloverImage.gameObject.SetActive(true);
            letterText.text = "";
        }
        else
        {
            letterText.text = letter;
        }

    }
    public string GetLetter()
    {
        return letter;
    }
    public void ShowEmpty(string chk)
    {
        if (chk == " ")
        {
            image.sprite = filledSprite;
        }
        else
        {
            image.sprite = normalSprite;
        }
    }
    public void Fly(Transform tt, string _letter)
    {
        letterText.text = _letter;
        GetComponent<RectTransform>().position = tt.position;
        image.enabled = true;
        letterText.enabled = true;
        //button.interactable = true;
        GetComponent<RectTransform>().DOScale(Vector3.one, 0.1f).SetEase(Ease.Flash);
        GetComponent<RectTransform>().DOMove(gameController.GetOutputPanel().position, 0.4f).SetEase(Ease.OutSine).OnComplete(
            () => GetComponent<RectTransform>().DOScale(scaling, 0.2f).SetEase(Ease.Flash).OnComplete(
                    () => ScaleEnd() ));

    }

    void ScaleEnd()
    {
        image.enabled = false;
        letterText.enabled = false;
    }
}
