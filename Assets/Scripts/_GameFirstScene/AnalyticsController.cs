﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LionStudios;

public class AnalyticsController : MonoBehaviour
{
    public static AnalyticsController analyticsController;

    int levelcount = 1;

    GameManager gameManager;
    private void Awake()
    {
        levelcount = 1;
        analyticsController = this;
        gameManager = transform.GetComponent<GameManager>();
    }
    private void Start()
    {
        levelcount = gameManager.GetDataManager().GetGamePlayer.levelsCompleted;
    }

    public static AnalyticsController GetController()
    {
        return analyticsController;
    }

    public void LevelStarted()
    {
        LionStudios.Analytics.Events.LevelStarted(levelcount);
    }
    public void LevelEnded()
    {
        LionStudios.Analytics.Events.LevelComplete(levelcount);
        levelcount++;
        SaveGame();
    }
    public void LevelFailed()
    {
        LionStudios.Analytics.Events.LevelFailed(levelcount);
    }
    public void SaveGame()
    {
        GamePlayer gp = new GamePlayer();
        gp.name = "";
        gp.id = 1;
        gp.levelsCompleted = levelcount;
        gp.totalCoins = GameController.GetController().GetTotalCoins();
        gp.lastPlayedLevel = gameManager.GetlevelCount();
        gp.handTutorialShown = true;

        gameManager.GetDataManager().SetGameplayerData(gp);
    }

}
