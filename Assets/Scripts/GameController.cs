﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameController : MonoBehaviour
{
    public static GameController gameController;
    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController() { return gameController; }


    [Header("others")]
    [SerializeField] int totalCoins = 0;
    [SerializeField] GameLogic gameLogic;
    [SerializeField] ButtonSpawner buttonSpawner;
    [SerializeField] UIController uiController;
    [SerializeField] ParticleSystem coinAddSmall;
    [SerializeField] ParticleSystem coinAddLarge;
    [SerializeField] ParticleSystem coinsFalling;
    [SerializeField] ParticleSystem happyParticle;
    [SerializeField] ParticleSystem sadParticle;
    [SerializeField] ParticleSystem spinAgainParticle;
    [SerializeField] ParticleSystem ripple;
    [SerializeField] Transform outputPanel;
    GameManager gameManager;
    AnalyticsController analytics;
    void Start()
    {
        analytics = AnalyticsController.GetController();
        gameManager = GameManager.GetManager();
        if (gameManager)
        {
            totalCoins = gameManager.GetDataManager().GetGamePlayer.totalCoins;
            uiController.UpdateCoins(totalCoins);
        }
    }

    public GameLogic GetGameLogic() { return gameLogic; }
    public ButtonSpawner GetButtonSpawner() { return buttonSpawner; }
    public UIController GetUI() { return uiController; }
    public void CoinSmall(Transform pos) { coinAddSmall.transform.position = new Vector3(pos.position.x,pos.position.y,80f); coinAddSmall.Play(); }
    public void CoinLarge() { coinAddLarge.Play(); }
    public void CoinsFalling() { coinsFalling.Play(); }
    public void HappyEmos() { happyParticle.Play(); }
    public void SadEmos() { sadParticle.Play(); }
    public void SpinAgain() { spinAgainParticle.Play(); }
    public void Ripple(Transform pos) { ripple.transform.position = new Vector3(pos.position.x, pos.position.y, 80f); ripple.Play();}
    public Transform GetOutputPanel(){ return outputPanel; }

    public void AddCoin(int number)
    {
        //int temp = totalCoins + number;
        totalCoins += number;
        uiController.UpdateCoins(totalCoins);
        if (analytics)
        {
            analytics.SaveGame();
        }
    }
    public void Bankrupt()
    {
        totalCoins = 0;
        uiController.UpdateCoins(0);
        if (analytics)
        {
            analytics.SaveGame();
        }
    }
    public int GetTotalCoins() { return totalCoins; }
}
