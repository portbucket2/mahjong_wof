﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ButtonFunctions : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI letterText;
    [SerializeField] ParticleSystem smoke;
    [SerializeField] Color normalColor;
    [SerializeField] Color selectedColor;
    [SerializeField] List<Sprite> clovers;
    [Header("Debug: ")]
    [SerializeField] string letter;
    [SerializeField] Button button;
    [SerializeField] Image image;
    [SerializeField] bool buttonPressed = false;

    GameController gameController;
    UIController UI;
    GameLogic gameLogic;
    Vector3 scaling = new Vector3(0.5f, 0.5f, 0.5f);
    private void Awake()
    {
        image = GetComponent<Image>();
    }
    void Start()
    {
        gameController = GameController.GetController();
        gameLogic = gameController.GetGameLogic();
        UI = gameController.GetUI();
        button = GetComponent<Button>();
        button.onClick.AddListener(delegate {
            Debug.Log("Button pressed: " + letter);
            UI.TutorialTextCheck();
            //GetComponent<RectTransform>().DOScale(scaling, 0.3f).SetEase(Ease.Flash).OnComplete(
            //    () => LetterCheck());

            LetterCheck();
            
            gameController.Ripple(transform);
            //smoke.Play();
        });   
    }
    void LetterCheck()
    {
        if (!buttonPressed)
        {
            //gameObject.SetActive(false);
            //button.interactable = false;
            buttonPressed = true;
            gameLogic.LetterCheck(this);
            ButtonSelected();
        }
        else
        {
            ButtonNormal();
        }
    }
    public void SetButtonValues(string _letter)
    {
        if (int.TryParse(_letter, out int result))
        {
            letter = _letter;
            letterText.text = " ";
            image.sprite = clovers[result];
            //image.sprite = clovers[Random.Range(0, clovers.Count)];
        }
        else
        {
            letter = _letter;
            letterText.text = letter;
        }
        
    }
    public string GetLetter()
    {
        return letter;
    }
    public void Interactable(bool interactable)
    {
        if (interactable)
        {
            //ButtonNormal();
            button.interactable = true;
            letterText.color = new Color(letterText.color.r, letterText.color.g, letterText.color.b, 255f); 
        }
        else
        {
            //ButtonSelected();
            button.interactable = false;
            letterText.color = new Color(letterText.color.r, letterText.color.g, letterText.color.b, 135f);
        }
    }
    
    public void ButtonNormal()
    {
        image.color = normalColor;
        buttonPressed = false;
        gameLogic.RemoveHolderButtoon(this);
    }
    void ButtonSelected()
    {
        image.color = selectedColor;
    }
    void Consume()
    {
        Debug.Log("Consumed the button");
        button.interactable = false;
        gameController.CoinSmall(transform);
        gameController.AddCoin(Random.Range(5, 50));
        GetComponent<RectTransform>().DOScale(scaling, 0.3f).SetEase(Ease.Flash).OnComplete(
            () => Consumed());
    }

    void Consumed()
    {
        gameObject.SetActive(false);
        gameLogic.LayerCompleteCheck();
    }
    

}
