﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UIController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI coinText;
    [SerializeField] TextMeshProUGUI coinTextLevelEnd;
    [SerializeField] GameObject levelCompletePanel;
    [SerializeField] GameObject levelFailedPanel;
    [SerializeField] Button nextLevelButton;
    [SerializeField] Button playAgainButton;
    [SerializeField] TextMeshProUGUI hintText;
    [SerializeField] TextMeshProUGUI categoryText;
    [SerializeField] TextMeshProUGUI multiplyerText;
    [SerializeField] ParticleSystem confettiParticle;
    [SerializeField] ParticleSystem confettiParticleBusrt;
    [SerializeField] WheelOfFortune wheelOfFortune;
    [SerializeField] GameObject introPanel;
    [SerializeField] Button introButton;
    [SerializeField] GameObject millionImage;
    [SerializeField] GameObject bankRuptImage;
    [SerializeField] GameObject tutorialText;
    [SerializeField] VKeyboard vKeyboard;
    [SerializeField] Button vKeyboardConfirm;
    [SerializeField] Button keyboardOn;
    [SerializeField] Image keyboardPanelBG;
    [SerializeField] GameObject popUpCorrectAnswer;
    [SerializeField] Animator popUpCorrectAnswerAnim;

    static bool introSeen = false;
    static bool tutorialTextSeen = false;

    readonly string PLAY = "play";

    GameManager gameManager;
    GameController gameController;
    GameLogic gameLogic;

    WaitForSeconds WAIT = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    AnalyticsController analytics;
    void Start()
    {
        gameManager = GameManager.GetManager();
        gameController = GameController.GetController();
        analytics = AnalyticsController.GetController();
        gameLogic = gameController.GetGameLogic();
        IntroPanel();

        SetHintText();

        nextLevelButton.onClick.AddListener(delegate {
            gameManager.GotoNextStage();
            analytics.LevelEnded();
        });
        playAgainButton.onClick.AddListener(delegate {
            gameManager.ResetStage();
        });
        introButton.onClick.AddListener(delegate {
            introPanel.SetActive(false);
            introSeen = true;
        });
        vKeyboardConfirm.onClick.AddListener(delegate {
            gameLogic.SolveSentence(vKeyboard.GetSentence());
            vKeyboard.gameObject.SetActive(false);
            keyboardPanelBG.enabled = false;
        });
        keyboardOn.onClick.AddListener(delegate {
            vKeyboard.gameObject.SetActive(true);
            keyboardOn.gameObject.SetActive(false);
            TutorialTextCheck();
            keyboardPanelBG.enabled = true;
        });
    }
    public void TutorialTextCheck()
    {
        if (!tutorialTextSeen)
        {
            tutorialText.SetActive(false);
            tutorialTextSeen = true;
        }
    }
    public void UpdateCoins(int coins) { coinText.text = coins.ToString();  }
    public void ActivateWheel() { StartCoroutine(WheelActivationRoutine()); }
    public void LevelComplete(string multiplyer, int luckVal) { StartCoroutine(LevelCompleteRoutine(multiplyer, luckVal)); }
    public void LevelCompletePopUp() { popUpCorrectAnswer.SetActive(true); popUpCorrectAnswerAnim.SetTrigger(PLAY); }
    public void LevelFailed() {
        levelFailedPanel.SetActive(true);
        vKeyboard.gameObject.SetActive(false);
        keyboardPanelBG.enabled = false;
    }
    public void SetHintText()
    {
        if (gameManager)
        {
            hintText.text = gameManager.GetLevelDescription();
            categoryText.text = gameManager.GetCategory();
        }
    }

    IEnumerator LevelCompleteRoutine(string _mult, int luck)
    {
        millionImage.SetActive(false);
        bankRuptImage.SetActive(false);

        if (luck > 0)
        {
            multiplyerText.text = "Bonus x " + _mult;
            int total = 100 * luck;
            gameController.AddCoin(total);
            coinTextLevelEnd.text = total.ToString();

            confettiParticle.Play();
            gameController.CoinLarge();
        }
        else
        {
            if (luck == -1)
            {
                multiplyerText.text = "";
                millionImage.SetActive(true);
                gameController.CoinsFalling();
                gameController.HappyEmos();
                int total = 1000000;
                gameController.AddCoin(total);
                coinTextLevelEnd.text = total.ToString();

                confettiParticle.Play();
                gameController.CoinLarge();
            }
            else if (luck == -2)
            {
                multiplyerText.text = "";
                bankRuptImage.SetActive(true);
                gameController.SadEmos();
                gameController.Bankrupt();
                coinTextLevelEnd.text = "0000";
            }
        }
        
        
        yield return WAIT;
        
        levelCompletePanel.SetActive(true);
    }
    IEnumerator WheelActivationRoutine()
    {
        keyboardOn.gameObject.SetActive(false);
        confettiParticleBusrt.Play();
        yield return WAITTWO;
        wheelOfFortune.gameObject.SetActive(true);
        wheelOfFortune.FadeIn();
    }
    void IntroPanel()
    {
        if (!introSeen)
        {
            introPanel.SetActive(true);            
        }
        else
        {
            introPanel.SetActive(false);
        }

        if (!tutorialTextSeen)
        {
            tutorialText.SetActive(true);
        }
        else
        {
            tutorialText.SetActive(false);
        }
    }
}
