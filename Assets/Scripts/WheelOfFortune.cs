﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class WheelOfFortune : MonoBehaviour
{
    [SerializeField] bool isRunning = false;
    [SerializeField] RectTransform wheel;
    [SerializeField] float timer =0f;
    [SerializeField] float max = 2f;
    [SerializeField] float speed = 10f;
    [SerializeField] float fadeSpeed = 1f;
    [SerializeField] Color pixelColor;
    [SerializeField] Camera mainCamera;
    [SerializeField] WheelClip mainClip;
    [SerializeField] Button startButton;
    GameController gameController;
    UIController uiController;

    CanvasGroup canvasGroup;

    int BANKRUPT = -1;
    int SPINAGAIN = -2;
    int MILLION = -3;
    WaitForEndOfFrame END = new WaitForEndOfFrame();

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
    void Start()
    {
        gameController = GameController.GetController();
        uiController = gameController.GetUI();
        
        startButton.onClick.AddListener(delegate
        {
            startButton.gameObject.SetActive(false);
            StartRotaion();
        });
    }

    private void Update()
    {
        if (isRunning ) 
        {
            if (timer > 0f)
            {
                wheel.Rotate(new Vector3(0f, 0f, speed * timer * Time.deltaTime));
                timer -= Time.deltaTime;
            }
            else
            {
                isRunning = false;
                Calculate();
            }            
        }       
    }
    void Calculate()
    {
        mainClip.GetWheelValue();        
    }
    public void SetMultiplayer(int val)
    {
        int multiplayer = 0;
        multiplayer = val;
        Debug.Log("Wheel value: " + multiplayer);
        string wheelResult = "";
        bool spinAgain = false;
        int luck = 0;
        if (multiplayer == 1) { wheelResult = "1"; luck = 1; }
        else if (multiplayer == 2) { wheelResult = "2"; luck = 2; }
        else if (multiplayer == 3) { wheelResult = "Spin Again"; spinAgain = true; }
        else if (multiplayer == 4) { wheelResult = "7"; luck = 7; }
        else if (multiplayer == 5) { wheelResult = "3"; luck = 4; }
        else if (multiplayer == 6) { wheelResult = "Million"; luck = -1; }
        else if (multiplayer == 7) { wheelResult = "3"; luck = 3; }
        else if (multiplayer == 8) { wheelResult = "5"; luck = 5; }
        else if (multiplayer == 9) { wheelResult = "Bankrupt"; luck = -2; }
        else if (multiplayer == 10) { wheelResult = "4"; luck = 4; }
        else if (multiplayer == 11) { wheelResult = "Spin Again"; spinAgain = true; }

        if (!spinAgain) { uiController.LevelComplete(wheelResult, luck); }
        else { ResetWheel();gameController.SpinAgain(); }
            
    }
    public void StartRotaion()
    {
        mainClip.StartRotation();
        timer = Random.Range(3f,7f);
        speed = Random.Range(150f, 220f);
        isRunning = true;
    }
    public void ResetWheel()
    {
        startButton.gameObject.SetActive(true);
        mainClip.Reset();
    }

    public void FadeIn() { StartCoroutine(FadeInRoutine()); }
    void FadeOut() { StartCoroutine(FadeOutRoutine()); }
    IEnumerator FadeInRoutine()
    {
        float alpha = 0;
        canvasGroup.alpha = 0f;
        while (alpha < 1)
        {
            alpha += Time.deltaTime * fadeSpeed;
            canvasGroup.alpha = alpha;
            yield return END;
        }
        yield return END;
        canvasGroup.alpha = 1f;
    }
    IEnumerator FadeOutRoutine()
    {
        float alpha = 1;
        canvasGroup.alpha = 1f;
        while (alpha < 0)
        {
            alpha -= Time.deltaTime * fadeSpeed;
            canvasGroup.alpha = alpha;
            yield return END;
        }
        yield return END;
        canvasGroup.alpha = 0f;
    }
}
